object WorkForm: TWorkForm
  Left = 286
  Top = 193
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 886
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 886
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 884
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel2: TBevel
        Left = 0
        Top = 83
        Width = 884
        Height = 5
        Align = alBottom
        Shape = bsBottomLine
        Style = bsRaised
        ExplicitLeft = -32
        ExplicitTop = 53
        ExplicitWidth = 1029
      end
      object Label1: TLabel
        Left = 304
        Top = 21
        Width = 36
        Height = 12
        Caption = 'Label1'
        Visible = False
      end
      object xmLE: TLabeledEdit
        Left = 142
        Top = 17
        Width = 139
        Height = 20
        CharCase = ecUpperCase
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #39033#30446#32534#21495#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        TabOrder = 0
      end
      object RunQuery: TButton
        Left = 440
        Top = 26
        Width = 82
        Height = 35
        Caption = #26597'  '#35810
        TabOrder = 1
        OnClick = RunQueryClick
      end
      object Toxls: TButton
        Left = 552
        Top = 25
        Width = 82
        Height = 35
        Caption = #25968#25454#23548#20986
        TabOrder = 2
        OnClick = ToxlsClick
      end
      object TimeP: TPanel
        Left = 42
        Top = 43
        Width = 247
        Height = 41
        BevelOuter = bvNone
        TabOrder = 3
        object Label2: TLabel
          Left = 12
          Top = 11
          Width = 144
          Height = 12
          Caption = #26597#35810#24180#24230'            '#26376#20221
        end
        object qbYear: TEdit
          Left = 75
          Top = 9
          Width = 49
          Height = 20
          NumbersOnly = True
          TabOrder = 0
          Text = 'qbYear'
          OnExit = qbYearExit
        end
        object qbmon: TComboBox
          Left = 162
          Top = 8
          Width = 77
          Height = 22
          Style = csOwnerDrawFixed
          TabOrder = 1
          OnClick = qbmonClick
          Items.Strings = (
            #19968#26376
            #20108#26376
            #19977#26376
            #22235#26376
            #20116#26376
            #20845#26376
            #19971#26376
            #20843#26376
            #20061#26376
            #21313#26376
            #21313#19968#26376
            #21313#20108#26376)
        end
      end
    end
    object MGrid: TcxGrid
      AlignWithMargins = True
      Left = 4
      Top = 92
      Width = 878
      Height = 493
      Align = alClient
      TabOrder = 1
      LookAndFeel.NativeStyle = False
      object MTV: TcxGridBandedTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <
          item
            Links = <
              item
                Column = MTVColumn8
              end>
            SummaryItems = <
              item
                Format = '0'
                Kind = skSum
                Position = spFooter
                Column = MTVColumn2
              end
              item
                Format = '0'
                Kind = skSum
                Position = spFooter
                Column = MTVColumn3
              end
              item
                Format = '0'
                Kind = skSum
                Position = spFooter
                Column = MTVColumn4
              end
              item
                Position = spFooter
                OnGetText = MTVTcxGridDataControllerTcxDataSummarySummaryGroups0SummaryItems3GetText
                Column = MTVColumn1
              end>
          end>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnSorting = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.FocusRect = False
        OptionsView.GroupByBox = False
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.GroupSummaryLayout = gslAlignWithColumns
        Styles.ContentEven = cxstyl2
        Styles.Group = cxstyl1
        OnCustomDrawGroupCell = MTVCustomDrawGroupCell
        Bands = <
          item
            Caption = #39033#30446'[]'#26045#24037#24773#20917#32479#35745#34920
            Width = 868
          end
          item
            Caption = #26045#24037#39033#30446#21517#31216#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 0
            Position.ColIndex = 0
            Width = 412
          end
          item
            Caption = #32479#35745#26399#38388#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 0
            Position.ColIndex = 1
            Width = 457
          end
          item
            Caption = 'A:'#19978#26376#32467#23384#21253#25968#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 1
            Position.ColIndex = 0
            Width = 312
          end
          item
            Position.BandIndex = 1
            Position.ColIndex = 1
            Width = 124
          end
          item
            Caption = #26376#26411#32467#20313#24773#20917
            Position.BandIndex = 2
            Position.ColIndex = 0
          end
          item
            Caption = 'B:'#26412#26376#20570#21253#25968#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 3
            Position.ColIndex = 0
            Width = 298
          end
          item
            Position.BandIndex = 4
            Position.ColIndex = 0
          end
          item
            Caption = 'E:'#26376#26411#26032#30732#22909'('#21547#27491#28888#28900#21644#24050#28888#22909')'#26410#20351#29992#21253#25968#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 5
            Position.ColIndex = 0
            Width = 249
          end
          item
            Position.BandIndex = 5
            Position.ColIndex = 1
            Width = 209
          end
          item
            Caption = 'C:'#26412#26376#24050#29992#21253#25968#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 6
            Position.ColIndex = 0
          end
          item
            Position.BandIndex = 7
            Position.ColIndex = 0
          end
          item
            Caption = 'F:'#26376#26411#27491#22312#20351#29992#21253#25968#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 8
            Position.ColIndex = 0
          end
          item
            Position.BandIndex = 9
            Position.ColIndex = 0
          end
          item
            Caption = 'D:'#26412#26376#32467#23384#21253#25968#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 10
            Position.ColIndex = 0
          end
          item
            Position.BandIndex = 11
            Position.ColIndex = 0
          end
          item
            Caption = #39033#30446#29702#35770#23551#21629#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 12
            Position.ColIndex = 0
          end
          item
            Position.BandIndex = 12
            Position.ColIndex = 1
          end
          item
            Caption = #32467#31639#37327#65306
            HeaderAlignmentHorz = taLeftJustify
            Position.BandIndex = 13
            Position.ColIndex = 0
            Width = 71
          end
          item
            Position.BandIndex = 13
            Position.ColIndex = 1
            Width = 66
          end>
        object MTVColumn1: TcxGridBandedColumn
          Caption = #21253#21495
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Position.BandIndex = 14
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn2: TcxGridBandedColumn
          Caption = #24403#26376#28809#27425
          DataBinding.ValueType = 'Integer'
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Position.BandIndex = 14
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object MTVColumn3: TcxGridBandedColumn
          Caption = #32047#35745#28809#27425
          DataBinding.ValueType = 'Integer'
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Position.BandIndex = 14
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object MTVColumn4: TcxGridBandedColumn
          Caption = #24403#26376#27975#38050#37327
          DataBinding.ValueType = 'Integer'
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Position.BandIndex = 15
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn5: TcxGridBandedColumn
          Caption = #19978#32447#26085#26399
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Position.BandIndex = 16
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn6: TcxGridBandedColumn
          Caption = #19979#32447#26085#26399
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Position.BandIndex = 17
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn7: TcxGridBandedColumn
          Caption = #22791#27880
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Width = 57
          Position.BandIndex = 18
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn8: TcxGridBandedColumn
          GroupIndex = 0
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Styles.Header = cxstyl2
          Width = 29
          Position.BandIndex = 19
          Position.ColIndex = 0
          Position.RowIndex = 0
          IsCaptionAssigned = True
        end
      end
      object MGridLevel1: TcxGridLevel
        GridView = MTV
      end
    end
  end
  object Memo1: TMemo
    Left = 536
    Top = 224
    Width = 265
    Height = 129
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Visible = False
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 248
    Top = 294
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 304
  end
  object DataSource1: TDataSource
    DataSet = GDCDS
    Left = 328
    Top = 288
  end
  object GDCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 200
    Top = 352
  end
  object SFD: TSaveTextFileDialog
    DefaultExt = 'xls'
    Left = 88
    Top = 320
  end
  object cxstylrpstry1: TcxStyleRepository
    Top = 40
    PixelsPerInch = 96
    object cxstyl1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clSkyBlue
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxstyl2: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
end
