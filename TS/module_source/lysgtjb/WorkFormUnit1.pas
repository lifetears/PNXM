﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, QBCommon, ExtCtrls, QBParcel,
  Vcl.dialogs, strUtils,
  Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, Vcl.ExtDlgs, cxGraphics,
  cxControls, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxGridLevel, cxGridExportLink,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, cxClasses,
  cxGridCustomView, cxGrid, cxStyles, cxLookAndFeels, cxLookAndFeelPainters,
  cxEdit, cxNavigator;

type
  TWorkForm = class(TForm)
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    xmLE: TLabeledEdit;
    RunQuery: TButton;
    Toxls: TButton;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    PrivilegeCDS: TClientDataSet;
    DataSource1: TDataSource;
    Label1: TLabel;
    GDCDS: TClientDataSet;
    Memo1: TMemo;
    SFD: TSaveTextFileDialog;
    MGrid: TcxGrid;
    MTV: TcxGridBandedTableView;
    MTVColumn1: TcxGridBandedColumn;
    MTVColumn2: TcxGridBandedColumn;
    MTVColumn3: TcxGridBandedColumn;
    MTVColumn4: TcxGridBandedColumn;
    MTVColumn5: TcxGridBandedColumn;
    MTVColumn6: TcxGridBandedColumn;
    MTVColumn7: TcxGridBandedColumn;
    MTVColumn8: TcxGridBandedColumn;
    MGridLevel1: TcxGridLevel;
    cxstylrpstry1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    cxstyl2: TcxStyle;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure MGridDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure MTVCustomDrawGroupCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableCellViewInfo;
      var ADone: Boolean);
    procedure MTVTcxGridDataControllerTcxDataSummarySummaryGroups0SummaryItems3GetText
      (Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    procedure SetTimeRange;
    function RunCDS(SQL: string; CS: TClientDataSet): Boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure GridToxls;
    procedure XmInfoNumINI;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

  xmhead = record
    reLoad: Boolean; // 项目信息是否要刷新
    HeadTime: string; // 表头时间信息
    XmQC: string; // 项目全称
    TimeZone: string; // 统计区间
    A: Integer; // 上月结存数
    B: Integer; // 本月做包数
    C: Integer; // 本月已用包数
    D: Integer; // 本月结存包数
    E: Integer; // 月末未用包数
    F: Integer; // 月末在线包数
    XmAge: Integer; // 项目理论寿命
    JsMc: string; // 结算名称
    jsl: double; // 结算量
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'LYSGTJB_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';

var
  zhanghu: string;
  xmInfo: xmhead;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  //
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 初始化xminfo
  xmInfo.reLoad := True;
  xmInfo.HeadTime := '';
  xmInfo.XmQC := '';
  xmInfo.TimeZone := '';
  XmInfoNumINI;
  xmInfo.XmAge := 0;
  xmInfo.JsMc := '';
  xmInfo.jsl := 0;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const

  // 查询主SQL
  MSQL = 'WITH dd AS                  ' + slineBreak +
    '(                                 ' + slineBreak +
    'SELECT tc_pjy01,tc_pjy02,tc_pjy05,tc_pjy07,tc_pjy11,' + slineBreak +
    '(CASE WHEN tc_pjy05 >= to_date(''%s'',''yyyymmdd'') THEN ''B'' ' +
    'ELSE ''A'' END) SY,                                 ' + slineBreak +
    '(CASE WHEN tc_pjy07 > to_date(''%s'',''yyyymmdd'') OR tc_pjy07 IS NULL THEN ''D'' ' +
    'ELSE ''C'' END) DY                                 ' + slineBreak +
    ' FROM tc_pjy_file                                              ' +
    ' WHERE   tc_pjy03 = ''%s'' AND ' + slineBreak +
    ' (tc_pjy07 >= to_date(''%0:s'',''yyyymmdd'') OR tc_pjy07 is NULL) ' + slineBreak +
    ' AND tc_pjy05 <= to_date(''%1:s'',''yyyymmdd'') ' + slineBreak +
    ' ORDER BY tc_pjy02                              ' + slineBreak +
    ' )                                              ' + slineBreak +
    '                                                ' + slineBreak +
    'SELECT tc_pjy02,NVL(lp.tc_pjz05,0) tc_pjz05,NVL(tp.tlc,0) tlc, ' +
    '  NVL(tc_pjz04,0) tc_pjz04,tc_pjy05,tc_pjy07,'''',             ' +
    '  (CASE WHEN (DY = ''D'' AND TP.TLC = 0) THEN ''E'' ' + slineBreak +
    ' WHEN (DY = ''D'' AND TP.TLC > 0) THEN ''F'' WHEN (DY=''C'') THEN ''C''  ELSE ''-'' END) JY,dy,sy' +
    slineBreak + ' FROM dd                                       ' + slineBreak
    + '  LEFT JOIN (SELECT tc_pjz01,NVL(SUM(tc_pjz05),0) TLC  FROM tc_pjz_file' +
    '           WHERE tc_pjz10 = ''%2:s'' AND  tc_pjz02*100+tc_pjz03 <= %3:d' +
    '           GROUP BY tc_pjz01) TP ON tc_pjz01 = tc_pjy01' + slineBreak +
    '  LEFT JOIN tc_pjz_file LP                 ' + slineBreak +
    '            ON lp.tc_pjz01 = tc_pjy01 AND  ' + slineBreak +
    '               lp.tc_pjz10 = ''%2:s'' AND  ' + slineBreak +
    '               lp.tc_pjz02 = %4:d AND      ' + slineBreak +
    '               lp.tc_pjz03 = %5:d          ' + slineBreak +
    'ORDER BY dy,tc_pjy02                       ';

var
  xmnF: string; // 项目编号
  BTimeF, ETimeF: Tdate; // 初始/结束时间 筛选条件
  bmonth, bYear: word; // 过滤条件中使用到的年与月份
  bts, ets: string;
  ym, year, month: Integer;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

procedure TWorkForm.SetTimeRange;
// 取初始/结束日期
var
  SQL: string;
  y, m, D: word;
begin
  if xmLE.Text = '' then
  begin
    showmessage('请先输入项目编号后继续');
    exit;
  end;
  y := strtointdef((qbYear.Text), YearOf(now));

  SQL := Format('SELECT gem02,NVL(ta_gem25,0) ll,NVL(ta_gem33,1) dd,' +
    'NVL(tc_pjb06,''-'') t6,NVL(tc_pjb07,0) t7 FROM gem_file ' + slineBreak +
    'LEFT JOIN tc_pjb_file ON gem01 = tc_pjb01 AND tc_pjb02 = %d AND tc_pjb03 = %d '
    + 'WHERE gem01 = ''%s''', [y, qbmon.ItemIndex + 1,trim(xmLE.Text)]);

       Memo1.Lines.Text := SQL;

  if Not RunCDS(SQL, GDCDS) then
    exit;
  GDCDS.Last;

//  if GDCDS.RecordCount > 1 then
//    exit; // 查询的结果大于1条,可能cooi032中维护了两种参数,所以将此条判断过滤掉。

  xmInfo.XmQC := GDCDS.FieldByName('gem02').AsString;
  xmInfo.XmAge := GDCDS.FieldByName('ll').AsInteger;
  xmInfo.JsMc := GDCDS.FieldByName('t6').AsString;
  xmInfo.jsl := GDCDS.FieldByName('T7').AsFloat;

  D := GDCDS.FieldByName('dd').AsInteger;
      m := qbmon.ItemIndex + 1;
  if D = 1 then
  begin
    BTimeF := EncodeDate(y, m, 1);
    ETimeF := IncMonth(EncodeDate(y, m, D), 1) - 1;
  end
  else
  begin
    BTimeF := incMonth(EncodeDate(y, m, D), -1) + 1;
    ETimeF := EncodeDate(y, m, D);
  end;
  memo1.Lines.Append(y.ToString + '==' + m.ToString + '==' + d.ToString);
  bts := FormatDateTime('yyyymmdd', BTimeF);
  ets := FormatDateTime('yyyymmdd', ETimeF);
  ym := y * 100 + m;
  year := y;
  month := m;

  xmInfo.HeadTime := Format('%d年%d月', [year, month]);
  xmInfo.TimeZone := FormatDateTime('dddddd', BTimeF) + '~' +
    FormatDateTime('dddddd', ETimeF);
end;

function TWorkForm.RunCDS(SQL: string; CS: TClientDataSet): Boolean;
begin
  Result := false;
  CS.close;
  if not dba.ReadDataset(SQL, CS) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    CS.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

function TWorkForm.GetFilterS: string;
// 返回动态筛选条件
var
  FS: TstringList;
  I: Integer;
  dt: Tdate;
  s, ss: string;
begin
  Result := '';
  SetTimeRange;
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
// 查寻
var
  filter, SQL: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;

  if trim(xmLE.Text) = '' then
  begin
    showmessage('请录入合适项目编号后继续!');
    exit;
  end;
  xmnF := trim(xmLE.Text);

  GetFilterS;

  // 主查询
  screen.Cursor := crSQLWait;

  try
    SQL := Format(MSQL, [bts, ets, xmnF, ym, year, month]);

//     Memo1.Lines.Text := SQL;
//     exit;
    RunCDS(SQL, GDCDS);

    if GDCDS.RecordCount = 0 then
      showmessage('该项目不存在或未有施工信息存在！')
    else
      FillmGrid(GDCDS);
    ResizeForm;
  finally
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.GridToxls;
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
      ExportGridToExcel(SaveDialog.FileName, MGrid, True, True, True, 'xls');
      showmessage('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.XmInfoNumINI;
//项目信息中的各种包数置零
begin
  xmInfo.A := 0;
  xmInfo.B := 0;
  xmInfo.C := 0;
  xmInfo.D := 0;
  xmInfo.E := 0;
  xmInfo.F := 0;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
begin
  GridToxls;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
begin
  GridToxls;
end;

procedure TWorkForm.MGridDrawDataCell(Sender: TObject; const Rect: TRect;
  Field: TField; State: TGridDrawState);
begin
  if (GDCDS.FieldByName('rowNum').AsInteger mod 2 > 0) then
  begin
    MGrid.Canvas.Brush.Color := clInfoBk; // 背景色
    MGrid.Canvas.FillRect(Rect);
  end;
end;

procedure TWorkForm.MTVCustomDrawGroupCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableCellViewInfo; var ADone: Boolean);
begin
  if AViewInfo.Text = 'C' then
    AViewInfo.Text := '已用包使用情况统计表';
  if AViewInfo.Text = 'F' then
    AViewInfo.Text := '月末正在使用情况统计表';
end;

procedure TWorkForm.
  MTVTcxGridDataControllerTcxDataSummarySummaryGroups0SummaryItems3GetText
  (Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  AText := '合计:';
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  I, j: Integer;
  s: char;
begin
  // 填充数据部分
  MTV.DataController.RecordCount := 0;
  if GDCDS.RecordCount = 0 then
    exit;
  GDCDS.First;
  I := 0;
  j := 0;
  MTV.BeginUpdate();
  while Not GDCDS.Eof do
  begin
    if GDCDS.FieldByName('JY').AsString[1] in ['C', 'F'] then
    begin
      MTV.DataController.AppendRecord;
      for j := 0 to 7 do
        MTV.DataController.Values[I, j] := GDCDS.Fields.Fields[j].AsString;
      Inc(I);
    end;
    GDCDS.Next;
  end;

  // 填充表头Head
  GDCDS.First;
  xmInfoNumIni;
  while Not GDCDS.Eof do
  begin
    s := GDCDS.FieldByName('sy').AsString[1];
    case s of
      'A':
        Inc(xmInfo.A);
      'B':
        Inc(xmInfo.B);
    end;

    s := GDCDS.FieldByName('dy').AsString[1];
    case s of
      'C':
        Inc(xmInfo.C);
      'D':
        Inc(xmInfo.D);
    end;

    s := GDCDS.FieldByName('JY').AsString[1];
    case s of
      'E':
        Inc(xmInfo.E);
      'F':
        Inc(xmInfo.F);
    end;
    GDCDS.Next;
  end;
  MTV.Bands[0].Caption := Format('项目[%s]施工情况统计表', [xmInfo.HeadTime]);
  MTV.Bands[1].Caption := Format('施工项目名称：%s', [xmInfo.XmQC]);
  MTV.Bands[2].Caption := Format('统计期间： %s', [xmInfo.TimeZone]);
  MTV.Bands[4].Caption := Format('%d 个', [xmInfo.A]);
  MTV.Bands[7].Caption := Format('%d 个', [xmInfo.B]);
  MTV.Bands[9].Caption := Format('%d 个', [xmInfo.E]);
  MTV.Bands[11].Caption := Format('%d 个', [xmInfo.C]);
  MTV.Bands[13].Caption := Format('%d 个', [xmInfo.F]);
  MTV.Bands[15].Caption := Format('%d 个', [xmInfo.D]);
  MTV.Bands[17].Caption := Format('%d', [xmInfo.XmAge]);
  MTV.Bands[18].Caption := Format('%s', [xmInfo.JsMc]);
  MTV.Bands[19].Caption := formatfloat('0.000', xmInfo.jsl);
  MTV.EndUpdate;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}

// 消息响应事件
// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
