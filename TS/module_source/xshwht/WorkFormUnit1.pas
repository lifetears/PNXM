﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTextEdit, cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer, cxContainer,
  cxEdit, Vcl.DBCtrls, cxDropDownEdit, cxDBEdit, cxButtonEdit, cxMaskEdit,
  cxCalendar, cxCurrencyEdit;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    DELS: TcxStyle;
    AddS: TcxStyle;
    ModS: TcxStyle;
    DefaultS: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    pnl1: TPanel;
    btndel: TButton;
    btnfind: TButton;
    btnOK: TButton;
    btnmodi: TButton;
    btnbadd: TButton;
    btnOpenTQP: TButton;
    pnl_db: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl7: TLabel;
    lbllzhuangtai: TLabel;
    cxdbtxtdtTQPUD01: TcxDBTextEdit;
    cxdbtxtdtTQP02: TcxDBTextEdit;
    cxdbdtdtTQP06: TcxDBDateEdit;
    cxdbdtdtTQP07: TcxDBDateEdit;
    cxdbtxtdtTQP03: TcxDBTextEdit;
    btn1_tqp08: TcxDBButtonEdit;
    khjc: TLabeledEdit;
    khqc: TLabeledEdit;
    khgj: TLabeledEdit;
    khqy: TLabeledEdit;
    khywy: TLabeledEdit;
    cbbTQP04: TcxDBComboBox;
    cbb_bizhong: TComboBox;
    cxdbtxtdt1_tqp21: TcxDBTextEdit;
    dbnvgr1: TDBNavigator;
    dbgrd1: TDBGrid;
    pnl_kh: TPanel;
    pnl4: TPanel;
    qkhbh: TLabeledEdit;
    qkhgj: TLabeledEdit;
    qkhjc: TLabeledEdit;
    qkhqc: TLabeledEdit;
    pnl5: TPanel;
    btn1: TButton;
    btn3: TButton;
    dbgrd2: TDBGrid;
    pnl_find: TPanel;
    pnl7: TPanel;
    qhtbh: TLabeledEdit;
    qhtmc: TLabeledEdit;
    qhtkq: TLabeledEdit;
    qhtgj: TLabeledEdit;
    pnl8: TPanel;
    btn2: TButton;
    btn4: TButton;
    ds1: TDataSource;
    Tuq: TClientDataSet;
    ds2: TDataSource;
    cxDBCurrencyTQBUD07: TcxDBCurrencyEdit;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure btnOpenTQPClick(Sender: TObject);
    procedure btnfindClick(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure dbgrd2DblClick(Sender: TObject);
    procedure cbbTQP04PropertiesEditValueChanged(Sender: TObject);
    procedure cxdbtxtdt1_tqp21PropertiesEditValueChanged(Sender: TObject);
    procedure cbb_bizhongChange(Sender: TObject);
    procedure dbnvgr1Click(Sender: TObject; Button: TNavigateBtn);
    procedure btn1_tqp08PropertiesEditValueChanged(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btn1_tqp08PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure btnmodiClick(Sender: TObject);
    procedure btnbaddClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure GetLoginUser(Key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string; DS: TClientDataSet): Boolean;
    procedure GetAuth(Key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure fillKhInfo;
    procedure InsertIniField;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'XSHWHT_DLL';

  // 业务代码
const
  FSQL = 'SELECT occ01,occ02,occ18,gen02,geb02,gea02 FROM occ_file ' +
    sLineBreak + 'LEFT JOIN geb_file ON geb01 = occ21 ' + sLineBreak +
    'LEFT JOIN gea_file ON gea01 = geb03 ' + sLineBreak +
    'LEFT JOIN gen_file ON gen01 = occ04 WHERE ROWNUM < 100 AND ' + sLineBreak +
    ' occ22 = ''99'' ' + sLineBreak + // 只查询国外的客户
    ' %s';
  MSQL = 'SELECT tqp01,tqpud01,tqp02,tqp06,tqp07,tqp03,tqpud07,tqp21,tqp04,tqp08,'
    + sLineBreak +
    'tqpud02,tqporiu,tqpdate,tqpplant,tqplegal,tqpacti,tqporig FROM tqp_file WHERE ROWNUM < 100  %s  ORDER BY tqp01 DESC';

var
  Sbizhong: TstringList;

  zhanghu: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if assigned(Sbizhong) then
    freeandNil(Sbizhong);
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  pnl1.Enabled := false;
end;

// -----------------------------------------------------------------------------
procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string; DS: TClientDataSet): Boolean;
var
  s, t: string;
begin
  Result := false;
  // DS.close;
  if not dba.ReadDataset(SQL, DS) then
  begin
    application.MessageBox('读数据集失败！', 'Infomation');
    dba.GetLastError(s, t);
    LogMsg(pchar(DLL_KEY + s + ':' + t));
  end
  else
  begin
    DS.Active := True;
    Result := True;
  end;
end;

procedure TWorkForm.btn3Click(Sender: TObject);
begin
  pnl_kh.Visible := false;
  MasterP.Enabled := True;
end;

procedure TWorkForm.dbgrd2DblClick(Sender: TObject);
begin
  // 将当前值插入到字段值中
  if Cds.State in [dsinsert, dsEdit] then
  begin
    Cds.FieldValues['tqp08'] := Tuq.FieldByName('occ01').Text;
    fillKhInfo;
  end;
  pnl_kh.Visible := false;
  dbgrd2.Enabled := false;
  MasterP.Enabled := True;
end;

procedure TWorkForm.dbnvgr1Click(Sender: TObject; Button: TNavigateBtn);
begin
  case Button of
    nbFirst:
      ;
    nbPrior:
      ;
    nbNext:
      ;
    nbLast:
      ;
    nbInsert:
      InsertIniField;
    nbDelete:
      ;
    nbEdit:
      ;
    nbPost:
      ;
    nbCancel:
      ;
    nbRefresh:
      ;
    nbApplyUpdates:
      ;
    nbCancelUpdates:
      ;
  end;
end;

procedure TWorkForm.btn1_tqp08PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if Cds.State in [dsinsert, dsEdit] then
  begin
    // open the form to select the customer
    MasterP.Enabled := false;
    pnl_kh.Visible := True;
    dbgrd2.Enabled := True;

    ds2.DataSet := nil;
    qkhbh.Text := '';
    qkhjc.Text := '';
    qkhqc.Text := '';
    qkhqc.Text := '';
  end;
end;

procedure TWorkForm.btn1_tqp08PropertiesEditValueChanged(Sender: TObject);
begin
  RunCDS(Format(FSQL, [' AND OCC01 = ''' + Cds.FieldByName('tqp08').Text +
    '''']), Tuq);
  fillKhInfo;
end;

procedure TWorkForm.btn1Click(Sender: TObject);
var
  s, SQL: string;
begin
  // 用来查询搜索的客户信息
  s := ' AND 1 = 1 ';
  if qkhbh.Text <> '' then
    s := s + ' AND occ01 like ''%' + qkhbh.Text + '%''';
  if qkhjc.Text <> '' then
    s := s + ' AND occ02 like ''%' + qkhjc.Text + '%''';
  if qkhqc.Text <> '' then
    s := s + ' AND occ18 like ''%' + qkhqc.Text + '%''';
  if qkhgj.Text <> '' then
    s := s + ' AND geb02 like ''%' + qkhgj.Text + '%''';

  SQL := Format(FSQL, [s]);
  RunCDS(SQL, Tuq);

  ds2.DataSet := Tuq;
end;

procedure TWorkForm.btn2Click(Sender: TObject);
var
  s, SQL: string;
begin
  s := ' AND 1 = 1 ';
  if qhtbh.Text <> '' then
    s := ' AND  tqpud01 LIKE ''%' + qhtbh.Text + '%''';
  if qhtmc.Text <> '' then
    s := ' AND  tqp02 LIKE ''%' + qhtmc.Text + '%''';
  if qhtkq.Text <> '' then
    s := ' AND  tqp08 LIKE ''%' + qhtkq.Text + '%''';
  if qhtgj.Text <> '' then
    s := ' AND  tqp08 IN (SELECT occ01 FROM occ_file,geb_file WHERE occ21 = geb01 AND geb02 LIKE ''%'
      + qhtgj.Text + '%'')';

  Cds.DisableControls;
  SQL := Format(MSQL, [s]);
  RunCDS(SQL, Cds);
  Cds.EnableControls;
  pnl_find.Visible := false;
  MasterP.Enabled := True;
  ResizeForm;
end;

procedure TWorkForm.btn4Click(Sender: TObject);
begin
  pnl_find.Visible := false;
  MasterP.Enabled := True;
end;

procedure TWorkForm.btnbaddClick(Sender: TObject);
begin
  //
end;

procedure TWorkForm.btnfindClick(Sender: TObject);
begin
  if Cds.ChangeCount > 0 then
  begin
    application.MessageBox('尚有记录修改未提交，请提交后继续！', 'Infomation');
    exit;
  end
  else
  begin
    pnl_find.Visible := True;
    qhtbh.Text := '';
    qhtmc.Text := '';
    qhtkq.Text := '';
    qhtgj.Text := '';
    MasterP.Enabled := false;
    ResizeForm;
  end;
end;

procedure TWorkForm.btnmodiClick(Sender: TObject);
begin
  if Cds.ChangeCount > 0 then
    Cds.MergeChangeLog
    else
    application.MessageBox('当前没有修改任何记录，请确认是否点击[√]键！', 'Infomation');
end;

procedure TWorkForm.btnOKClick(Sender: TObject);
var
  s, t: string;
begin
  if Cds.ChangeCount > 0 then
  begin
    if dba.CommitCdsDelta('TQP_FILE', 'TQP01', Cds) then
    begin
      Cds.MergeChangeLog;
      application.MessageBox('数据上传成功！', 'Infomation');
    end
    else
    begin
      dba.GetLastError(s, t);
      LogMsg(pchar('上传错误代码' + s + ' : ' + t));
    end;
  end
  else
    application.MessageBox('没有需要上传的记录数据，请确认是否点击[√]键！', 'Infomation');
end;

procedure TWorkForm.btnOpenTQPClick(Sender: TObject);
var
  SQL: string;
begin
  if not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 5 > 0) then
      exit;
  end;

  if Not assigned(Sbizhong) then
  begin
    Sbizhong := TstringList.Create;
    SQL := 'SELECT azi01,azi02 FROM AZI_FILE';
    RunCDS(SQL, Tuq);
    Tuq.First;
    while Not Tuq.Eof do
    begin
      Sbizhong.Append(Tuq.FieldByName('azi01').Text);
      cbb_bizhong.Items.Append(Tuq.FieldByName('azi02').Text);
      Tuq.Next;
    end;
  end;
  SQL := Format(MSQL, [' AND 1 =1 ']);
  RunCDS(SQL, Cds);
  ResizeForm;
  btnOpenTQP.Enabled := false;
  pnl1.Enabled := true;
end;

procedure TWorkForm.cbbTQP04PropertiesEditValueChanged(Sender: TObject);
begin
  case StrToIntDef(cbbTQP04.Text, 3) of
    1:
      lbllzhuangtai.Caption := '开立';
    2:
      lbllzhuangtai.Caption := '申请';
    3:
      lbllzhuangtai.Caption := '审核';
    4:
      lbllzhuangtai.Caption := '结案';
  end;
end;

procedure TWorkForm.cbb_bizhongChange(Sender: TObject);
begin
  if Cds.State in [dsEdit, dsinsert] then
    Cds.FieldValues['tqp21'] := Sbizhong[cbb_bizhong.ItemIndex];
end;

procedure TWorkForm.cxdbtxtdt1_tqp21PropertiesEditValueChanged(Sender: TObject);
var
  i: Integer;
  s: string;
begin
  if Cds.State in [dsbrowse] then
  begin
    s := Cds.FieldByName('tqp21').Text;
    i := Sbizhong.IndexOf(s);
    if i > -1 then
      cbb_bizhong.ItemIndex := i
    else
      cbb_bizhong.ItemIndex := cbb_bizhong.Items.Count - 1;
  end;
end;

// ------------------------------------------------------------------------------------
procedure TWorkForm.InsertIniField;
var
  ID: string;
begin
  if Not dba.GenerateId('TQP_FILE', 'tqp01', '', 'A', '', ID) then
  begin
    application.MessageBox('生成记录ID失败，请重试', 'Information');
    exit;
  end;
  // 新增记录时初始化字段值
  Cds.DisableControls;
  Cds.FieldValues['tqp01'] := ID; // 获取自动计数号
  Cds.FieldValues['tqp03'] := '000'; // 表示为易拓系统外录入数据
  Cds.FieldValues['tqp21'] := 'RMB';
  Cds.FieldValues['tqp04'] := '3'; // 默认审核状态
  Cds.FieldValues['tqpud07'] := 0;
  Cds.FieldValues['tqporiu'] := zhanghu;
  Cds.FieldValues['tqpdate'] := Date;
  Cds.FieldValues['tqp06'] := Date;
  Cds.FieldValues['tqp07'] := Date;
  Cds.FieldValues['tqpplant'] := 'GWGS';
  Cds.FieldValues['tqplegal'] := 'GWGS';
  Cds.FieldValues['tqpacti'] := 'Y';
  Cds.FieldValues['tqporig'] := '010601';
  Cds.EnableControls;
  cbb_bizhong.ItemIndex := Sbizhong.IndexOf('RMB');
end;

procedure TWorkForm.fillKhInfo;
begin
  khjc.Text := Tuq.FieldByName('occ02').Text;
  khqc.Text := Tuq.FieldByName('occ18').Text;
  khgj.Text := Tuq.FieldByName('geb02').Text;
  khqy.Text := Tuq.FieldByName('gea02').Text;
  khywy.Text := Tuq.FieldByName('gen02').Text;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  freeandNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(Key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(Key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  freeandNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  freeandNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(Key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(Key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  freeandNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(pchar(MsgName), pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(pchar(MsgName), pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(pchar(MsgName), pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
