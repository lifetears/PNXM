﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTextEdit, cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    ToXlsMX: TButton;
    xmF: TPanel;
    Panel5: TPanel;
    Bevel3: TBevel;
    xmsa: TButton;
    xmcl: TButton;
    xmok: TButton;
    xmca: TButton;
    xmcb: TAdvStringGrid;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    DELS: TcxStyle;
    AddS: TcxStyle;
    ModS: TcxStyle;
    DefaultS: TcxStyle;
    MTreeL: TcxTreeList;
    MTreeLColumn1: TcxTreeListColumn;
    MTreeLColumn2: TcxTreeListColumn;
    MTreeLColumn3: TcxTreeListColumn;
    MTreeLColumn4: TcxTreeListColumn;
    MTreeLColumn5: TcxTreeListColumn;
    MTreeLColumn6: TcxTreeListColumn;
    MTreeLColumn7: TcxTreeListColumn;
    MTreeLColumn8: TcxTreeListColumn;
    MTreeLColumn9: TcxTreeListColumn;
    MTreeLColumn10: TcxTreeListColumn;
    MTreeLColumn11: TcxTreeListColumn;
    MTreeLColumn12: TcxTreeListColumn;
    MTreeLColumn13: TcxTreeListColumn;
    MTreeLColumn14: TcxTreeListColumn;
    MTreeLColumn15: TcxTreeListColumn;
    MTreeLColumn16: TcxTreeListColumn;
    MTreeLColumn17: TcxTreeListColumn;
    MTreeLColumn18: TcxTreeListColumn;
    PrivilegeCDS: TClientDataSet;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure ztxzClick(Sender: TObject);
    procedure qxmClick(Sender: TObject);
    procedure xmclClick(Sender: TObject);
    procedure xmokClick(Sender: TObject);
    procedure xmsaClick(Sender: TObject);
    procedure xmcaClick(Sender: TObject);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure MTreeLCustomDrawDataCell(Sender: TcxCustomTreeList;
      ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      var ADone: Boolean);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function XmCount(const zh: string): Integer;
    procedure XmcbClose;
    procedure XmcbOpen;
    procedure SetTimeRange;
    function XmcbSelectCount: Integer;
    function RunCDS(SQL: string): Boolean;
    function GetFilterS: string;
    procedure FillTreeList(Q: TClientDataSet);
    procedure FullRTTree(ARecords: TstringList);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'LYLingLiao_DLL';
  // 业务代码
  SPCHAR = '-';
  TreeSplit = '@';
  DefaultZT = 'GWGS';
  tlev = 17; // 记录lev的列次
  tLJ = 0; // 记录料件的列次
  tXM = 7; // 记录项目的列次
  tBH = 9; // 记录包号的列次
  // 查询用户负责的项目编号集

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now;

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  TimeP.Visible := false;

  // 获取是否可修改项目编号权限,设置xeLE的只读性
  GetPrivilege('ASSERT', '1', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    xmLE.ReadOnly := True
  else
    xmLE.ReadOnly := PrivilegeCDS.FieldByName('R1').AsString = '0';
end;

// -----------------------------------------------------------------------------
// 业务代码
const
  // 查询用户负责的项目编号集
  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';
  // 查询主SQL
  MasterSQL =
    'SELECT imn03,ima02,ima021,imn42,ima25,imn45,ima907,imm17,imm01,DECODE(immud03,''1'',''新砌'',''2'',''维修'',''3'',''分摊'','''') AS immud03,imn02,'
    + sLineBreak +
    'DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',''YKGS'',''营口公司'',''-'') IMN06,'
    + sLineBreak + 'imn15,imd02,immud02,imm14,gem02,lev FROM ' + sLineBreak +
    '(                                                                                   '
    + sLineBreak +
    'SELECT imn03,imn42,imn45,imm17,imm01,immud03,imn02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,imn15,immud02,nvl(imm14,'' '') imm14,4 AS lev     '
    + sLineBreak +
    'FROM %simn_file,%0:simm_file WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL AND immud06 IS NULL   %s   '
    + sLineBreak + 'UNION ALL      ' + sLineBreak +
    'SELECT imn03,sum(imn42),sum(imn45),TRUNC(SYSDATE),''-'','''',0,'' '','' '','' '','' '',1 AS lev     '
    + sLineBreak +
    'FROM %0:simn_file,%0:simm_file WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL AND immud06 IS NULL  %1:s   '
    + sLineBreak + 'GROUP BY imn03     ' + sLineBreak + 'UNION ALL      ' +
    sLineBreak +
    'SELECT imn03,sum(imn42),sum(imn45),TRUNC(SYSDATE),''--'','''',0,'' '','' '','' '',imm14,2 AS lev     '
    + sLineBreak +
    'FROM %0:simn_file,%0:simm_file WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL AND immud06 IS NULL  %1:s  '
    + sLineBreak + 'GROUP BY imn03,imm14     ' + sLineBreak + 'UNION ALL     ' +
    sLineBreak +
    'SELECT imn03,sum(imn42),sum(imn45),TRUNC(SYSDATE),''---'','''',0,'' '','' '',SUBSTR(immud02,0,INSTR(immud02,''-'')-1) immud02,imm14,3 AS lev     '
    + sLineBreak +
    'FROM %0:simn_file,%0:simm_file WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL AND immud06 IS NULL  %1:s    '
    + sLineBreak +
    'GROUP BY imn03,imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1)     ' +
    sLineBreak + ') oo      ' + sLineBreak +
    'LEFT JOIN ima_file ON ima01 = oo.imn03     ' + sLineBreak +
    'LEFT JOIN gem_file ON gem01 = imm14     ' + sLineBreak +
    'LEFT JOIN imd_file ON imd01 = imn15     ' + sLineBreak +
    'ORDER BY imn03,imm14,immud02,imm01     ';

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.XmCount(const zh: string): Integer;
// 返回账号所负责的项目数目
var
  i: Integer;
  SQL: string;
begin
  if zh = '' then
    exit(0);
  // 查询返回项目数
  if CurrentZT = '' then
    CurrentZT := DefaultZT;

  SQL := Format(QueryXmSQL, [CurrentZT + '.', zh]);
{$IFDEF DEBUG} LogMsg('LYshouhuo_GetXM_SQL:' + SQL); {$ENDIF}
  if not RunCDS(SQL) then
    exit(0);

  if Cds.RecordCount = 0 then
    exit(0);

  dba.ReadDataset(SQL, Cds);
  if Cds.RecordCount = 0 then
    exit(0);

  xmcb.FixedCols := 0;
  xmcb.ColWidths[0] := 20;
  xmcb.Options := xmcb.Options + [goRowSelect, goEditing];
  xmcb.ShowSelection := false;

  // 在GRID中显示项目
  xmcb.RowCount := Cds.RecordCount + 1;
  Cds.First;
  i := 1;
  while not Cds.Eof do
  begin
    xmcb.AddCheckBox(0, i, false, false);
    xmcb.Cells[1, i] := Cds.Fields.Fields[0].AsString;
    xmcb.Cells[2, i] := Cds.Fields.Fields[1].AsString;
    inc(i);
    Cds.Next;
  end;
  Result := Cds.RecordCount;
end;

procedure TWorkForm.MTreeLCustomDrawDataCell(Sender: TcxCustomTreeList;
  ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
  var ADone: Boolean);
// 当料件汇总时，料件及品号及规格字体为红
// 当料件+项目汇总时，料件及品号及规格及项目纺字体为红
// 当料件+项目+包号汇总时，料件及品号及规格字体为红
begin
  inherited;
  if AViewInfo.Node.Texts[tlev] = '1' then
    if AViewInfo.Column.ItemIndex in [0, 1, 2] then
      ACanvas.Font.Color := clred;
  if AViewInfo.Node.Texts[tlev] = '2' then
    if AViewInfo.Column.ItemIndex in [0, 1, 2, 7, 8] then
      ACanvas.Font.Color := clred;
  if AViewInfo.Node.Texts[tlev] = '3' then
    if AViewInfo.Column.ItemIndex in [0, 1, 2, 7, 8, 9] then
      ACanvas.Font.Color := clred;
end;

procedure TWorkForm.xmokClick(Sender: TObject);
// 确定返回的选中项目号写入项目编号中。若全不选中，则弹出出错框。若全选，则筛选条件不包括项目编号条件
var
  i: Integer;
  State: Boolean;
  Value: TstringList;
begin
  Value := TstringList.Create;

  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, i, State);
    if State then
      Value.Append(xmcb.Cells[1, i]);
  end;
  if Value.Count = 0 then
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
    begin
      Value.Free;
      exit;
    end;

  xmLE.Text := Value.DelimitedText; // 返回项目统计选择项
  Value.Free;
  XmcbClose;
end;

procedure TWorkForm.xmsaClick(Sender: TObject);
var
  i: Integer;
begin
  // 设置所有项目编号全选中
  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, i, True);
    xmcb.RowColor[i] := xmcb.SelectionColor;
  end;
end;

procedure TWorkForm.ztxzClick(Sender: TObject);
var
  s: string;
begin
  // 切换当前选择项，获取当前账套
  case ztxz.ItemIndex of
    1:
      CurrentZT := 'XCXD' // 现场测试账套
  else
    CurrentZT := DefaultZT;
  end;
  // 获取新的项目编号
  xmF.Visible := false;
  TimeP.Visible := false;
  s := xmcb.ColumnHeaders.DelimitedText;
  xmcb.ClearAll;
  xmcb.ColumnHeaders.DelimitedText := s;
  if XmCount(zhanghu) = 0 then
    application.MessageBox(Pchar('当前账套下没有账户：' + zhanghu + ' 所负责的项目!'), '提示');

  // 界面初始化
  xmLE.Text := '';

  MTreeL.Clear;

  self.Caption := '收货查询 易拓账号: ' + zhanghu + '  当前账套： ' + ztxz.Items
    [ztxz.ItemIndex];
end;

procedure TWorkForm.XmcbOpen;
// 打开项目编号选择
begin
  MasterP.Enabled := false;
  xmF.Visible := True;
  ResizeForm;
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
var
  y, m, d: word;
  SQL: string;
begin
  // 期别的影响
  if Not TimeP.Visible then
    exit;

  SQL := 'SELECT  NVL(ta_gem33,1) dd FROM ' + CurrentZT +
    '.gem_file WHERE gem01 = ''' + trim(xmLE.Text) + '''';
  if Not RunCDS(SQL) then
    exit;
  Cds.First;
  if Cds.RecordCount > 1 then
    exit; // 查询的结果大于1条,不可能,直接退出
  y := strtoint(qbYear.Text);
  m := qbmon.ItemIndex + 1;
  d := Cds.FieldByName('dd').AsInteger;
  if d = 1 then
  begin
    beginT.DateTime := EncodeDate(y, m, d);
    endT.DateTime := IncMonth(EncodeDate(y, m, d), 1) - 1;
  end
  else
  begin
    beginT.DateTime := incMonth(EncodeDate(y, m, d), -1) + 1;
    endT.DateTime := EncodeDate(y, m, d);
  end;
end;

procedure TWorkForm.XmcbClose;
// 关闭项目编号选择
begin
  xmF.Visible := false;
  MasterP.Enabled := True;
  // 当选中项为1时显示期间选择
  TimeP.Visible := XmcbSelectCount = 1;
  if TimeP.Visible then
    SetTimeRange;
  ResizeForm;
end;

function TWorkForm.XmcbSelectCount: Integer;
// 返回项目编号选择数目
var
  i: Integer;
  State: Boolean;
begin
  Result := 0;
  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, i, State);
    if State then
      inc(Result);
  end;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin
  // 返回项目编号选择,选择前后MasterP大小发生变化不知为何，重新定义其大小
  XmcbOpen;
end;

procedure TWorkForm.xmcaClick(Sender: TObject);
begin
  // 直接退出，设置xmf不可见。保持原筛选条件
  if XmcbSelectCount = 0 then
  begin
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
      exit;
  end;
  XmcbClose;
end;

procedure TWorkForm.xmclClick(Sender: TObject);
var
  i: Integer;
begin
  // 设置所有项目编号全不选中
  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, i, false);
    xmcb.RowColor[i] := xmcb.Color;
  end;
end;

function TWorkForm.GetFilterS: string;
// 返回动动态筛选条件
// 若项目编号选择为空，则带出所有项目编号信息
var
  FS: TstringList;
  i: Integer;
  bt, et: Tdate;
begin
  Result := '';
  FS := TstringList.Create;

  if xmLE.Text = '' then
  // 若项目编号选择为空，则带出所有项目编号信息
  begin
    for i := 1 to xmcb.RowCount - 1 do
      FS.Append(xmcb.Cells[1, i]);
    xmLE.Text := FS.DelimitedText;
  end
  else
    FS.DelimitedText := xmLE.Text;
  for i := 0 to FS.Count - 1 do
    FS[i] := '''' + FS[i] + '''';

  Result := ' AND imm14 in (' + FS.DelimitedText + ') ';
  FS.Free;

  bt := beginT.Date;
  et := endT.Date;
  if et < bt then
  begin
    Result := 'Error : 结束日期不可小于开始时间！';
    exit;
  end;

  Result := Result + ' AND imm17 >= to_date(''' + FormatDateTime('yyyymmdd', bt)
    + ''',''yyyymmdd'') AND ' + ' imm17 <= to_date(''' +
    FormatDateTime('yyyymmdd', et) + ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
// 查寻
var
  filter, SQL: string;
begin
  if not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 5 > 0) then
      exit;
  end;
  // 主查询
  self.Cursor := crSQLWait;
  filter := GetFilterS;

  if filter[1] = 'E' then // 返回值以E开头，表示值有问题，直接退出
  begin
    application.MessageBox(Pchar(filter), '错误');
    exit;
  end;

  try
    // 主查询
    SQL := Format(MasterSQL, [CurrentZT + '.', filter]);

    RunCDS(SQL);

    if Cds.RecordCount = 0 then
      application.MessageBox('没有查询到相关记录，请修改查询条件后继续！', '提示')
    else
      FillTreeList(Cds);
    ResizeForm;
  finally
    self.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
// 将汇总值导入excel
// 将汇总值导入excel
var
  str: string;
  excelapp, sheet: Variant;
  i: Integer;
  j: Integer;

  function S2E(Col: Integer): Boolean;
  // 确定哪些字段值不导入到excel中
  begin
    Result := (Col > 8);
    Result := not Result;
  end;

begin
  screen.Cursor := crAppStart;
  str := '';

  // 项目筛选记录
  str := '筛选项目编号包括 ： ' + char(9) + StringReplace(xmLE.Text, ',', char(9),
    [rfReplaceAll]) + #13 + #13;
  // 时间范围
  str := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date) +
    char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date) + #13
    + #13 + #13;

  for i := 0 to MTreeL.ColumnCount - 1 do
  // 显示列标题
  begin
    if S2E(i) then
      str := str + MTreeL.Columns[i].Caption.Text + char(9);
  end;
  str := str + #13;

  // 显示内容
  MTreeL.Enabled := false;
  for i := 0 to MTreeL.Count - 1 do
  begin
    // 若不是汇总项，刚直接跳过 汇总项判断：GRID单号列字值为'-'
    if MTreeL.Items[i].Texts[11] <> SPCHAR then
      continue;
    for j := 0 to MTreeL.ColumnCount - 1 do
    begin
      if S2E(j) then
        str := str + MTreeL.Items[i].Texts[j] + char(9);
    end;
    str := str + #13;
    application.ProcessMessages;
  end;

  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := True;
    except
      application.MessageBox('Excel未安装', '提示');
    end;
  finally
    clipboard.Clear;
    MTreeL.Enabled := True;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
// 将明细表值导入excel
var
  str: string;
  excelapp, sheet: Variant;
  j: Integer;
begin
  screen.Cursor := crAppStart;
  str := '';

  // 项目筛选记录
  str := '筛选项目编号包括 ： ' + char(9) + StringReplace(xmLE.Text, ',', char(9),
    [rfReplaceAll]) + #13 + #13;
  // 时间范围
  str := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date) +
    char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date) + #13
    + #13 + #13;

  str := str + '料号' + char(9) + '品号' + char(9) + '规格' + char(9) + '数量' + char(9)
    + '数量单位' + char(9) + '重量' + char(9) + '重量单位' + char(9) + '过账日期' + char(9) +
    '领料单号' + char(9) + '单别' + char(9) + '项次' + char(9) + '来源公司' + char(9) +
    '仓库编号' + char(9) + '仓库名称' + char(9) + '包号' + char(9) + '项目编号' +
    char(9) + '项目名称';

  str := str + #13;

  // 显示内容
  Cds.First;
  while not Cds.Eof do
  begin
    if Cds.FieldByName('imn02').AsInteger = 0 then
    begin
      Cds.Next;
      continue;
    end;
    for j := 0 to Cds.FieldCount - 2 do
    begin
      str := str + Cds.Fields[j].AsString + char(9);
    end;
    str := str + #13;
    Cds.Next;
    application.ProcessMessages;
  end;
  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := True;
      screen.Cursor := crDefault;
    except
      showmessage('Excel未安装');
    end;
  finally
    clipboard.Clear;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.FullRTTree(ARecords: TstringList);
var
  i, j: Integer;
  AValues: TstringList;
  lNode, xNode, bNode, cNode: TcxTreeListNode; // 分别带表料/项目/包号/当前节点
  NRoot: Boolean;
  pLev, cLev: Integer;
begin
  AValues := TstringList.Create;
  AValues.StrictDelimiter := True; // bug修复，不让空格自动分割行
  AValues.Delimiter := TreeSplit;
  NRoot := false;
  MTreeL.BeginUpdate;

  lNode := MTreeL.add;
  cNode := lNode;
  pLev := 1;
  cLev := 1;
  for j := 0 to ARecords.Count - 1 do
  begin
    AValues.DelimitedText := ARecords[j];
    if NRoot then
    // 其包含关系在SQL获取的记录中保存，所有返回记录的顺序非常关键
    begin
      try
        cLev := strtoint(AValues[AValues.Count - 1]);
      except
        cLev := 4;
      end;
      case cLev of
        1:
          begin
            cNode := MTreeL.add;
            lNode := cNode;
          end;
        2:
          begin
            cNode := lNode.AddChild;
            xNode := cNode;
          end;
        3:
          begin
            cNode := xNode.AddChild;
            bNode := cNode;
          end;
        4:
          begin
            cNode := bNode.AddChild;
          end;
      end;
    end;
    for i := 0 to AValues.Count - 1 do
      cNode.Texts[i] := AValues[i];
    NRoot := True;
  end;
  MTreeL.EndUpdate;

  AValues.Free;
end;

procedure TWorkForm.FillTreeList(Q: TClientDataSet);
var
  aRecord: TstringList;
begin
  MTreeL.Clear;

  // 将所有记录转换到StringList中
  aRecord := TstringList.Create;
  aRecord.Clear;
  Q.First;
  while not Q.Eof do
  begin
    aRecord.add(Q.FieldByName('imn03').AsString + TreeSplit +
      Q.FieldByName('ima02').AsString + TreeSplit + Q.FieldByName('ima021')
      .AsString + TreeSplit + Q.FieldByName('imn42').AsString + TreeSplit +
      Q.FieldByName('ima25').AsString + TreeSplit + Q.FieldByName('imn45')
      .AsString + TreeSplit + Q.FieldByName('ima907').AsString + TreeSplit +
      Q.FieldByName('imm14').AsString + TreeSplit + Q.FieldByName('gem02')
      .AsString + TreeSplit + Q.FieldByName('immud02').AsString + TreeSplit +
      Q.FieldByName('imm17').AsString + TreeSplit + Q.FieldByName('imm01')
      .AsString + TreeSplit + Q.FieldByName('imn02').AsString + TreeSplit +
      Q.FieldByName('imn06').AsString + TreeSplit + Q.FieldByName('imn15')
      .AsString + TreeSplit + Q.FieldByName('imd02').AsString + TreeSplit +
      Q.FieldByName('immud03').AsString + TreeSplit + Q.FieldByName('LEV')
      .AsString);
    Q.Next;
  end;
  FullRTTree(aRecord);
  aRecord.Free;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      XmCount(zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
