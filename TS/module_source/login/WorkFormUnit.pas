﻿//
// WorkFormUnit.pas -- 工作窗体单元（应用程序员自行修改窗体内容）
// Version 1.00 Ansi Edition
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit;

interface

uses
  winapi.Windows, StdCtrls, Forms, Buttons, Controls, VCL.IMAGING.jpeg,
  ExtCtrls, Classes,
  QBParcel, UserConnection, QBJson, ComCtrls, ImgList, FileTransfer, DbAccessor,
  System.SysUtils, IdHashSHA, IdGlobal, System.StrUtils, System.DateUtils,
  DBClient,
  RemoteUniDac, QBAes, QBMisc, Data.DB;

type
  TWorkForm = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    Bevel1: TBevel;
    Label1: TLabel;
    Image1: TImage;
    Edit1: TEdit;
    Edit2: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    dba: TRemoteUniDac;
    CDS: TClientDataSet;
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    function IsOK: boolean;
    function ZHOk(UID : string): Boolean;
  public
    { Public declarations }
    //
    // 必须有的四个接口对象...
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

var
  WorkForm: TWorkForm;

implementation

{$R *.dfm}

// ------校核系统是否有效代码----------------------------------------------------
const
  A = 'OK';
  key = '@#25^23sdfDD';

function SHA1(Input: String): String;
begin
  with TIdHashSHA1.Create do
    try
      Result := LowerCase(HashBytesAsHex(TidBytes(Bytesof(Input))));
    finally
      Free;
    end;
end;

function TWorkForm.IsOK: boolean;
var
  CDS: TClientDataSet;
  SDate, CDate: TDate;
  cy, cm, cd: Word;
  s5, s6: string;
  AA: string; // 用于匹配的字符串
begin
  dba.TargetDatabaseId := 'ERP';
  Result := false;
  AA := ReverseString(SHA1(A));
  try
    CDS := nil;
    CDS := TClientDataSet.Create(nil);
    CDS.Close;
    if not dba.ReadDataset
      ('SELECT smaud04,smaud05,SYSDATE ST FROM pnjt.sma_file', CDS) then
      exit(false)
    else
      CDS.active := true;

    s5 := CDS.FieldByName('smaud04').AsString;
    s6 := CDS.FieldByName('smaud05').AsString; // 字符格式为：20181001
    SDate := CDS.FieldByName('ST').AsDateTime; // 服务器当前时间
    CDS.Close;
    s6 := AesDecrypt(ReverseString(s6), key);
    cy := StrToint(copy(s6, 1, 4));
    cm := StrToint(copy(s6, 5, 2));
    cd := StrToint(copy(s6, 7, 2));
    CDate := EncodeDate(cy, cm, cd);
  finally
    FreeAndNil(CDS);
  end;
  if s5 = AA then
    exit(true); // 验证正常,直接退出
  if (s5 = '') OR (s6 = '') then
    exit(false);
  if (CDate = 0) or (SDate = 0) then
    exit(false);
  if CDate > SDate then
    exit(True);
end;
// ------------------------------------------------------------------------------

function TWorkForm.ZHOk(UID : string) : Boolean; //用户密码判断
var
S : string;
begin
  dba.TargetDatabaseId := 'U_TS';
  CDS.Active := false;
    if not dba.ReadDataset
      ('SELECT PD FROM rht_user WHERE ID = ''' + UID + '''', CDS) then
      exit(false)
    else
      CDS.active := true;
  S := CDS.FieldByName('PD').Text;
  if (S = '') and (trim(edit2.Text) = '') then exit(true);
  if S = ReverseString(SHA1(Trim(edit1.Text) + (edit2.Text))) then    //加密信息＝账号+密码（避免不同账号同密码，PD字段值相同）
    result := true
    else
    result := false;
end;

procedure TWorkForm.BitBtn1Click(Sender: TObject);
begin
  if trim(Edit1.Text) = '' then
  begin
    showError('请录入用户账号', 'Error');
    exit;
  end;
  if Not ZhOK(trim(edit1.Text)) then
  begin
    showError('输入密码错误！','Error');
    exit;
  end;
  OutputParcel.PutBooleanGoods('ProcessResult', true);
  OutputParcel.PutStringGoods('DLL_Key', 'login_dll');
  OutputParcel.PutStringGoods('LoginUser', trim(Edit1.Text));
  OutputParcel.PutBooleanGoods('SoftWareAuth', IsOK);
  Close;
end;

procedure TWorkForm.BitBtn2Click(Sender: TObject);
begin
  OutputParcel.PutBooleanGoods('ProcessResult', false);
  Close;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.UserConn := UserConn;
  dba.TargetDatabaseId := 'ERP';
  Edit1.Text := '';
  Edit2.Text := '';

  if fileexists('modules\login.jpg') then
    Image1.Picture.LoadFromFile('modules\login.jpg');
end;

end.
