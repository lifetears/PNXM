﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd, Vcl.dialogs, strUtils,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, CRGrid, Vcl.ExtDlgs;

type
  TWorkForm = class(TForm)
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    xmLE: TLabeledEdit;
    RunQuery: TButton;
    Toxls: TButton;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    PrivilegeCDS: TClientDataSet;
    MGrid: TCRDBGrid;
    DataSource1: TDataSource;
    Label1: TLabel;
    GDCDS: TClientDataSet;
    Memo1: TMemo;
    SFD: TSaveTextFileDialog;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure MGridDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    procedure SetTimeRange;
    function RunCDS(SQL: string; CS: TClientDataSet): boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure GridToxls;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CBYXXWJS_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';
  KeyCol = 11; // 在GRID中用于区别汇总与明细的列
  // Grid列表标题
  XCCC = 9; // 现场库存标题列数

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  //
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;
end;

// -----------------------------------------------------------------------------
// 业务代码
// SELECT t.gen02,Yr,mth,azf03,imaud02,imaud03,ogb092,s.gen02,k.occ18,ogaud03,
// (CASE  WHEN TA_gem21 IN ('1','2') THEN '炼铁系统'
// WHEN TA_gem21 IN ( '3','4','6') THEN '钢包系统'
// WHEN TA_gem21 IN ('7') THEN '中间包系统'
// ELSE '其他系统' END) xmfl,
// gem02,oga01,ogb03,ogaud04,ogaconf,ogapost,ogb04,ima02,ima021,ogb912,ogb910,
// round(ogb912*dz,3) zl,
// ogb913,ogb60,xsdj, xse FROM
// (
// SELECT ogaoriu,oga01,oga02,YEAR(oga02) Yr,MONTH(oga02) mth,oga03,oga18,
// decode(ogaud04,'1','承包','2','单结')
// ogaud04,ogaconf,ogapost,ogb03,ogaud03,ogb04,
// SUBSTR(ogb092,1,INSTR(ogb092,'-',1)-1) ogb092,
// (ogb16-ogb60) ogb912,
// ogb910,ogb915,
// ogb915/decode(ogb912,0,1,ogb912) dz,
// ogb913,ogb60,
// round(ta_ogb16,2) xsdj,
// round(ta_ogb16*(ogb16 - ogb60),2) xse
// FROM oga_file,ogb_file
// WHERE ogb01 = oga01 AND ogb16 > ogb60 AND oga09 ='3' AND ogapost = 'Y' AND oga03 IN (SELECT occ01 FROM occ_file WHERE occ07 =  '1000260')
// ) tt
// LEFT JOIN ima_file ON ima01 = ogb04
// LEFT JOIN Azf_file ON azf01 = ima09
// LEFT JOIN occ_file k ON k.occ01 = oga18
// LEFT JOIN gem_file   ON gem01 = ogaud03
// LEFT JOIN gen_file t ON t.gen01 = ogaoriu
// LEFT JOIN gen_file s ON s.gen01 = occ04
Const
  XCCN = ',项目编号,年度,月份,项次,料件,单位,来源公司,标准成本';

  // 查询用户负责的项目编号集
  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';
  // 查询主SQL
  MSQL = 'SELECT rowNum,t.gen02 结算员,Yr 年,mth 月,azf03 标准名称,' + slinebreak +
    'imaud02 牌号,imaud03 配方,ogb092 来源公司,s.gen02 业务员,' + slinebreak +
    'k.occ18 客户全称,ogaud03 项目编号, ' + slinebreak +
    '(CASE  WHEN TA_gem21 IN (''1'',''2'') THEN ''炼铁系统''' + slinebreak +
    '       WHEN TA_gem21 IN ( ''3'',''4'',''6'') THEN ''钢包系统''' + slinebreak +
    '       WHEN TA_gem21 IN (''7'') THEN ''中间包系统''' + slinebreak +
    '       ELSE ''其他系统'' END) 项目类型, ' + slinebreak +
    'gem02 项目名称,oga01 单号,ogb03 项次,ogaud04 单据类型,ogaconf 是否审核,' + slinebreak +
    'ogapost 是否过账,ogb04 料号,ima02 产品名称,ima021 产品规格,' + slinebreak +
    'ogb912 未结数量,ogb910 单位,' + slinebreak + 'round(ogb912*dz,3) 未结重量,   ' +
    slinebreak + 'ogb913 单位,ogb60 已结数量,xsdj 销售单价, xse 销售额' + slinebreak +
    'FROM   ' + slinebreak + '(      ' + slinebreak +
    '  SELECT ogaoriu,oga01,oga02,YEAR(oga02) Yr,MONTH(oga02) mth,oga03,oga18,'
    + slinebreak + '    decode(ogaud04,''1'',''承包'',''2'',''单结'') ' +
    '    ogaud04,ogaconf,ogapost,ogb03,ogaud03,ogb04,' + slinebreak +
    '    SUBSTR(ogb092,1,INSTR(ogb092,''-'',1)-1) ogb092,' + slinebreak +
    '    (ogb16-ogb60) ogb912,                         ' + slinebreak +
    '    ogb910,ogb915,                                ' + slinebreak +
    '    ogb915/decode(ogb912,0,1,ogb912) dz,          ' + slinebreak +
    '    ogb913,ogb60,                                 ' + slinebreak +
    '    round(ta_ogb16,2) xsdj,                       ' + slinebreak +
    '    round(ta_ogb16*(ogb16 - ogb60),2) xse         ' + slinebreak +
    ' FROM oga_file,ogb_file                           ' + slinebreak +
    ' WHERE ogb01 = oga01 AND ogb16 > ogb60 AND ' + slinebreak +
    'oga09 =''3'' AND ogapost = ''Y'' AND %s ' + slinebreak +
    'AND oga03 IN (SELECT occ01 FROM occ_file WHERE occ07 IN (%s))' + slinebreak
    + ') tt   ' + slinebreak + 'LEFT JOIN ima_file ON ima01 = ogb04      ' +
    slinebreak + 'LEFT JOIN Azf_file ON azf01 = ima09      ' + slinebreak +
    'LEFT JOIN occ_file k ON k.occ01 = oga18  ' + slinebreak +
    'LEFT JOIN gem_file   ON gem01 = ogaud03  ' + slinebreak +
    'LEFT JOIN gen_file t ON t.gen01 = ogaoriu ' + slinebreak +
    'LEFT JOIN gen_file s ON s.gen01 = occ04   ';

var
  xmnF: string; // 客户编号
  sjF: string; // 时间筛选条件
  bmonth, bYear: word; // 过滤条件中使用到的年与月份

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
begin
  // 与其他dll中的settimeRange不同，本函数作用是获取查询的耗量清单的期别，比其他dll中的功能要少很多
  bYear := strtointdef((qbYear.Text), YearOf(now));
  bmonth := qbmon.ItemIndex + 1;
end;

function TWorkForm.RunCDS(SQL: string; CS: TClientDataSet): boolean;
begin
  Result := false;
  CS.close;
  if not dba.ReadDataset(SQL, CS) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    CS.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

function TWorkForm.GetFilterS: string;
// 返回动态筛选条件
var
  FS: TstringList;
  I: Integer;
  dt: Tdate;
  s, ss: string;
begin
  Result := '';
  if length(xmLE.Text) < 4 then
  begin
    exit('不是有效的客户编号');
  end;
  if xmLE.Text = '' then
  begin
    exit('客户编号不能为空');
  end;
  FS := TstringList.Create;
  FS.DelimitedText := xmLE.Text;
  for I := 0 to FS.Count - 1 do
    FS[I] := '''' + FS[I] + '''';
  xmnF := FS.DelimitedText;
  FS.Free;

  SetTimeRange;

  // 当月份选择“历史全部”时 qbmon.itemindex = 13
  if bmonth = 14 then
  begin
    sjF := ' 1 = 1 ';
    exit;
  end;
  // 当月份选择“本年全部时”  qbmon.itemindex = 12
  if bmonth = 13 then
  begin
    s := bYear.ToString + '0101';
    sjF := ' OGA02 >= TO_DATE(''' + s + ''',''yyyymmdd'')';
    exit;
  end;
  dt := incDay(encodeDate(bYear, bmonth + 1, 1), -1); // 获取当月的最后一天
  sjF := ' OGA02 BETWEEN TO_DATE(''' + FormatDateTime('yyyymmdd',
    StartOfTheMonth(dt)) + ''',''yyyymmdd'') AND ' + ' TO_DATE(''' +
    FormatDateTime('yyyymmdd', dt) + ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
// 查寻
var
  filter, SQL: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;

  // 主查询
  screen.Cursor := crSQLWait;
  filter := GetFilterS;
  if filter <> '' then
  begin
    screen.Cursor := crDefault;
    application.MessageBox(Pchar(filter), '提示');
    exit;
  end;
  try
    SQL := Format(MSQL, [sjF, xmnF]);

    // Memo1.Lines.Text := SQL;
    // exit;
    RunCDS(SQL, GDCDS);

    if GDCDS.RecordCount = 0 then
      application.MessageBox('该客户没有已下线未结算记录！', '提示')
    else
      FillmGrid(GDCDS);
    ResizeForm;
  finally
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.GridToxls;
var
  s: TstringList;
  str, fileName: string;
  I: Integer;
begin
  str := '';
  MGrid.DataSource.DataSet.DisableControls;
  for I := 0 to MGrid.DataSource.DataSet.FieldCount - 1 do
    if MGrid.DataSource.DataSet.fields[I].Visible then
      str := str + MGrid.DataSource.DataSet.fields[I].DisplayLabel + char(9);
  str := str + #13;
  MGrid.DataSource.DataSet.First;
  while not(MGrid.DataSource.DataSet.eof) do
  begin
    for I := 0 to MGrid.DataSource.DataSet.FieldCount - 1 do
      if MGrid.DataSource.DataSet.fields[I].Visible then
        str := str + MGrid.DataSource.DataSet.fields[I].AsString + char(9);

    str := str + #13;
    MGrid.DataSource.DataSet.next;
  end; // end  while

  MGrid.DataSource.DataSet.EnableControls;

  if SFD.Execute then
    fileName := SFD.fileName;

  if fileName = '' then
  begin
    showmessage('请选择正确的文件名继续');
    exit;
  end;

  s := TstringList.Create;
  s.Add(str);
  s.SaveToFile(fileName);
  s.Free;
  showmessage('导入成功！');
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
begin
  GridToxls;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
begin
  GridToxls;
end;

procedure TWorkForm.MGridDrawDataCell(Sender: TObject; const Rect: TRect;
  Field: TField; State: TGridDrawState);
begin
  if (GDCDS.FieldByName('rowNum').AsInteger mod 2 > 0) then
  begin
    MGrid.Canvas.Brush.Color := clInfoBk; // 背景色
    MGrid.Canvas.FillRect(Rect);
  end;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  I, j: Integer;
  s: string;
begin
  GDCDS.FieldByName('rowNum').Visible := false;
  GDCDS.FieldByName('结算员').DisplayWidth := 8;
  GDCDS.FieldByName('年').DisplayWidth := 6;
  GDCDS.FieldByName('月').DisplayWidth := 6;
  GDCDS.FieldByName('标准名称').DisplayWidth := 16;
  GDCDS.FieldByName('牌号').DisplayWidth := 12;
  GDCDS.FieldByName('配方').DisplayWidth := 12;
  GDCDS.FieldByName('来源公司').DisplayWidth := 10;
  GDCDS.FieldByName('业务员').DisplayWidth := 8;
  GDCDS.FieldByName('客户全称').DisplayWidth := 30;
  GDCDS.FieldByName('项目编号').DisplayWidth := 8;
  GDCDS.FieldByName('项目类型').DisplayWidth := 16;
  GDCDS.FieldByName('项目名称').DisplayWidth := 30;
  GDCDS.FieldByName('单号').DisplayWidth := 18;
  GDCDS.FieldByName('项次').DisplayWidth := 4;
  GDCDS.FieldByName('单据类型').DisplayWidth := 8;
  GDCDS.FieldByName('是否审核').DisplayWidth := 4;
  GDCDS.FieldByName('是否过账').DisplayWidth := 4;
  GDCDS.FieldByName('料号').DisplayWidth := 16;
  GDCDS.FieldByName('产品名称').DisplayWidth := 30;
  GDCDS.FieldByName('产品规格').DisplayWidth := 30;
  GDCDS.FieldByName('未结数量').DisplayWidth := 10;
  GDCDS.FieldByName('单位').DisplayWidth := 4;
  GDCDS.FieldByName('未结重量').DisplayWidth := 10;
  GDCDS.FieldByName('单位').DisplayWidth := 4;
  GDCDS.FieldByName('已结数量').DisplayWidth := 10;
  GDCDS.FieldByName('销售单价').DisplayWidth := 10;
  GDCDS.FieldByName('销售额').DisplayWidth := 10;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}

// 消息响应事件
// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
