object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 886
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 886
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 884
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel2: TBevel
        Left = 0
        Top = 83
        Width = 884
        Height = 5
        Align = alBottom
        Shape = bsBottomLine
        Style = bsRaised
        ExplicitLeft = -32
        ExplicitTop = 53
        ExplicitWidth = 1029
      end
      object Label1: TLabel
        Left = 304
        Top = 21
        Width = 36
        Height = 12
        Caption = 'Label1'
        Visible = False
      end
      object xmLE: TLabeledEdit
        Left = 142
        Top = 17
        Width = 139
        Height = 20
        CharCase = ecUpperCase
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #25910#27454#23458#25143#32534#21495#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        TabOrder = 0
      end
      object RunQuery: TButton
        Left = 440
        Top = 26
        Width = 82
        Height = 35
        Caption = #26597'  '#35810
        TabOrder = 1
        OnClick = RunQueryClick
      end
      object Toxls: TButton
        Left = 552
        Top = 25
        Width = 82
        Height = 35
        Caption = #25968#25454#23548#20986
        TabOrder = 2
        OnClick = ToxlsClick
      end
      object TimeP: TPanel
        Left = 42
        Top = 43
        Width = 247
        Height = 41
        BevelOuter = bvNone
        TabOrder = 3
        object Label2: TLabel
          Left = 12
          Top = 11
          Width = 144
          Height = 12
          Caption = #26597#35810#24180#24230'            '#26376#20221
        end
        object qbYear: TEdit
          Left = 75
          Top = 9
          Width = 49
          Height = 20
          NumbersOnly = True
          TabOrder = 0
          Text = 'qbYear'
          OnExit = qbYearExit
        end
        object qbmon: TComboBox
          Left = 162
          Top = 8
          Width = 77
          Height = 22
          Style = csOwnerDrawFixed
          TabOrder = 1
          OnClick = qbmonClick
          Items.Strings = (
            #19968#26376
            #20108#26376
            #19977#26376
            #22235#26376
            #20116#26376
            #20845#26376
            #19971#26376
            #20843#26376
            #20061#26376
            #21313#26376
            #21313#19968#26376
            #21313#20108#26376
            #24403#24180#20840#37096
            #21382#21490#20840#37096)
        end
      end
    end
    object MGrid: TCRDBGrid
      Left = 1
      Top = 89
      Width = 884
      Height = 499
      Margins.Right = 30
      Align = alClient
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #23435#20307
      TitleFont.Style = []
      OnDrawDataCell = MGridDrawDataCell
    end
    object Memo1: TMemo
      Left = 656
      Top = 168
      Width = 105
      Height = 161
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
      Visible = False
    end
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 248
    Top = 294
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 304
  end
  object DataSource1: TDataSource
    DataSet = GDCDS
    Left = 328
    Top = 288
  end
  object GDCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 200
    Top = 352
  end
  object SFD: TSaveTextFileDialog
    DefaultExt = 'xls'
    Left = 88
    Top = 320
  end
end
