﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxStyles, cxClasses,
  tmsAdvGridExcel;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    ToXlsMX: TButton;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    MGrid: TAdvStringGrid;
    agexp: TAdvGridExcelIO;
    pnl4: TPanel;
    Q_ima01: TLabeledEdit;
    cbb1: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure endTExit(Sender: TObject);
    procedure endTClick(Sender: TObject);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure qxmClick(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet; tp: Integer = 1);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure WaitMessage;
    procedure FillGridHeader;
    function GetPh: string;
    procedure GetKH;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CBYXH_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';

var
  zhanghu: string;
  CurrentZT: string;
  vMonth : integer;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  // 获取插件formCaption
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  //前几个月的默认值
  VMonth := 4;

  // 默认为当天
  endT.Date := now;
  beginT.Date := StartofTheMonth(incMonth(now,1 - Vmonth));

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  TimeP.Visible := false;

  FillGridHeader;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const

  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';

MSQL =
'SELECT imn03,ima02,ima021,imaud03,imn04,GS,occ18,MH,s32, s35 FROM ' + slinebreak +
'(SELECT imn03,imn04,GS, MH,sum(imn32) s32,sum(imn35) s35 FROM ' + slinebreak +
'  (SELECT imn03,imn04,imn06,substr(imn06,1,INSTR(imn06,''-'',1)-1) GS, ' + slinebreak +
'       imm17,to_char(imm17,''YYYY/MM'') MH,imn32,imn35 ' + slinebreak +
'    FROM imm_file ,imn_file  ' + slinebreak +
'      WHERE imm01 = imn01 AND regexp_like(imm01,''^CL01'') AND ' + slinebreak +
'       %s AND --料件条件  ' + slinebreak +
'       %s AND --仓库条件   ' + slinebreak +
'       %s AND --imm17过账日期条件  ' + slinebreak +
'       immconf = ''Y'' AND imm03 = ''Y'' AND imn04 > ''10000'' ) dd ' + slinebreak +
'   GROUP BY imn03,imn04,MH,GS) tt ' + slinebreak +
' LEFT JOIN ima_file ON ima01 = tt.imn03 ' + slinebreak +
' LEFT JOIN occ_file ON occ01 = tt.imn04 ' + slinebreak +
' ORDER BY 1,2,3,4 DESC';

  headStr: array [0 .. 7] of string = ('序号', '料件编号', '料件名称','料件规格','配方','仓库','来源公司','客户简称');

var
  xmNF,ljlh, zqTimeF: string; // 客户,料件,时间的过滤条件

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

function TWorkForm.GetPh: string;
// 获取品号筛选条件
var
  s: string;
begin
  s := Trim(Q_ima01.Text);
  if s = '' then
    Exit(' 1=1 ');
  case cbb1.ItemIndex of
    0:
      result := ' IMN03 = ''' + s + '''';
    1:
      result := ' IMN03 LIKE ''%' + s + '%''';
    2:
      result := ' IMN03 LIKE ''' + s + '%''';
    3:
      result := ' IMN03 LIKE ''%' + s + ''''
    else
      result := ' 1=1 ';
  end;
end;

procedure TWorkForm.GetKH;
begin
  //客户编号，在单据中为拨出仓库
  if xmLE.Text = '' then
  begin
    xmNF := ' 1=1 ';
  end
  else
  begin
    xmNF := StringReplace(trim(xmLE.Text), DChar, '''' + DChar + '''', [rfReplaceAll]);
    xmNF := ' imn04 IN (''' + xmNF + ''') ';
  end;
end;

function TWorkForm.GetFilterS: string;
var
  bt, et: Tdate;
begin
  Result := '';
  GetKH;
//料件筛选
  ljlh := GetPh;
//  时间筛选
  bt := beginT.Date;
  et := endT.Date;

  if et < bt then
  begin
    exit('Error : 结束日期不可小于开始时间！');
  end;

  zqTimeF := ' imm17 BETWEEN to_date(''' + FormatDateTime('yyyymmdd', bt) +
    ''',''yyyymmdd'') AND ' + ' to_date(''' + FormatDateTime('yyyymmdd', et) +
    ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  SQL: string;
  E: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;
  // 主查询
  MGrid.RowCount := 2;
  screen.Cursor := crSQLWait;
  E := GetFilterS;
  try
    if E <> '' then
    begin
      screen.Cursor := crDefault;
      application.MessageBox(Pchar(E), '提示');
      exit;
    end;
    SQL := Format(MSQL, [xmNF,ljlh,zqTimeF]);
//     memo1.Text := sql;
//     exit;
    RunCDS(SQL);
    if Cds.RecordCount > 0 then
      FillmGrid(Cds);
  finally
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
      agexp.XLSExport(SaveDialog.FileName + '.xls');
      showmessage('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet; tp: Integer = 1);
var
  i, w, wc, R: Integer;
  xm, Ts: string;
  Flag: string;
  sl,zl : string;
  xmList, wkList,MonthList: TstringList;
begin
  MonthList := TstringList.Create;
  MonthList.Sorted := True;
  MonthList.Duplicates := dupAccept;
  xmList := TstringList.Create;
  xmList.Sorted := True;
  xmList.Duplicates := dupIgnore;
  wkList := TstringList.Create;
  wkList.Sorted := True;
  wkList.Duplicates := dupIgnore;
  //为避免当前月没有领料时不显示当前月记录，此处强制加一条当月标记
  wkList.Add(FormatDatetime('YYYY/MM',endT.Date));
  Q.First;
  while not Q.Eof do
  begin
    xmList.Add(Q.FieldByName('imn03').AsString + Q.FieldByName('imn04').AsString + Q.FieldByName('GS').AsString);
    wkList.Add(Q.FieldByName('MH').AsString);
    Q.Next;
  end;
//  // 若月数不够vmonth数，补齐
  MonthList.DelimitedText := wkList.DelimitedText;
  wc := MonthList.Count;
  for i := wc to VMonth  do
    MonthList.add('-');
  try
    MGrid.BeginUpdate;
    // 填充表头
    MGrid.RowCount := xmList.Count + 2;
    MGrid.FixedRows := 2;
    wc := MonthList.Count;
    MGrid.Cells[8, 0] := MonthList[wc - 1];
    MGrid.Cells[10, 0] := MonthList[wc - 2];
    MGrid.Cells[12, 0] := MonthList[wc - 3];
    MGrid.Cells[14, 0] := MonthList[wc - 4];
    // 填充数据区
    // Mgrid.RowCount := ;
    xm := '';
    Ts := '';
    i := 2;

    Q.First;
    while not Q.Eof do
    begin
      Flag := Q.FieldByName('imn03').AsString + Q.FieldByName('imn04').AsString + Q.FieldByName('GS').AsString;
      if xm <> Flag then
      begin
        MGrid.Cells[0, i] := inttostr(i - 1);
        xm := Flag;
        MGrid.Cells[1, i] := Q.Fields.Fields[0].AsString;
        MGrid.Cells[2, i] := Q.Fields.Fields[1].AsString;
        MGrid.Cells[3, i] := Q.Fields.Fields[2].AsString;
        MGrid.Cells[4, i] := Q.Fields.Fields[3].AsString;
        MGrid.Cells[5, i] := Q.Fields.Fields[4].AsString;
        MGrid.Cells[6, i] := Q.Fields.Fields[5].AsString;
        MGrid.Cells[7, i] := Q.Fields.Fields[6].AsString;
        inc(i);
      end;
      sl := Q.FieldByName('s32').AsString;
      zl := Q.FieldByName('s32').AsString;
      Ts := Q.FieldByName('MH').AsString;
      w := MonthList.IndexOf(Ts);
      // 如果是四周以前数据，则直接跳过
      if (wc - 1 - w) >= 4 then
      begin
        Q.Next;
        continue;
      end;

      R := 8 + (wc - 1 - w) * 2;

      MGrid.Cells[R, i - 1] := sl;
      MGrid.Cells[R + 1,i - 1] := zl;
      Q.Next;
    end;
    MGrid.AutoSizeColumns(false, 5);
  finally
    MGrid.EndUpdate;
    wkList.Free;
    xmList.Free;
    MonthList.Free;
  end;
end;

procedure TWorkForm.endTClick(Sender: TObject);
begin
  beginT.Date := StartofTheMonth(incMonth(EndT.Date,1 - Vmonth));
end;

procedure TWorkForm.endTExit(Sender: TObject);
begin
  beginT.Date := StartofTheMonth(incMonth(EndT.Date,1 - Vmonth));
end;

procedure TWorkForm.FillGridHeader;
begin
  MGrid.MergeCells(0, 0, 1, 2);
  MGrid.MergeCells(1, 0, 1, 2);
  MGrid.MergeCells(2, 0, 1, 2);
  MGrid.MergeCells(3, 0, 1, 2);
  MGrid.MergeCells(4, 0, 1, 2);
  MGrid.MergeCells(5, 0, 1, 2);
  MGrid.MergeCells(6, 0, 1, 2);
  MGrid.MergeCells(7, 0, 1, 2);
  MGrid.MergeCells(8, 0, 2, 1);
  MGrid.MergeCells(10, 0, 2, 1);
  MGrid.MergeCells(12, 0, 2, 1);
  MGrid.MergeCells(14, 0, 2, 1);
  MGrid.Cells[0, 0] := headStr[0];
  MGrid.Cells[1, 0] := headStr[1];
  MGrid.Cells[2, 0] := headStr[2];
  MGrid.Cells[3, 0] := headStr[3];
  MGrid.Cells[4, 0] := headStr[4];
  MGrid.Cells[5, 0] := headStr[5];
  MGrid.Cells[6, 0] := headStr[6];
  MGrid.Cells[7, 0] := headStr[7];
  MGrid.Cells[8, 1] := '数量';
  MGrid.Cells[9, 1] := '重量';
  MGrid.Cells[10, 1] := '数量';
  MGrid.Cells[11, 1] := '重量';
  MGrid.Cells[12, 1] := '数量';
  MGrid.Cells[13, 1] := '重量';
  MGrid.Cells[14, 1] := '数量';
  MGrid.Cells[15, 1] := '重量';
end;

procedure TWorkForm.MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 2 then
  begin
    HAlign := taCenter;
    VAlign := vtaCenter;
  end;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if ARow > 1 then
  begin
    if (ARow mod 2) = 0 then
      ABrush.Color := MGrid.Color
    else
      ABrush.Color := clBtnFace;
    if (MGrid.Cells[0, ARow] <> '') and (MGrid.Cells[ACol, ARow] = '') then
      ABrush.Color := clYellow;
  end;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin

end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息  同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
