﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, CRGrid, Vcl.ExtDlgs;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    FlowPanel1: TFlowPanel;
    Panel2: TPanel;
    Label2: TLabel;
    BeginDate: TDateTimePicker;
    EndDate: TDateTimePicker;
    Panel3: TPanel;
    Bevel2: TBevel;
    Label1: TLabel;
    Bevel5: TBevel;
    CBS: TComboBox;
    Edit1: TEdit;
    Panel4: TPanel;
    Bevel3: TBevel;
    Bevel6: TBevel;
    khjc: TLabeledEdit;
    Panel5: TPanel;
    Bevel4: TBevel;
    Bevel7: TBevel;
    jhygh: TLabeledEdit;
    Panel7: TPanel;
    Bevel8: TBevel;
    Bevel9: TBevel;
    jhyxm: TLabeledEdit;
    Panel6: TPanel;
    Button1: TButton;
    Button2: TButton;
    CRDBGrid1: TCRDBGrid;
    SFD: TSaveTextFileDialog;
    DataSource1: TDataSource;
    Panel8: TPanel;
    bvl1: TBevel;
    bvl2: TBevel;
    rg1: TRadioGroup;
    Panel9: TPanel;
    rg2: TRadioGroup;
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CRDBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    procedure GetAuth(key: AnsiString);
    function GetFilterStr: string;
    procedure ResizeForm;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'LYLingLiao_DLL';
  // 业务代码
  SPCHAR = '-';
  TreeSplit = '@';
  DefaultZT = 'GWGS';
  tlev = 16; // 记录lev的列次
  tLJ = 0; // 记录料件的列次
  tXM = 7; // 记录项目的列次
  tBH = 9; // 记录包号的列次
  // 查询用户负责的项目编号集

var
  zhanghu: string;
  softWareAuth: Boolean;
  // ----------------------------------------------------------------------------
  //

procedure TWorkForm.FormShow(Sender: TObject);
begin
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);

  // 界面初始化
  // 默认为当天
  BeginDate.Date := now;
  EndDate.Date := now;

  ResizeForm;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  SQL = ' SELECT rownum,tc_gen03 AS 公司,decode(SUBSTR(oeaorig,4,1),''3'',''国内'',''国外'') 订单性质,sfb01 AS 工单,gem02 AS 生产车间,预计开工日,ima01 AS 品号,ima02 AS 品名,ima021 AS 规格,imaud02 AS 牌号, '
    + #13#10 +
    ' imaud03 AS 配方,imaud05 AS 包装方式,DECODE(NVL(smd04,0),0,0,NVL(smd06,0)/smd04) AS 单重,imd02 AS 仓库,occ02 AS 客户,dddh 订单单号,oea02 AS 订单制单日,xc AS 项次,sfb22 工单对应订单, '
    + #13#10 +
    ' ddsl AS 订单数量,ROUND(DECODE(NVL(smd04,0),0,0,NVL(smd06,0)/smd04) * ddsl,3) as 订单重量,dw AS 单位,订单已发数量,工单排产数,工单完工数,(工单排产数 - 工单完工数) AS 生产差数, '
    + #13#10 +
    ' DECODE(sign(ddsl - 工单完工数),-1,0,ddsl-工单完工数) AS 订单未入库量,oeb15 AS 约定交货日, ' +
    #13#10 + ' 最近完工日,(CASE WHEN 最近完工日 > oeb15 THEN 最近完工日 - oeb15 ELSE 0 END) AS 工单延期, '
    + #13#10 + '  (CASE  ' + #13#10 +
    '   WHEN (NVL(工单完工数,0) = 0) THEN  ''未生产'' ' + #13#10 +
    '   WHEN (工单完工数 >= 工单排产数) THEN ''已完工'' ' + #13#10 +
    '   ELSE ''生产中'' END) AS 工单状态,DECODE(sfb04,''8'',''是'',''否'') AS 工单结案, ' +
    #13#10 + '   (CASE       ' + #13#10 +
    'WHEN (NVL(工单完工数,0) >= ddsl) THEN ''完成入库''  ' + #13#10 +
    'WHEN (NVL(sfb04,''0'') = ''8'' AND NVL(工单完工数,0) < ddsl)    THEN ''结案入库'' '
    + #13#10 +
    'WHEN (NVL(sfb04,''0'') <> ''8'' AND NVL(工单完工数,0) = 0) THEN ''未入库'' ' +
    #13#10 + 'WHEN (NVL(sfb04,''0'') <> ''8'' AND NVL(工单完工数,0) > 0 AND NVL(工单完工数,0) < ddsl) THEN ''入库中'' '
    + #13#10 + 'ELSE ''未明'' END) AS 订单入库状态, ' + #13#10 +
    'oeaoriu AS 计划员,xx.gen02 AS 计划员姓名,oea14,yy.gen02 AS 业务经理   ' + #13#10 +
    '  FROM ' + #13#10 + '  ( ' + #13#10 +
    '  SELECT tc_gen03 ,oeaud02,oea901,oeb01 AS dddh,oeb03 AS xc,oea03 AS kh,oeb04,oeb912 AS ddsl,oeb910 AS dw,sfb08 AS 工单排产数, '
    + #13#10 +
    ' sfb09 AS 工单完工数,oeb24 AS 订单已发数量,oeb15,oeaoriu,sfb01,sfb22,sfb13 AS 预计开工日,sfb04, '
    + #13#10 +
    '  (SELECT MAX(sfu02) FROM sfu_file WHERE sfu01 IN (SELECT sfv01 FROM sfv_file WHERE sfv11 = sfb01) AND sfupost = ''Y'') AS 最近完工日,gem02,oea02,oea14,oeaorig  '
    + #13#10 + '  FROM oeb_file  ' + #13#10 +
    '  LEFT JOIN oea_file ON oea01 = oeb01   ' + #13#10 +
    '  LEFT JOIN sfb_file ON sfb22 = oeb01 AND sfb221 = oeb03 ' + #13#10 +
    '  LEFT JOIN gem_file ON gem01 = sfb98 ' + #13#10 +
    '  LEFT JOIN tc_gen_file ON tc_gen01 = oeaud02 ' + #13#10 + '  WHERE  ' +
    #13#10 + '  regexp_like(oeaorig, ''^%s'') AND oea901 = ''N'' AND oeb70 = ''N'' AND tc_gen04 <> ''011503'' '
    + #13#10 + ' UNION ALL  ' + #13#10 +
    '  SELECT tc_gen03,aa.oeaud02,aa.oea901,xx.oeb01 AS dddh,xx.oeb03 AS xc,aa.oea03 AS kh,xx.oeb04,yy.oeb912 AS ddsl,yy.oeb910 AS dw,sfb08 AS 工单排产数, '
    + #13#10 + ' sfb09 AS 工单完工数,xx.oeb24 AS 订单已发数量, ' + #13#10 +
    '  xx.oeb15,aa.oeaoriu,sfb01,sfb22,sfb13 AS 预计开工日,sfb04, ' + #13#10 +
    '  (SELECT MAX(sfu02) FROM gngs.sfu_file WHERE sfu01 IN (SELECT sfv01 FROM gngs.sfv_file WHERE sfv11 = sfb01) AND sfupost = ''Y'') AS 最近完工日,gem02,aa.oea02,aa.oea14,aa.oeaorig  '
    + #13#10 + '  FROM oeb_file xx  ' + #13#10 +
    '  LEFT JOIN oea_file aa  ON aa.oea01 = xx.oeb01   ' + #13#10 +
    '  LEFT JOIN gngs.oea_file bb ON bb.oea99 = aa.oea99 ' + #13#10 +
    '  LEFT JOIN gngs.oeb_file yy ON yy.oeb01 = bb.oea01 AND yy.oeb03 = xx.oeb03 '
    + #13#10 +
    '  LEFT JOIN gngs.sfb_file ON sfb22 = bb.oea01 AND sfb221 = xx.oeb03  ' +
    #13#10 + '  LEFT JOIN tc_gen_file ON tc_gen01 = aa.oeaud02 ' + #13#10 +
    '  LEFT JOIN gngs.gem_file ON gem01 = sfb98 ' + #13#10 + '  WHERE  ' +
    #13#10 + ' regexp_like(aa.oeaorig, ''^%0:s'') AND aa.oea901 = ''Y'' AND xx.oeb70 = ''N'' AND tc_gen03 = ''GNGS'' AND tc_gen04 <> ''011503''  '
    + #13#10 + ' UNION ALL    ' + #13#10 +
    '  SELECT tc_gen03,aa.oeaud02,aa.oea901,xx.oeb01 AS dddh,xx.oeb03 AS xc,aa.oea03 AS kh,xx.oeb04,yy.oeb912 AS ddsl,yy.oeb910 AS dw,sfb08 AS 工单排产数, '
    + #13#10 +
    ' sfb09 AS 工单完工数,xx.oeb24 AS 订单已发数量,xx.oeb15,aa.oeaoriu,sfb01,sfb22,sfb13 AS 预计开工日,sfb04, '
    + #13#10 +
    '  (SELECT MAX(sfu02) FROM ykgs.sfu_file WHERE sfu01 IN (SELECT sfv01 FROM ykgs.sfv_file WHERE sfv11 = sfb01) AND sfupost = ''Y'') AS 最近完工日,gem02,aa.oea02,aa.oea14,aa.oeaorig  '
    + #13#10 + '  FROM oeb_file xx  ' + #13#10 +
    '  LEFT JOIN oea_file aa  ON aa.oea01 = xx.oeb01   ' + #13#10 +
    '  LEFT JOIN ykgs.oea_file bb ON bb.oea99 = aa.oea99 ' + #13#10 +
    '  LEFT JOIN ykgs.oeb_file yy ON yy.oeb01 = bb.oea01 AND yy.oeb03 = xx.oeb03  '
    + #13#10 +
    '  LEFT JOIN ykgs.sfb_file ON sfb22 = bb.oea01 AND sfb221 = xx.oeb03  ' +
    #13#10 + '  LEFT JOIN tc_gen_file ON tc_gen01 = aa.oeaud02 ' + #13#10 +
    '  LEFT JOIN ykgs.gem_file ON gem01 = sfb98 ' + #13#10 + '  WHERE  ' +
    #13#10 + ' regexp_like(aa.oeaorig, ''^%0:s'') AND aa.oea901 = ''Y''  AND xx.oeb70 = ''N'' AND tc_gen03 = ''YKGS'' AND tc_gen04 <> ''011503''  '
    + sLineBreak + ' %1:s ' + #13#10 + '  ) LT  ' + #13#10 +
    '  LEFT JOIN ima_file ON ima01 = LT.oeb04 ' + #13#10 +
    '  LEFT JOIN imd_file ON imd01 = ima35 ' + #13#10 +
    '  LEFT JOIN occ_file ON occ01 = kh ' + #13#10 +
    '  LEFT JOIN smd_file ON smd01 = LT.oeb04 ' + #13#10 +
    '  LEFT JOIN gen_file xx ON xx.gen01 = oeaoriu  ' + #13#10 +
    '  LEFT JOIN gen_file yy ON yy.gen01 = oea14  ' + #13#10 +
    '  WHERE ddsl > 0 AND oeaud02 < ''A'' ';

  HYSQL = 'UNION ALL ' + sLineBreak +
    'SELECT tc_gen03 ,oeaud02,oea901,oeb01 AS dddh,oeb03 AS xc,oea03 AS kh,oeb04,oeb912 AS ddsl,oeb910 AS dw,sfb08 AS 工单排产数,  '
    + sLineBreak +
    'sfb09 AS 工单完工数,oeb24 AS 订单已发数量,oeb15,oeaoriu,sfb01,sfb22,sfb13 AS 预计开工日,SFB04,   '
    + sLineBreak +
    ' (SELECT MAX(sfu02) FROM ykgs.sfu_file WHERE sfu01 IN (SELECT sfv01 FROM ykgs.sfv_file WHERE sfv11 = sfb01) AND sfupost = ''Y'') AS 最近完工日,gem02,oea02,oea14,oeaorig '
    + sLineBreak + ' FROM ykgs.oeb_file  ' + sLineBreak +
    ' LEFT JOIN ykgs.oea_file ON oea01 = oeb01    ' + sLineBreak +
    ' LEFT JOIN ykgs.sfb_file ON sfb22 = oeb01 AND sfb221 = oeb03 ' + sLineBreak
    + ' LEFT JOIN ykgs.gem_file ON gem01 = sfb98                    ' +
    sLineBreak + ' LEFT JOIN ykgs.tc_gen_file ON tc_gen01 = oeaud02            '
    + sLineBreak +
    ' WHERE                                                       ' + sLineBreak
    + '  regexp_like(oeaorig, ''^%s'') AND                        ' + sLineBreak
    + ' oea901 = ''N'' AND oeb70 = ''N'' AND tc_gen04 <> ''011503''       ';

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := true;
    Result := true;
  end;
end;

function TWorkForm.GetFilterStr: string;
var
  i: Integer;
  s1: string;
  rqField: string; // 用来记录日期筛选条件是哪个字段
begin
  Result := '';
  // 客户简称筛选
  s1 := trim(khjc.Text);
  if s1 <> '' then
    Result := ' occ02 LIKE ''%' + s1 + '%'' ';
  // 计划员姓名筛选
  s1 := trim(jhyxm.Text);
  if s1 <> '' then
  begin
    if Result <> '' then
      Result := Result + ' AND ';
    Result := Result + ' xx.gen02 LIKE ''%' + s1 + '%'' ';
  end;
  // 品名筛选
  i := CBS.ItemIndex;
  s1 := trim(Edit1.Text);
  if s1 <> '' then
  begin
    if Result <> '' then
      Result := Result + ' AND ';
    case i of
      0:
        Result := Result + ' ima01 = ''' + s1 + ''' ';
      1:
        Result := Result + ' ima01 LIKE ''%' + s1 + '%'' ';
      2:
        Result := Result + ' ima01 LIKE ''' + s1 + '%'' ';
      3:
        Result := Result + ' ima01 LIKE ''%' + s1 + ''' ';
    end;
  end;
  // 约定交货日修改为订单日期筛选
  if Result <> '' then
    Result := Result + ' AND ';
  case rg1.ItemIndex of
    0:
      rqField := 'oea02'; // 订单制单日
    1:
      rqField := 'oeb15' // 订单约定交货日
  else
    rqField := 'oea02';
  end;
  Result := Result + rqField + ' BETWEEN to_date(''' +
    FormatDateTime('yyyymmdd', BeginDate.Date) + ''',''yyyymmdd'') AND ' +
    ' to_date(''' + FormatDateTime('yyyymmdd', EndDate.Date) +
    ''',''yyyymmdd'') ';

  // 计划员筛选
  s1 := trim(jhygh.Text);
  if s1 <> '' then
  begin
    if Result <> '' then
      Result := Result + ' AND ';
    Result := Result + ' oeaoriu = ''' + s1 + ''' ';
  end;
end;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
var
  filter, sfilter, ykf: string;
  MSQL: string;
begin
  Screen.Cursor := crSQLWait;
  try
    filter := GetFilterStr;
    case rg2.ItemIndex of
      0:
        begin
          sfilter := '0113';
          ykf := '';
          MSQL := Format(SQL, [sfilter, ykf]);
        end;
      1:
        begin
          sfilter := '0115';
          ykf := Format(HYSQL, [sfilter]);
          MSQL := Format(SQL, [sfilter, ykf]);
        end
    else
      begin
        sfilter := '011[35]';
        ykf := Format(HYSQL, [sfilter]);
        MSQL := Format(SQL, [sfilter, ykf]);
      end;
    end;
    if filter = '' then
      filter := MSQL
    else
      filter := MSQL + ' AND ' + filter;
    // memo1.Lines.Text := filter;
    RunCDS(filter);
    Cds.FieldByName('rownum').Visible := false;
    Cds.FieldByName('工单排产数').Visible := false;
    Cds.FieldByName('工单完工数').Visible := false;
    Cds.FieldByName('生产差数').Visible := false;
    Cds.FieldByName('OEA14').Visible := false;
    Cds.FieldByName('公司').DisplayWidth := 8;
    Cds.FieldByName('订单性质').DisplayWidth := 10;
    Cds.FieldByName('订单单号').DisplayWidth := 20;
    Cds.FieldByName('工单').DisplayWidth := 20;
    Cds.FieldByName('工单对应订单').DisplayWidth := 20;
    Cds.FieldByName('生产车间').DisplayWidth := 20;
    Cds.FieldByName('品号').DisplayWidth := 20;
    Cds.FieldByName('品名').DisplayWidth := 40;
    Cds.FieldByName('规格').DisplayWidth := 50;
    Cds.FieldByName('牌号').DisplayWidth := 20;
    Cds.FieldByName('配方').DisplayWidth := 20;
    Cds.FieldByName('包装方式').DisplayWidth := 20;
    Cds.FieldByName('仓库').DisplayWidth := 20;
    Cds.FieldByName('客户').DisplayWidth := 40;
    Cds.FieldByName('计划员姓名').DisplayWidth := 15;
    Cds.FieldByName('业务经理').DisplayWidth := 15;
    Cds.FieldByName('单位').DisplayWidth := 4;

    if Cds.RecordCount = 0 then
      application.MessageBox('没有查询到相关记录，请修改查询条件后继续！', '提示')
    else
      Button2.Enabled := true;
  finally
    ResizeForm;
    Screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.Button2Click(Sender: TObject);
var
  s: TStringList;
  str, fileName: string;
  i: Integer;
begin
  str := '';
  CRDBGrid1.DataSource.DataSet.DisableControls;
  for i := 0 to CRDBGrid1.DataSource.DataSet.FieldCount - 1 do
    if CRDBGrid1.DataSource.DataSet.fields[i].Visible then
      str := str + CRDBGrid1.DataSource.DataSet.fields[i].DisplayLabel
        + char(9);
  str := str + #13;
  CRDBGrid1.DataSource.DataSet.First;
  while not(CRDBGrid1.DataSource.DataSet.eof) do
  begin
    for i := 0 to CRDBGrid1.DataSource.DataSet.FieldCount - 1 do
      if CRDBGrid1.DataSource.DataSet.fields[i].Visible then
        str := str + CRDBGrid1.DataSource.DataSet.fields[i].AsString + char(9);

    str := str + #13;
    CRDBGrid1.DataSource.DataSet.next;
  end; // end  while

  CRDBGrid1.DataSource.DataSet.EnableControls;

  if SFD.Execute then
    fileName := SFD.fileName;

  if fileName = '' then
  begin
    showmessage('请选择正确的文件名继续');
    exit;
  end;

  s := TStringList.Create;
  s.Add(str);
  s.SaveToFile(fileName);
  s.Free;
  showmessage('导入成功！');
end;

procedure TWorkForm.CRDBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
  Field: TField; State: TGridDrawState);
begin
  if (Cds.FieldByName('rowNum').AsInteger mod 2 > 0) then
  begin
    CRDBGrid1.Canvas.Brush.Color := clInfoBk; // 背景色
    CRDBGrid1.Canvas.FillRect(Rect);
  end;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
