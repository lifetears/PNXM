object WorkForm: TWorkForm
  AlignWithMargins = True
  Left = 309
  Top = 196
  Margins.Top = 5
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMinimized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 145
    Align = alTop
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 1029
      Height = 103
      Align = alClient
      Caption = #26597#35810#26465#20214
      TabOrder = 0
      object FlowPanel1: TFlowPanel
        Left = 2
        Top = 14
        Width = 1025
        Height = 87
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 3
        ExplicitTop = 13
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 257
          Height = 41
          BevelOuter = bvNone
          TabOrder = 0
          object rg2: TRadioGroup
            Left = 0
            Top = 0
            Width = 257
            Height = 41
            Align = alClient
            Caption = #38144#21806#37096#38376#36873#25321
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              #22269#20869#33829#38144
              #28023#22806#33829#38144
              #20840#37096)
            TabOrder = 0
            ExplicitWidth = 194
          end
        end
        object Panel3: TPanel
          Left = 257
          Top = 0
          Width = 216
          Height = 41
          BevelOuter = bvNone
          TabOrder = 1
          object rg1: TRadioGroup
            Left = 0
            Top = 0
            Width = 216
            Height = 41
            Align = alClient
            Caption = #26085#26399#36873#25321
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              #35746#21333#21046#21333#26085
              #32422#23450#20132#36135#26085)
            TabOrder = 0
            ExplicitLeft = -6
            ExplicitTop = 4
            ExplicitWidth = 335
          end
        end
        object Panel4: TPanel
          Left = 473
          Top = 0
          Width = 344
          Height = 41
          BevelOuter = bvNone
          TabOrder = 2
          object Bevel2: TBevel
            Left = 0
            Top = 36
            Width = 344
            Height = 5
            Align = alBottom
            Shape = bsBottomLine
            ExplicitLeft = 56
            ExplicitTop = 22
            ExplicitWidth = 50
          end
          object Bevel5: TBevel
            Left = 0
            Top = 0
            Width = 5
            Height = 36
            Align = alLeft
            Shape = bsLeftLine
            ExplicitLeft = 16
            ExplicitTop = -5
            ExplicitHeight = 50
          end
          object Label2: TLabel
            Left = 15
            Top = 14
            Width = 192
            Height = 12
            Caption = #26085#26399#65306#20174'                    '#21040#65306
          end
          object BeginDate: TDateTimePicker
            Left = 72
            Top = 9
            Width = 98
            Height = 21
            Date = 0.445955277777102300
            Time = 0.445955277777102300
            TabOrder = 0
          end
          object EndDate: TDateTimePicker
            Left = 213
            Top = 9
            Width = 98
            Height = 21
            Date = 42825.446090439810000000
            Time = 42825.446090439810000000
            TabOrder = 1
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 41
          Width = 257
          Height = 41
          BevelOuter = bvNone
          TabOrder = 3
          object Bevel3: TBevel
            Left = 0
            Top = 36
            Width = 257
            Height = 5
            Align = alBottom
            Shape = bsBottomLine
            ExplicitLeft = 6
            ExplicitWidth = 265
          end
          object Bevel6: TBevel
            Left = 0
            Top = 0
            Width = 5
            Height = 36
            Align = alLeft
            Shape = bsLeftLine
            ExplicitLeft = 16
            ExplicitTop = -9
            ExplicitHeight = 50
          end
          object Label1: TLabel
            Left = 11
            Top = 13
            Width = 36
            Height = 12
            Caption = #21697'  '#21495
          end
          object CBS: TComboBox
            Left = 53
            Top = 10
            Width = 73
            Height = 20
            ItemIndex = 0
            TabOrder = 0
            Text = #31561#20110
            Items.Strings = (
              #31561#20110
              #21253#21547
              #24320#22836#20026
              #32467#23614#20026)
          end
          object Edit1: TEdit
            Left = 55
            Top = 10
            Width = 186
            Height = 20
            TabOrder = 1
          end
        end
        object Panel7: TPanel
          Left = 257
          Top = 41
          Width = 221
          Height = 41
          BevelOuter = bvNone
          TabOrder = 4
          object Bevel4: TBevel
            Left = 0
            Top = 36
            Width = 221
            Height = 5
            Align = alBottom
            Shape = bsBottomLine
            ExplicitLeft = 72
            ExplicitWidth = 50
          end
          object Bevel7: TBevel
            Left = 0
            Top = 0
            Width = 5
            Height = 36
            Align = alLeft
            Shape = bsLeftLine
            ExplicitLeft = 24
            ExplicitTop = -9
            ExplicitHeight = 50
          end
          object khjc: TLabeledEdit
            Left = 74
            Top = 10
            Width = 143
            Height = 20
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #23458#25143#31616#31216#65306
            LabelPosition = lpLeft
            LabelSpacing = 10
            TabOrder = 0
          end
        end
        object Panel8: TPanel
          Left = 478
          Top = 41
          Width = 208
          Height = 41
          BevelOuter = bvNone
          TabOrder = 5
          object Bevel8: TBevel
            Left = 0
            Top = 0
            Width = 5
            Height = 36
            Align = alLeft
            Shape = bsLeftLine
            ExplicitLeft = 8
            ExplicitHeight = 41
          end
          object Bevel9: TBevel
            Left = 0
            Top = 36
            Width = 208
            Height = 5
            Align = alBottom
            Shape = bsBottomLine
            ExplicitLeft = 6
            ExplicitTop = 37
          end
          object jhygh: TLabeledEdit
            Left = 94
            Top = 11
            Width = 96
            Height = 20
            EditLabel.Width = 72
            EditLabel.Height = 12
            EditLabel.Caption = #35745#21010#21592#24037#21495#65306
            LabelPosition = lpLeft
            LabelSpacing = 10
            TabOrder = 0
          end
        end
        object Panel9: TPanel
          Left = 686
          Top = 41
          Width = 219
          Height = 41
          BevelOuter = bvNone
          Caption = 'Panel9'
          TabOrder = 6
          object bvl1: TBevel
            Left = 0
            Top = 0
            Width = 5
            Height = 36
            Align = alLeft
            Shape = bsLeftLine
            ExplicitLeft = 8
            ExplicitHeight = 41
          end
          object bvl2: TBevel
            Left = 0
            Top = 36
            Width = 219
            Height = 5
            Align = alBottom
            Shape = bsBottomLine
            ExplicitLeft = 72
            ExplicitWidth = 50
          end
          object jhyxm: TLabeledEdit
            Left = 87
            Top = 10
            Width = 105
            Height = 20
            EditLabel.Width = 72
            EditLabel.Height = 12
            EditLabel.Caption = #35745#21010#21592#22995#21517#65306
            LabelPosition = lpLeft
            LabelSpacing = 10
            TabOrder = 0
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 104
      Width = 1029
      Height = 40
      Align = alBottom
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = #26597#35810#22791#27880#65306#24037#21495#24517#39035#36755#20840';'#23458#25143#31616#31216#36755#20837#20851#38190#23383';'#36820#22238#20179#24211#20449#24687#20026#26009#20214#22312#39640#28201#36134#22871#30340#40664#35748#20179#24211#65289
      TabOrder = 1
      object Button1: TButton
        Left = 688
        Top = 6
        Width = 75
        Height = 25
        Caption = #26597'  '#35810
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 810
        Top = 6
        Width = 75
        Height = 25
        Caption = #23548#20986'EXCEL'
        Enabled = False
        TabOrder = 1
        OnClick = Button2Click
      end
    end
  end
  object CRDBGrid1: TCRDBGrid
    Left = 0
    Top = 145
    Width = 1031
    Height = 444
    Margins.Right = 30
    Align = alClient
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #23435#20307
    TitleFont.Style = []
    OnDrawDataCell = CRDBGrid1DrawDataCell
  end
  object Memo1: TMemo
    Left = 296
    Top = 200
    Width = 585
    Height = 297
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    Visible = False
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 168
    Top = 312
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 176
    Top = 398
  end
  object SFD: TSaveTextFileDialog
    DefaultExt = 'xls'
    Left = 88
    Top = 320
  end
  object DataSource1: TDataSource
    DataSet = Cds
    Left = 328
    Top = 288
  end
end
