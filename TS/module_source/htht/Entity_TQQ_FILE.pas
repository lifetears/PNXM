//
// ------------------------------------------------------------------------------------
//
// Entity_TQQ_FILE.pas -- 数据表"TQQ_FILE"对应的实体类单元（UniDAC引擎）for VCL
//
//                          本单元由QuickBurro V5.0.4.0 实体类生成器（GetEntity）所生成。
//
//                          生成时间: 2019-03-18 14:14:49
//
//                          版权所有(C) 樵夫软件工作室（Jopher Software Studio）, 2006-2017 
//
//
//// ------------------------------------------------------------------------------------
//

unit Entity_TQQ_FILE;

interface

uses
   {$IF CompilerVersion>=23.0}
   System.SysUtils, 
   System.Classes, 
   Data.db, 
   Datasnap.DBClient,
   {$ELSE}
   SysUtils, 
   Classes, 
   db, 
   DBClient,
   {$IFEND}
   {$IFDEF UNICODE}
   {$IF CompilerVersion>=23.0}System.AnsiStrings{$ELSE}AnsiStrings{$IFEND},
   {$ENDIF}
   QBParcel,
   RemoteUniDac;

type
  TTQQ_FILE = class(TObject)

  Private
    ff_TQQ01: String;
    ff_TQQ02: Integer;
    ff_TQQ03: String;
    ff_TQQ04: String;
    ff_TQQ05: String;
    ff_TQQ06: String;
    ff_TQQUD01: String;
    ff_TQQUD02: String;
    ff_TQQUD03: String;
    ff_TQQUD04: String;
    ff_TQQUD05: String;
    ff_TQQUD06: String;
    ff_TQQUD07: Double;
    ff_TQQUD08: Double;
    ff_TQQUD09: Double;
    ff_TQQUD10: Int64;
    ff_TQQUD11: Int64;
    ff_TQQUD12: Int64;
    ff_TQQUD13: TDateTime;
    ff_TQQUD14: TDateTime;
    ff_TQQUD15: TDateTime;
    ff_TQQPLANT: String;
    ff_TQQLEGAL: String;

    flag_TQQ01: boolean;
    flag_TQQ02: boolean;
    flag_TQQ03: boolean;
    flag_TQQ04: boolean;
    flag_TQQ05: boolean;
    flag_TQQ06: boolean;
    flag_TQQUD01: boolean;
    flag_TQQUD02: boolean;
    flag_TQQUD03: boolean;
    flag_TQQUD04: boolean;
    flag_TQQUD05: boolean;
    flag_TQQUD06: boolean;
    flag_TQQUD07: boolean;
    flag_TQQUD08: boolean;
    flag_TQQUD09: boolean;
    flag_TQQUD10: boolean;
    flag_TQQUD11: boolean;
    flag_TQQUD12: boolean;
    flag_TQQUD13: boolean;
    flag_TQQUD14: boolean;
    flag_TQQUD15: boolean;
    flag_TQQPLANT: boolean;
    flag_TQQLEGAL: boolean;

    procedure Set_TQQ01(_TQQ01: String);
    procedure Set_TQQ02(_TQQ02: Integer);
    procedure Set_TQQ03(_TQQ03: String);
    procedure Set_TQQ04(_TQQ04: String);
    procedure Set_TQQ05(_TQQ05: String);
    procedure Set_TQQ06(_TQQ06: String);
    procedure Set_TQQUD01(_TQQUD01: String);
    procedure Set_TQQUD02(_TQQUD02: String);
    procedure Set_TQQUD03(_TQQUD03: String);
    procedure Set_TQQUD04(_TQQUD04: String);
    procedure Set_TQQUD05(_TQQUD05: String);
    procedure Set_TQQUD06(_TQQUD06: String);
    procedure Set_TQQUD07(_TQQUD07: Double);
    procedure Set_TQQUD08(_TQQUD08: Double);
    procedure Set_TQQUD09(_TQQUD09: Double);
    procedure Set_TQQUD10(_TQQUD10: Int64);
    procedure Set_TQQUD11(_TQQUD11: Int64);
    procedure Set_TQQUD12(_TQQUD12: Int64);
    procedure Set_TQQUD13(_TQQUD13: TDateTime);
    procedure Set_TQQUD14(_TQQUD14: TDateTime);
    procedure Set_TQQUD15(_TQQUD15: TDateTime);
    procedure Set_TQQPLANT(_TQQPLANT: String);
    procedure Set_TQQLEGAL(_TQQLEGAL: String);

  public
    Property f_TQQ01: String read ff_TQQ01 write Set_TQQ01;
    Property TQQ01_Updated: boolean read flag_TQQ01 write flag_TQQ01;
    Property f_TQQ02: Integer read ff_TQQ02 write Set_TQQ02;
    Property TQQ02_Updated: boolean read flag_TQQ02 write flag_TQQ02;
    Property f_TQQ03: String read ff_TQQ03 write Set_TQQ03;
    Property TQQ03_Updated: boolean read flag_TQQ03 write flag_TQQ03;
    Property f_TQQ04: String read ff_TQQ04 write Set_TQQ04;
    Property TQQ04_Updated: boolean read flag_TQQ04 write flag_TQQ04;
    Property f_TQQ05: String read ff_TQQ05 write Set_TQQ05;
    Property TQQ05_Updated: boolean read flag_TQQ05 write flag_TQQ05;
    Property f_TQQ06: String read ff_TQQ06 write Set_TQQ06;
    Property TQQ06_Updated: boolean read flag_TQQ06 write flag_TQQ06;
    Property f_TQQUD01: String read ff_TQQUD01 write Set_TQQUD01;
    Property TQQUD01_Updated: boolean read flag_TQQUD01 write flag_TQQUD01;
    Property f_TQQUD02: String read ff_TQQUD02 write Set_TQQUD02;
    Property TQQUD02_Updated: boolean read flag_TQQUD02 write flag_TQQUD02;
    Property f_TQQUD03: String read ff_TQQUD03 write Set_TQQUD03;
    Property TQQUD03_Updated: boolean read flag_TQQUD03 write flag_TQQUD03;
    Property f_TQQUD04: String read ff_TQQUD04 write Set_TQQUD04;
    Property TQQUD04_Updated: boolean read flag_TQQUD04 write flag_TQQUD04;
    Property f_TQQUD05: String read ff_TQQUD05 write Set_TQQUD05;
    Property TQQUD05_Updated: boolean read flag_TQQUD05 write flag_TQQUD05;
    Property f_TQQUD06: String read ff_TQQUD06 write Set_TQQUD06;
    Property TQQUD06_Updated: boolean read flag_TQQUD06 write flag_TQQUD06;
    Property f_TQQUD07: Double read ff_TQQUD07 write Set_TQQUD07;
    Property TQQUD07_Updated: boolean read flag_TQQUD07 write flag_TQQUD07;
    Property f_TQQUD08: Double read ff_TQQUD08 write Set_TQQUD08;
    Property TQQUD08_Updated: boolean read flag_TQQUD08 write flag_TQQUD08;
    Property f_TQQUD09: Double read ff_TQQUD09 write Set_TQQUD09;
    Property TQQUD09_Updated: boolean read flag_TQQUD09 write flag_TQQUD09;
    Property f_TQQUD10: Int64 read ff_TQQUD10 write Set_TQQUD10;
    Property TQQUD10_Updated: boolean read flag_TQQUD10 write flag_TQQUD10;
    Property f_TQQUD11: Int64 read ff_TQQUD11 write Set_TQQUD11;
    Property TQQUD11_Updated: boolean read flag_TQQUD11 write flag_TQQUD11;
    Property f_TQQUD12: Int64 read ff_TQQUD12 write Set_TQQUD12;
    Property TQQUD12_Updated: boolean read flag_TQQUD12 write flag_TQQUD12;
    Property f_TQQUD13: TDateTime read ff_TQQUD13 write Set_TQQUD13;
    Property TQQUD13_Updated: boolean read flag_TQQUD13 write flag_TQQUD13;
    Property f_TQQUD14: TDateTime read ff_TQQUD14 write Set_TQQUD14;
    Property TQQUD14_Updated: boolean read flag_TQQUD14 write flag_TQQUD14;
    Property f_TQQUD15: TDateTime read ff_TQQUD15 write Set_TQQUD15;
    Property TQQUD15_Updated: boolean read flag_TQQUD15 write flag_TQQUD15;
    Property f_TQQPLANT: String read ff_TQQPLANT write Set_TQQPLANT;
    Property TQQPLANT_Updated: boolean read flag_TQQPLANT write flag_TQQPLANT;
    Property f_TQQLEGAL: String read ff_TQQLEGAL write Set_TQQLEGAL;
    Property TQQLEGAL_Updated: boolean read flag_TQQLEGAL write flag_TQQLEGAL;

    constructor Create;
    destructor Destroy; override;
    class function CreateCds: TClientDataset;
    class function ReadFromCds(aCds: TDataset): TTQQ_FILE;
    function SaveToCds(Cds: TDataset): Boolean;
    class function ReadFromDB(DBA: TRemoteUnidac; Condition: String): TTQQ_FILE;
    function DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
    function InsertToDB(DBA: TRemoteUnidac): boolean;
    function UpdateToDB(DBA: TRemoteUnidac; Condition: String): boolean;
    function UpdateToDB2(DBA: TRemoteUnidac; Condition: String): boolean;
    function PropIsNull(PropName: string): boolean;
    function SetPropNull(PropName: string): boolean;
    function CloneTo(var NewObj: TTQQ_FILE): boolean;

  end;

  TTQQ_FILEList = class(TObject)
  Private
     fCount: integer;
     fList: array of TTQQ_FILE;

  public    
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure add(Obj: TTQQ_FILE);
    procedure Delete(Index: Integer);
    function Get(Index: Integer): Pointer;
    procedure Put(Index: Integer; Item: Pointer);
    class function ReadFromDB(DBA: TRemoteUnidac; Condition: String = ''; 
      OrderBy: String = ''; SelectOption: string=''): TTQQ_FILEList;
    class function ReadFromCDS(Cds: TDataset): TTQQ_FILEList;
    function SaveToCds(Cds: TDataset): boolean;
    function InsertToDB(DBA: TRemoteUnidac): boolean;
    function UpdateToDB(DBA: TRemoteUnidac; KeyFieldList: string; Condition: String): boolean;
    function DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
    function SaveToFile(FileName: String; DataFormat: TDataPacketFormat): boolean;
    function LoadFromFile(FileName: String): boolean;
    function SaveToStream(aStream: TMemoryStream; DataFormat: TDataPacketFormat): boolean;
    function LoadFromStream(aStream: TMemoryStream): boolean;
    function SaveToParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
    function LoadFromParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
    function CloneTo(NewList: TTQQ_FILEList): boolean;
    property Count: integer read fCount;
    property Items[Index: Integer]: Pointer read Get write Put;
    
  end;

implementation

{ TTQQ_FILE }

//
// 属性设置的一系列内部过程...
procedure TTQQ_FILE.Set_TQQ01(_TQQ01: String);
begin
   ff_TQQ01:=_TQQ01;
   flag_TQQ01:=true;
end;

procedure TTQQ_FILE.Set_TQQ02(_TQQ02: Integer);
begin
   ff_TQQ02:=_TQQ02;
   flag_TQQ02:=true;
end;

procedure TTQQ_FILE.Set_TQQ03(_TQQ03: String);
begin
   ff_TQQ03:=_TQQ03;
   flag_TQQ03:=true;
end;

procedure TTQQ_FILE.Set_TQQ04(_TQQ04: String);
begin
   ff_TQQ04:=_TQQ04;
   flag_TQQ04:=true;
end;

procedure TTQQ_FILE.Set_TQQ05(_TQQ05: String);
begin
   ff_TQQ05:=_TQQ05;
   flag_TQQ05:=true;
end;

procedure TTQQ_FILE.Set_TQQ06(_TQQ06: String);
begin
   ff_TQQ06:=_TQQ06;
   flag_TQQ06:=true;
end;

procedure TTQQ_FILE.Set_TQQUD01(_TQQUD01: String);
begin
   ff_TQQUD01:=_TQQUD01;
   flag_TQQUD01:=true;
end;

procedure TTQQ_FILE.Set_TQQUD02(_TQQUD02: String);
begin
   ff_TQQUD02:=_TQQUD02;
   flag_TQQUD02:=true;
end;

procedure TTQQ_FILE.Set_TQQUD03(_TQQUD03: String);
begin
   ff_TQQUD03:=_TQQUD03;
   flag_TQQUD03:=true;
end;

procedure TTQQ_FILE.Set_TQQUD04(_TQQUD04: String);
begin
   ff_TQQUD04:=_TQQUD04;
   flag_TQQUD04:=true;
end;

procedure TTQQ_FILE.Set_TQQUD05(_TQQUD05: String);
begin
   ff_TQQUD05:=_TQQUD05;
   flag_TQQUD05:=true;
end;

procedure TTQQ_FILE.Set_TQQUD06(_TQQUD06: String);
begin
   ff_TQQUD06:=_TQQUD06;
   flag_TQQUD06:=true;
end;

procedure TTQQ_FILE.Set_TQQUD07(_TQQUD07: Double);
begin
   ff_TQQUD07:=_TQQUD07;
   flag_TQQUD07:=true;
end;

procedure TTQQ_FILE.Set_TQQUD08(_TQQUD08: Double);
begin
   ff_TQQUD08:=_TQQUD08;
   flag_TQQUD08:=true;
end;

procedure TTQQ_FILE.Set_TQQUD09(_TQQUD09: Double);
begin
   ff_TQQUD09:=_TQQUD09;
   flag_TQQUD09:=true;
end;

procedure TTQQ_FILE.Set_TQQUD10(_TQQUD10: Int64);
begin
   ff_TQQUD10:=_TQQUD10;
   flag_TQQUD10:=true;
end;

procedure TTQQ_FILE.Set_TQQUD11(_TQQUD11: Int64);
begin
   ff_TQQUD11:=_TQQUD11;
   flag_TQQUD11:=true;
end;

procedure TTQQ_FILE.Set_TQQUD12(_TQQUD12: Int64);
begin
   ff_TQQUD12:=_TQQUD12;
   flag_TQQUD12:=true;
end;

procedure TTQQ_FILE.Set_TQQUD13(_TQQUD13: TDateTime);
begin
   ff_TQQUD13:=_TQQUD13;
   flag_TQQUD13:=true;
end;

procedure TTQQ_FILE.Set_TQQUD14(_TQQUD14: TDateTime);
begin
   ff_TQQUD14:=_TQQUD14;
   flag_TQQUD14:=true;
end;

procedure TTQQ_FILE.Set_TQQUD15(_TQQUD15: TDateTime);
begin
   ff_TQQUD15:=_TQQUD15;
   flag_TQQUD15:=true;
end;

procedure TTQQ_FILE.Set_TQQPLANT(_TQQPLANT: String);
begin
   ff_TQQPLANT:=_TQQPLANT;
   flag_TQQPLANT:=true;
end;

procedure TTQQ_FILE.Set_TQQLEGAL(_TQQLEGAL: String);
begin
   ff_TQQLEGAL:=_TQQLEGAL;
   flag_TQQLEGAL:=true;
end;


//
// 创建Cds容器，用于记录数据的本地暂存...
class function TTQQ_FILE.CreateCds: TClientDataset;
begin
  Result:=TClientDataset.Create(nil);
  with Result do
     begin
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQ01';
              DataType:=ftString;
              Size:=40;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQ02';
              DataType:=ftInteger;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQ03';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQ04';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQ05';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQ06';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD01';
              DataType:=ftString;
              Size:=510;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD02';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD03';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD04';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD05';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD06';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD07';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD08';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD09';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD10';
              DataType:=ftLargeint;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD11';
              DataType:=ftLargeint;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD12';
              DataType:=ftLargeint;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD13';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD14';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQUD15';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQPLANT';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQQLEGAL';
              DataType:=ftString;
              Size:=20;
           end;
        CreateDataSet;
     end;
  Result.Open;
end;

//
// 创建对象实例...
constructor TTQQ_FILE.Create;
begin
  inherited;
  ff_TQQ01:='';
  flag_TQQ01:=false;
  ff_TQQ02:=0;
  flag_TQQ02:=false;
  ff_TQQ03:='';
  flag_TQQ03:=false;
  ff_TQQ04:='';
  flag_TQQ04:=false;
  ff_TQQ05:='';
  flag_TQQ05:=false;
  ff_TQQ06:='';
  flag_TQQ06:=false;
  ff_TQQUD01:='';
  flag_TQQUD01:=false;
  ff_TQQUD02:='';
  flag_TQQUD02:=false;
  ff_TQQUD03:='';
  flag_TQQUD03:=false;
  ff_TQQUD04:='';
  flag_TQQUD04:=false;
  ff_TQQUD05:='';
  flag_TQQUD05:=false;
  ff_TQQUD06:='';
  flag_TQQUD06:=false;
  ff_TQQUD07:=0.0;
  flag_TQQUD07:=false;
  ff_TQQUD08:=0.0;
  flag_TQQUD08:=false;
  ff_TQQUD09:=0.0;
  flag_TQQUD09:=false;
  ff_TQQUD10:=0;
  flag_TQQUD10:=false;
  ff_TQQUD11:=0;
  flag_TQQUD11:=false;
  ff_TQQUD12:=0;
  flag_TQQUD12:=false;
  ff_TQQUD13:=0;
  flag_TQQUD13:=false;
  ff_TQQUD14:=0;
  flag_TQQUD14:=false;
  ff_TQQUD15:=0;
  flag_TQQUD15:=false;
  ff_TQQPLANT:='';
  flag_TQQPLANT:=false;
  ff_TQQLEGAL:='';
  flag_TQQLEGAL:=false;
end;

//
// 释放对象实例...
destructor TTQQ_FILE.Destroy;
begin
  inherited;
end;

//
// 将Cds中的当前记录转换为一个对象实例...
class function TTQQ_FILE.ReadFromCds(aCds: TDataset): TTQQ_FILE;
begin
  if (not aCds.active) or (aCds.RecordCount<=0) then
    begin
      Result:=nil;
      exit;
    end;
  Result := TTQQ_FILE.Create;
  with Result do
    begin
      ff_TQQ01 := Trim(aCds.FieldByName('TQQ01').AsString);
      flag_TQQ01:=(not aCds.FieldByName('TQQ01').IsNull);
      ff_TQQ02 := aCds.FieldByName('TQQ02').AsInteger;
      flag_TQQ02:=(not aCds.FieldByName('TQQ02').IsNull);
      ff_TQQ03 := Trim(aCds.FieldByName('TQQ03').AsString);
      flag_TQQ03:=(not aCds.FieldByName('TQQ03').IsNull);
      ff_TQQ04 := Trim(aCds.FieldByName('TQQ04').AsString);
      flag_TQQ04:=(not aCds.FieldByName('TQQ04').IsNull);
      ff_TQQ05 := Trim(aCds.FieldByName('TQQ05').AsString);
      flag_TQQ05:=(not aCds.FieldByName('TQQ05').IsNull);
      ff_TQQ06 := Trim(aCds.FieldByName('TQQ06').AsString);
      flag_TQQ06:=(not aCds.FieldByName('TQQ06').IsNull);
      ff_TQQUD01 := Trim(aCds.FieldByName('TQQUD01').AsString);
      flag_TQQUD01:=(not aCds.FieldByName('TQQUD01').IsNull);
      ff_TQQUD02 := Trim(aCds.FieldByName('TQQUD02').AsString);
      flag_TQQUD02:=(not aCds.FieldByName('TQQUD02').IsNull);
      ff_TQQUD03 := Trim(aCds.FieldByName('TQQUD03').AsString);
      flag_TQQUD03:=(not aCds.FieldByName('TQQUD03').IsNull);
      ff_TQQUD04 := Trim(aCds.FieldByName('TQQUD04').AsString);
      flag_TQQUD04:=(not aCds.FieldByName('TQQUD04').IsNull);
      ff_TQQUD05 := Trim(aCds.FieldByName('TQQUD05').AsString);
      flag_TQQUD05:=(not aCds.FieldByName('TQQUD05').IsNull);
      ff_TQQUD06 := Trim(aCds.FieldByName('TQQUD06').AsString);
      flag_TQQUD06:=(not aCds.FieldByName('TQQUD06').IsNull);
      ff_TQQUD07 := aCds.FieldByName('TQQUD07').AsFloat;
      flag_TQQUD07:=(not aCds.FieldByName('TQQUD07').IsNull);
      ff_TQQUD08 := aCds.FieldByName('TQQUD08').AsFloat;
      flag_TQQUD08:=(not aCds.FieldByName('TQQUD08').IsNull);
      ff_TQQUD09 := aCds.FieldByName('TQQUD09').AsFloat;
      flag_TQQUD09:=(not aCds.FieldByName('TQQUD09').IsNull);
      ff_TQQUD10 := aCds.FieldByName('TQQUD10').AsInteger;
      flag_TQQUD10:=(not aCds.FieldByName('TQQUD10').IsNull);
      ff_TQQUD11 := aCds.FieldByName('TQQUD11').AsInteger;
      flag_TQQUD11:=(not aCds.FieldByName('TQQUD11').IsNull);
      ff_TQQUD12 := aCds.FieldByName('TQQUD12').AsInteger;
      flag_TQQUD12:=(not aCds.FieldByName('TQQUD12').IsNull);
      ff_TQQUD13 := aCds.FieldByName('TQQUD13').AsDateTime;
      flag_TQQUD13:=(not aCds.FieldByName('TQQUD13').IsNull);
      ff_TQQUD14 := aCds.FieldByName('TQQUD14').AsDateTime;
      flag_TQQUD14:=(not aCds.FieldByName('TQQUD14').IsNull);
      ff_TQQUD15 := aCds.FieldByName('TQQUD15').AsDateTime;
      flag_TQQUD15:=(not aCds.FieldByName('TQQUD15').IsNull);
      ff_TQQPLANT := Trim(aCds.FieldByName('TQQPLANT').AsString);
      flag_TQQPLANT:=(not aCds.FieldByName('TQQPLANT').IsNull);
      ff_TQQLEGAL := Trim(aCds.FieldByName('TQQLEGAL').AsString);
      flag_TQQLEGAL:=(not aCds.FieldByName('TQQLEGAL').IsNull);
    end;
end;

//
// 将对象转换到Cds的当前记录...
function TTQQ_FILE.SaveToCds(Cds: TDataset): Boolean;
begin
   try
     Cds.edit;
     if flag_TQQ01 then
        Cds.FieldValues['TQQ01']:=ff_TQQ01;
     if flag_TQQ02 then
        Cds.FieldValues['TQQ02']:=ff_TQQ02;
     if flag_TQQ03 then
        Cds.FieldValues['TQQ03']:=ff_TQQ03;
     if flag_TQQ04 then
        Cds.FieldValues['TQQ04']:=ff_TQQ04;
     if flag_TQQ05 then
        Cds.FieldValues['TQQ05']:=ff_TQQ05;
     if flag_TQQ06 then
        Cds.FieldValues['TQQ06']:=ff_TQQ06;
     if flag_TQQUD01 then
        Cds.FieldValues['TQQUD01']:=ff_TQQUD01;
     if flag_TQQUD02 then
        Cds.FieldValues['TQQUD02']:=ff_TQQUD02;
     if flag_TQQUD03 then
        Cds.FieldValues['TQQUD03']:=ff_TQQUD03;
     if flag_TQQUD04 then
        Cds.FieldValues['TQQUD04']:=ff_TQQUD04;
     if flag_TQQUD05 then
        Cds.FieldValues['TQQUD05']:=ff_TQQUD05;
     if flag_TQQUD06 then
        Cds.FieldValues['TQQUD06']:=ff_TQQUD06;
     if flag_TQQUD07 then
        Cds.FieldValues['TQQUD07']:=ff_TQQUD07;
     if flag_TQQUD08 then
        Cds.FieldValues['TQQUD08']:=ff_TQQUD08;
     if flag_TQQUD09 then
        Cds.FieldValues['TQQUD09']:=ff_TQQUD09;
     if flag_TQQUD10 then
        Cds.FieldValues['TQQUD10']:=ff_TQQUD10;
     if flag_TQQUD11 then
        Cds.FieldValues['TQQUD11']:=ff_TQQUD11;
     if flag_TQQUD12 then
        Cds.FieldValues['TQQUD12']:=ff_TQQUD12;
     if flag_TQQUD13 then
        Cds.FieldValues['TQQUD13']:=ff_TQQUD13;
     if flag_TQQUD14 then
        Cds.FieldValues['TQQUD14']:=ff_TQQUD14;
     if flag_TQQUD15 then
        Cds.FieldValues['TQQUD15']:=ff_TQQUD15;
     if flag_TQQPLANT then
        Cds.FieldValues['TQQPLANT']:=ff_TQQPLANT;
     if flag_TQQLEGAL then
        Cds.FieldValues['TQQLEGAL']:=ff_TQQLEGAL;
     Cds.post;
     result:=true;
   except
     result:=false;
   end;
end;

//
// 从远程数据库中读一个记录，并转换成对象实例...
class function TTQQ_FILE.ReadFromDB(DBA: TRemoteUnidac; Condition: String): TTQQ_FILE;
var
  SqlCommand: String;
  Cds: TClientDataset;
begin
  Result := nil;
  Cds:=TClientDataset.Create(nil);
  if Condition='' then
     SqlCommand:='SELECT * FROM TQQ_FILE'
  else
     SqlCommand := 'SELECT * FROM TQQ_FILE WHERE '+condition;
  if not DBA.ReadDataset(SqlCommand,Cds) then
     begin
       Cds.Free;
       exit;
     end;
  Result:=ReadFromCds(Cds);
  Cds.Free;
end;

//
// 将对象实例的属性写入到远程数据库记录...
function TTQQ_FILE.InsertToDB(DBA: TRemoteUnidac): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQQ_FILE.CreateCds;
  try
     Cds.Append;
     if flag_TQQ01 then
        Cds.FieldValues['TQQ01']:=ff_TQQ01;
     if flag_TQQ02 then
        Cds.FieldValues['TQQ02']:=ff_TQQ02;
     if flag_TQQ03 then
        Cds.FieldValues['TQQ03']:=ff_TQQ03;
     if flag_TQQ04 then
        Cds.FieldValues['TQQ04']:=ff_TQQ04;
     if flag_TQQ05 then
        Cds.FieldValues['TQQ05']:=ff_TQQ05;
     if flag_TQQ06 then
        Cds.FieldValues['TQQ06']:=ff_TQQ06;
     if flag_TQQUD01 then
        Cds.FieldValues['TQQUD01']:=ff_TQQUD01;
     if flag_TQQUD02 then
        Cds.FieldValues['TQQUD02']:=ff_TQQUD02;
     if flag_TQQUD03 then
        Cds.FieldValues['TQQUD03']:=ff_TQQUD03;
     if flag_TQQUD04 then
        Cds.FieldValues['TQQUD04']:=ff_TQQUD04;
     if flag_TQQUD05 then
        Cds.FieldValues['TQQUD05']:=ff_TQQUD05;
     if flag_TQQUD06 then
        Cds.FieldValues['TQQUD06']:=ff_TQQUD06;
     if flag_TQQUD07 then
        Cds.FieldValues['TQQUD07']:=ff_TQQUD07;
     if flag_TQQUD08 then
        Cds.FieldValues['TQQUD08']:=ff_TQQUD08;
     if flag_TQQUD09 then
        Cds.FieldValues['TQQUD09']:=ff_TQQUD09;
     if flag_TQQUD10 then
        Cds.FieldValues['TQQUD10']:=ff_TQQUD10;
     if flag_TQQUD11 then
        Cds.FieldValues['TQQUD11']:=ff_TQQUD11;
     if flag_TQQUD12 then
        Cds.FieldValues['TQQUD12']:=ff_TQQUD12;
     if flag_TQQUD13 then
        Cds.FieldValues['TQQUD13']:=ff_TQQUD13;
     if flag_TQQUD14 then
        Cds.FieldValues['TQQUD14']:=ff_TQQUD14;
     if flag_TQQUD15 then
        Cds.FieldValues['TQQUD15']:=ff_TQQUD15;
     if flag_TQQPLANT then
        Cds.FieldValues['TQQPLANT']:=ff_TQQPLANT;
     if flag_TQQLEGAL then
        Cds.FieldValues['TQQLEGAL']:=ff_TQQLEGAL;
     Cds.Post;
  except
     Cds.Free;
     Result:=false;
     exit;
  end;
  Result:=DBA.AppendRecord('TQQ_FILE',Cds);
  Cds.Free;
end;

//
// 将对象实例的属性更新到数据库记录（更新全部字段）...
function TTQQ_FILE.UpdateToDB(DBA: TRemoteUnidac; Condition: String): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQQ_FILE.CreateCds;
  try
     Cds.Append;
     if flag_TQQ01 then
        Cds.FieldValues['TQQ01']:=ff_TQQ01;
     if flag_TQQ02 then
        Cds.FieldValues['TQQ02']:=ff_TQQ02;
     if flag_TQQ03 then
        Cds.FieldValues['TQQ03']:=ff_TQQ03;
     if flag_TQQ04 then
        Cds.FieldValues['TQQ04']:=ff_TQQ04;
     if flag_TQQ05 then
        Cds.FieldValues['TQQ05']:=ff_TQQ05;
     if flag_TQQ06 then
        Cds.FieldValues['TQQ06']:=ff_TQQ06;
     if flag_TQQUD01 then
        Cds.FieldValues['TQQUD01']:=ff_TQQUD01;
     if flag_TQQUD02 then
        Cds.FieldValues['TQQUD02']:=ff_TQQUD02;
     if flag_TQQUD03 then
        Cds.FieldValues['TQQUD03']:=ff_TQQUD03;
     if flag_TQQUD04 then
        Cds.FieldValues['TQQUD04']:=ff_TQQUD04;
     if flag_TQQUD05 then
        Cds.FieldValues['TQQUD05']:=ff_TQQUD05;
     if flag_TQQUD06 then
        Cds.FieldValues['TQQUD06']:=ff_TQQUD06;
     if flag_TQQUD07 then
        Cds.FieldValues['TQQUD07']:=ff_TQQUD07;
     if flag_TQQUD08 then
        Cds.FieldValues['TQQUD08']:=ff_TQQUD08;
     if flag_TQQUD09 then
        Cds.FieldValues['TQQUD09']:=ff_TQQUD09;
     if flag_TQQUD10 then
        Cds.FieldValues['TQQUD10']:=ff_TQQUD10;
     if flag_TQQUD11 then
        Cds.FieldValues['TQQUD11']:=ff_TQQUD11;
     if flag_TQQUD12 then
        Cds.FieldValues['TQQUD12']:=ff_TQQUD12;
     if flag_TQQUD13 then
        Cds.FieldValues['TQQUD13']:=ff_TQQUD13;
     if flag_TQQUD14 then
        Cds.FieldValues['TQQUD14']:=ff_TQQUD14;
     if flag_TQQUD15 then
        Cds.FieldValues['TQQUD15']:=ff_TQQUD15;
     if flag_TQQPLANT then
        Cds.FieldValues['TQQPLANT']:=ff_TQQPLANT;
     if flag_TQQLEGAL then
        Cds.FieldValues['TQQLEGAL']:=ff_TQQLEGAL;
     Cds.Post;
  except
     Cds.Free;
     Result:=false;
     exit;
  end;
  Result:=DBA.UpdateRecord('TQQ_FILE',Condition,Cds);
  Cds.Free;
end;

//
// 将对象实例的属性更新到数据库记录（更新修改了的字段）...
function TTQQ_FILE.UpdateToDB2(DBA: TRemoteUnidac; Condition: String): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQQ_FILE.CreateCds;
  try
     Cds.Append;
     if flag_TQQ01 then
        Cds.FieldValues['TQQ01']:=ff_TQQ01;
     if flag_TQQ02 then
        Cds.FieldValues['TQQ02']:=ff_TQQ02;
     if flag_TQQ03 then
        Cds.FieldValues['TQQ03']:=ff_TQQ03;
     if flag_TQQ04 then
        Cds.FieldValues['TQQ04']:=ff_TQQ04;
     if flag_TQQ05 then
        Cds.FieldValues['TQQ05']:=ff_TQQ05;
     if flag_TQQ06 then
        Cds.FieldValues['TQQ06']:=ff_TQQ06;
     if flag_TQQUD01 then
        Cds.FieldValues['TQQUD01']:=ff_TQQUD01;
     if flag_TQQUD02 then
        Cds.FieldValues['TQQUD02']:=ff_TQQUD02;
     if flag_TQQUD03 then
        Cds.FieldValues['TQQUD03']:=ff_TQQUD03;
     if flag_TQQUD04 then
        Cds.FieldValues['TQQUD04']:=ff_TQQUD04;
     if flag_TQQUD05 then
        Cds.FieldValues['TQQUD05']:=ff_TQQUD05;
     if flag_TQQUD06 then
        Cds.FieldValues['TQQUD06']:=ff_TQQUD06;
     if flag_TQQUD07 then
        Cds.FieldValues['TQQUD07']:=ff_TQQUD07;
     if flag_TQQUD08 then
        Cds.FieldValues['TQQUD08']:=ff_TQQUD08;
     if flag_TQQUD09 then
        Cds.FieldValues['TQQUD09']:=ff_TQQUD09;
     if flag_TQQUD10 then
        Cds.FieldValues['TQQUD10']:=ff_TQQUD10;
     if flag_TQQUD11 then
        Cds.FieldValues['TQQUD11']:=ff_TQQUD11;
     if flag_TQQUD12 then
        Cds.FieldValues['TQQUD12']:=ff_TQQUD12;
     if flag_TQQUD13 then
        Cds.FieldValues['TQQUD13']:=ff_TQQUD13;
     if flag_TQQUD14 then
        Cds.FieldValues['TQQUD14']:=ff_TQQUD14;
     if flag_TQQUD15 then
        Cds.FieldValues['TQQUD15']:=ff_TQQUD15;
     if flag_TQQPLANT then
        Cds.FieldValues['TQQPLANT']:=ff_TQQPLANT;
     if flag_TQQLEGAL then
        Cds.FieldValues['TQQLEGAL']:=ff_TQQLEGAL;
     Cds.Post;
  except
     Cds.Free;
     Result:=false;
     exit;
  end;
  Result:=DBA.UpdateRecord('TQQ_FILE',Condition,Cds,True);
  Cds.Free;
end;

//
// 删除对象实例所对应的数据库记录...
function TTQQ_FILE.DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
var
   SqlCommand,tmpstr: String;
begin
  if Condition='' then
     SqlCommand:='DELETE FROM TQQ_FILE'
  else
     SqlCommand:='DELETE FROM TQQ_FILE WHERE '+condition;
  result:=DBA.ExecuteSQL(SqlCommand,false,tmpstr);
end;

//
// 判定属性是否null的函数...
function TTQQ_FILE.PropIsNull(PropName: string): boolean;
begin
  result:=true;
  if StrIComp(PChar(PropName),PChar('TQQ01'))=0 then
     begin
        result:=not flag_TQQ01;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ02'))=0 then
     begin
        result:=not flag_TQQ02;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ03'))=0 then
     begin
        result:=not flag_TQQ03;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ04'))=0 then
     begin
        result:=not flag_TQQ04;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ05'))=0 then
     begin
        result:=not flag_TQQ05;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ06'))=0 then
     begin
        result:=not flag_TQQ06;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD01'))=0 then
     begin
        result:=not flag_TQQUD01;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD02'))=0 then
     begin
        result:=not flag_TQQUD02;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD03'))=0 then
     begin
        result:=not flag_TQQUD03;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD04'))=0 then
     begin
        result:=not flag_TQQUD04;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD05'))=0 then
     begin
        result:=not flag_TQQUD05;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD06'))=0 then
     begin
        result:=not flag_TQQUD06;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD07'))=0 then
     begin
        result:=not flag_TQQUD07;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD08'))=0 then
     begin
        result:=not flag_TQQUD08;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD09'))=0 then
     begin
        result:=not flag_TQQUD09;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD10'))=0 then
     begin
        result:=not flag_TQQUD10;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD11'))=0 then
     begin
        result:=not flag_TQQUD11;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD12'))=0 then
     begin
        result:=not flag_TQQUD12;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD13'))=0 then
     begin
        result:=not flag_TQQUD13;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD14'))=0 then
     begin
        result:=not flag_TQQUD14;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD15'))=0 then
     begin
        result:=not flag_TQQUD15;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQPLANT'))=0 then
     begin
        result:=not flag_TQQPLANT;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQLEGAL'))=0 then
     begin
        result:=not flag_TQQLEGAL;
        exit;
     end;
end;

//
// 设置某属性的值为null...
function TTQQ_FILE.SetPropNull(PropName: string): boolean;
begin
  result:=false;
  if StrIComp(PChar(PropName),PChar('TQQ01'))=0 then
     begin
        ff_TQQ01:='';
        flag_TQQ01:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ02'))=0 then
     begin
        ff_TQQ02:=0;
        flag_TQQ02:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ03'))=0 then
     begin
        ff_TQQ03:='';
        flag_TQQ03:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ04'))=0 then
     begin
        ff_TQQ04:='';
        flag_TQQ04:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ05'))=0 then
     begin
        ff_TQQ05:='';
        flag_TQQ05:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQ06'))=0 then
     begin
        ff_TQQ06:='';
        flag_TQQ06:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD01'))=0 then
     begin
        ff_TQQUD01:='';
        flag_TQQUD01:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD02'))=0 then
     begin
        ff_TQQUD02:='';
        flag_TQQUD02:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD03'))=0 then
     begin
        ff_TQQUD03:='';
        flag_TQQUD03:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD04'))=0 then
     begin
        ff_TQQUD04:='';
        flag_TQQUD04:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD05'))=0 then
     begin
        ff_TQQUD05:='';
        flag_TQQUD05:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD06'))=0 then
     begin
        ff_TQQUD06:='';
        flag_TQQUD06:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD07'))=0 then
     begin
        ff_TQQUD07:=0.0;
        flag_TQQUD07:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD08'))=0 then
     begin
        ff_TQQUD08:=0.0;
        flag_TQQUD08:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD09'))=0 then
     begin
        ff_TQQUD09:=0.0;
        flag_TQQUD09:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD10'))=0 then
     begin
        ff_TQQUD10:=0;
        flag_TQQUD10:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD11'))=0 then
     begin
        ff_TQQUD11:=0;
        flag_TQQUD11:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD12'))=0 then
     begin
        ff_TQQUD12:=0;
        flag_TQQUD12:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD13'))=0 then
     begin
        ff_TQQUD13:=0;
        flag_TQQUD13:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD14'))=0 then
     begin
        ff_TQQUD14:=0;
        flag_TQQUD14:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQUD15'))=0 then
     begin
        ff_TQQUD15:=0;
        flag_TQQUD15:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQPLANT'))=0 then
     begin
        ff_TQQPLANT:='';
        flag_TQQPLANT:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQQLEGAL'))=0 then
     begin
        ff_TQQLEGAL:='';
        flag_TQQLEGAL:=false;
        result:=true;
        exit;
     end;
end;

//
// 设置某属性的值为null...
function TTQQ_FILE.CloneTo(var NewObj: TTQQ_FILE): boolean;
begin
   NewObj.f_TQQ01:=ff_TQQ01;
   NewObj.f_TQQ02:=ff_TQQ02;
   NewObj.f_TQQ03:=ff_TQQ03;
   NewObj.f_TQQ04:=ff_TQQ04;
   NewObj.f_TQQ05:=ff_TQQ05;
   NewObj.f_TQQ06:=ff_TQQ06;
   NewObj.f_TQQUD01:=ff_TQQUD01;
   NewObj.f_TQQUD02:=ff_TQQUD02;
   NewObj.f_TQQUD03:=ff_TQQUD03;
   NewObj.f_TQQUD04:=ff_TQQUD04;
   NewObj.f_TQQUD05:=ff_TQQUD05;
   NewObj.f_TQQUD06:=ff_TQQUD06;
   NewObj.f_TQQUD07:=ff_TQQUD07;
   NewObj.f_TQQUD08:=ff_TQQUD08;
   NewObj.f_TQQUD09:=ff_TQQUD09;
   NewObj.f_TQQUD10:=ff_TQQUD10;
   NewObj.f_TQQUD11:=ff_TQQUD11;
   NewObj.f_TQQUD12:=ff_TQQUD12;
   NewObj.f_TQQUD13:=ff_TQQUD13;
   NewObj.f_TQQUD14:=ff_TQQUD14;
   NewObj.f_TQQUD15:=ff_TQQUD15;
   NewObj.f_TQQPLANT:=ff_TQQPLANT;
   NewObj.f_TQQLEGAL:=ff_TQQLEGAL;
   Result:=true;
end;


{ TTQQ_FILEList }

//
// 创建列表对象实例...
constructor TTQQ_FILEList.Create;
begin
  inherited Create;
end;

//
// 释放列表对象实例...
destructor TTQQ_FILEList.Destroy;
var
  I: Integer;
  Obj: TTQQ_FILE;
begin
  for I := 0 to fCount - 1 do
     begin
        Obj := TTQQ_FILE(fList[I]);
        Obj.Free;
     end;
  inherited;
end;

//
// 清除对象...
procedure TTQQ_FILEList.Clear;
begin
  fcount:=0;
  setlength(flist,0);
end;

//
// 增加对象...
procedure TTQQ_FILEList.add(Obj: TTQQ_FILE);
begin
   inc(fCount);
   setlength(flist,fcount);
   flist[fcount-1]:=obj;
end;

//
// 删除对象...
procedure TTQQ_FILEList.Delete(Index: Integer);
var
   i: integer;
begin
   FreeAndNil(flist[Index]);
   for i:=index to fcount-2 do
      flist[i]:=flist[i+1];
   dec(fCount);
   setlength(flist,fcount);
end;

//
// 取一个元素...
function TTQQ_FILEList.Get(Index: Integer): Pointer;
begin
  Result := FList[Index];
end;

//
// Put方法...
procedure TTQQ_FILEList.Put(Index: Integer; Item: Pointer);
begin
  if Item <> FList[Index] then
    FList[Index] := Item;
end;

//
// 从远程数据表读取记录并转换为列表对象实例...
class function TTQQ_FILEList.ReadFromDB(DBA: TRemoteUnidac; Condition: String = ''; 
      OrderBy: String = ''; SelectOption: string=''): TTQQ_FILEList;
var
  SqlCommand: String;
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
begin
  Cds:=TClientDataset.Create(nil);
  if Condition='' then
     begin
        SqlCommand := 'SELECT '+SelectOption+' '
                    +'TQQ01,'
                    +'TQQ02,'
                    +'TQQ03,'
                    +'TQQ04,'
                    +'TQQ05,'
                    +'TQQ06,'
                    +'TQQUD01,'
                    +'TQQUD02,'
                    +'TQQUD03,'
                    +'TQQUD04,'
                    +'TQQUD05,'
                    +'TQQUD06,'
                    +'TQQUD07,'
                    +'TQQUD08,'
                    +'TQQUD09,'
                    +'TQQUD10,'
                    +'TQQUD11,'
                    +'TQQUD12,'
                    +'TQQUD13,'
                    +'TQQUD14,'
                    +'TQQUD15,'
                    +'TQQPLANT,'
                    +'TQQLEGAL'
                    +' FROM TQQ_FILE'
     end
  else
     begin
        SqlCommand := 'SELECT '+SelectOption+' '
                    +'TQQ01,'
                    +'TQQ02,'
                    +'TQQ03,'
                    +'TQQ04,'
                    +'TQQ05,'
                    +'TQQ06,'
                    +'TQQUD01,'
                    +'TQQUD02,'
                    +'TQQUD03,'
                    +'TQQUD04,'
                    +'TQQUD05,'
                    +'TQQUD06,'
                    +'TQQUD07,'
                    +'TQQUD08,'
                    +'TQQUD09,'
                    +'TQQUD10,'
                    +'TQQUD11,'
                    +'TQQUD12,'
                    +'TQQUD13,'
                    +'TQQUD14,'
                    +'TQQUD15,'
                    +'TQQPLANT,'
                    +'TQQLEGAL'
                    +' FROM TQQ_FILE WHERE '+Condition;
     end;
  if Orderby<>'' then
     SqlCommand:=SqlCommand+' ORDER BY '+Orderby;
  if not DBA.ReadDataset(SqlCommand,Cds) then
     begin
        Result:=nil;
        Cds.Free;
        exit;
     end;
  Result := TTQQ_FILEList.Create;
  Cds.First;
  while not Cds.Eof do
     begin
        TmpObj := TTQQ_FILE.ReadFromCDS(Cds);
        Result.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
end;

//
// 将本地Cds中的记录集转换为列表对象实例...
class function TTQQ_FILEList.ReadFromCDS(Cds: TDataset): TTQQ_FILEList;
var
  TmpObj: TTQQ_FILE;
begin
  Result := TTQQ_FILEList.Create;
  Cds.First;
  while not Cds.Eof do
     begin
        TmpObj := TTQQ_FILE.ReadFromCDS(Cds);
        Result.Add(TmpObj);
        Cds.Next;
     end;
end;

//
// 将列表对象包含的一批对象保存为Cds中的若干记录...
function TTQQ_FILEList.SaveToCds(Cds: TDataset): boolean;
var
  TmpObj: TTQQ_FILE;
  i: integer;
begin
  Result:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQQ01_Updated then
              Cds.FieldValues['TQQ01']:=TmpObj.f_TQQ01;
           if TmpObj.TQQ02_Updated then
              Cds.FieldValues['TQQ02']:=TmpObj.f_TQQ02;
           if TmpObj.TQQ03_Updated then
              Cds.FieldValues['TQQ03']:=TmpObj.f_TQQ03;
           if TmpObj.TQQ04_Updated then
              Cds.FieldValues['TQQ04']:=TmpObj.f_TQQ04;
           if TmpObj.TQQ05_Updated then
              Cds.FieldValues['TQQ05']:=TmpObj.f_TQQ05;
           if TmpObj.TQQ06_Updated then
              Cds.FieldValues['TQQ06']:=TmpObj.f_TQQ06;
           if TmpObj.TQQUD01_Updated then
              Cds.FieldValues['TQQUD01']:=TmpObj.f_TQQUD01;
           if TmpObj.TQQUD02_Updated then
              Cds.FieldValues['TQQUD02']:=TmpObj.f_TQQUD02;
           if TmpObj.TQQUD03_Updated then
              Cds.FieldValues['TQQUD03']:=TmpObj.f_TQQUD03;
           if TmpObj.TQQUD04_Updated then
              Cds.FieldValues['TQQUD04']:=TmpObj.f_TQQUD04;
           if TmpObj.TQQUD05_Updated then
              Cds.FieldValues['TQQUD05']:=TmpObj.f_TQQUD05;
           if TmpObj.TQQUD06_Updated then
              Cds.FieldValues['TQQUD06']:=TmpObj.f_TQQUD06;
           if TmpObj.TQQUD07_Updated then
              Cds.FieldValues['TQQUD07']:=TmpObj.f_TQQUD07;
           if TmpObj.TQQUD08_Updated then
              Cds.FieldValues['TQQUD08']:=TmpObj.f_TQQUD08;
           if TmpObj.TQQUD09_Updated then
              Cds.FieldValues['TQQUD09']:=TmpObj.f_TQQUD09;
           if TmpObj.TQQUD10_Updated then
              Cds.FieldValues['TQQUD10']:=TmpObj.f_TQQUD10;
           if TmpObj.TQQUD11_Updated then
              Cds.FieldValues['TQQUD11']:=TmpObj.f_TQQUD11;
           if TmpObj.TQQUD12_Updated then
              Cds.FieldValues['TQQUD12']:=TmpObj.f_TQQUD12;
           if TmpObj.TQQUD13_Updated then
              Cds.FieldValues['TQQUD13']:=TmpObj.f_TQQUD13;
           if TmpObj.TQQUD14_Updated then
              Cds.FieldValues['TQQUD14']:=TmpObj.f_TQQUD14;
           if TmpObj.TQQUD15_Updated then
              Cds.FieldValues['TQQUD15']:=TmpObj.f_TQQUD15;
           if TmpObj.TQQPLANT_Updated then
              Cds.FieldValues['TQQPLANT']:=TmpObj.f_TQQPLANT;
           if TmpObj.TQQLEGAL_Updated then
              Cds.FieldValues['TQQLEGAL']:=TmpObj.f_TQQLEGAL;
           Cds.Post;
        except
           Result:=false;
        end;
        if not Result then
           break;
     end;
end;

//
// 将对象实例的属性写入到远程数据库记录...
function TTQQ_FILEList.InsertToDB(DBA: TRemoteUnidac): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQQ_FILE.CreateCds;
  Result:=Self.SaveToCds(Cds);
  if Result then
     Result:=DBA.WriteDataset(Cds,'TQQ_FILE');
  FreeAndNil(Cds);
end;

//
// 将对象实例的属性更新到数据库记录...
function TTQQ_FILEList.UpdateToDB(DBA: TRemoteUnidac; KeyFieldList: string; Condition: String): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQQ_FILE.CreateCds;
  Result:=Self.SaveToCds(Cds);
  if Result then
     Result:=DBA.UpdateDataset(Cds,'TQQ_FILE',KeyFieldList,Condition);
  FreeAndNil(Cds);
end;

//
// 删除对象实例所对应的数据库记录...
function TTQQ_FILEList.DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
var
   SqlCommand,tmpstr: String;
begin
  SqlCommand:='DELETE FROM TQQ_FILE WHERE '+condition;
  result:=DBA.ExecuteSQL(SqlCommand,false,tmpstr);
end;

//
// 将列表对象包含的一批对象保存到外部文件中...
function TTQQ_FILEList.SaveToFile(FileName: String; DataFormat: TDataPacketFormat): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
  i: integer;
  ok: boolean;
begin
  Cds:=TTQQ_FILE.CreateCds;
  ok:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQQ01_Updated then
              Cds.FieldValues['TQQ01']:=TmpObj.f_TQQ01;
           if TmpObj.TQQ02_Updated then
              Cds.FieldValues['TQQ02']:=TmpObj.f_TQQ02;
           if TmpObj.TQQ03_Updated then
              Cds.FieldValues['TQQ03']:=TmpObj.f_TQQ03;
           if TmpObj.TQQ04_Updated then
              Cds.FieldValues['TQQ04']:=TmpObj.f_TQQ04;
           if TmpObj.TQQ05_Updated then
              Cds.FieldValues['TQQ05']:=TmpObj.f_TQQ05;
           if TmpObj.TQQ06_Updated then
              Cds.FieldValues['TQQ06']:=TmpObj.f_TQQ06;
           if TmpObj.TQQUD01_Updated then
              Cds.FieldValues['TQQUD01']:=TmpObj.f_TQQUD01;
           if TmpObj.TQQUD02_Updated then
              Cds.FieldValues['TQQUD02']:=TmpObj.f_TQQUD02;
           if TmpObj.TQQUD03_Updated then
              Cds.FieldValues['TQQUD03']:=TmpObj.f_TQQUD03;
           if TmpObj.TQQUD04_Updated then
              Cds.FieldValues['TQQUD04']:=TmpObj.f_TQQUD04;
           if TmpObj.TQQUD05_Updated then
              Cds.FieldValues['TQQUD05']:=TmpObj.f_TQQUD05;
           if TmpObj.TQQUD06_Updated then
              Cds.FieldValues['TQQUD06']:=TmpObj.f_TQQUD06;
           if TmpObj.TQQUD07_Updated then
              Cds.FieldValues['TQQUD07']:=TmpObj.f_TQQUD07;
           if TmpObj.TQQUD08_Updated then
              Cds.FieldValues['TQQUD08']:=TmpObj.f_TQQUD08;
           if TmpObj.TQQUD09_Updated then
              Cds.FieldValues['TQQUD09']:=TmpObj.f_TQQUD09;
           if TmpObj.TQQUD10_Updated then
              Cds.FieldValues['TQQUD10']:=TmpObj.f_TQQUD10;
           if TmpObj.TQQUD11_Updated then
              Cds.FieldValues['TQQUD11']:=TmpObj.f_TQQUD11;
           if TmpObj.TQQUD12_Updated then
              Cds.FieldValues['TQQUD12']:=TmpObj.f_TQQUD12;
           if TmpObj.TQQUD13_Updated then
              Cds.FieldValues['TQQUD13']:=TmpObj.f_TQQUD13;
           if TmpObj.TQQUD14_Updated then
              Cds.FieldValues['TQQUD14']:=TmpObj.f_TQQUD14;
           if TmpObj.TQQUD15_Updated then
              Cds.FieldValues['TQQUD15']:=TmpObj.f_TQQUD15;
           if TmpObj.TQQPLANT_Updated then
              Cds.FieldValues['TQQPLANT']:=TmpObj.f_TQQPLANT;
           if TmpObj.TQQLEGAL_Updated then
              Cds.FieldValues['TQQLEGAL']:=TmpObj.f_TQQLEGAL;
           Cds.Post;
        except
           ok:=false;
        end;
        if not ok then
           break;
     end;
  if ok then
     begin
        try
           Cds.SaveToFile(String(FileName),DataFormat);
        except
           ok:=false;
        end;
     end;
  Cds.Free;
  result:=ok;
end;

//
// 从文件中装入数据，转换为列表对象中的一系列对象...
function TTQQ_FILEList.LoadFromFile(FileName: String): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
  ok: boolean;
begin
  self.Clear;
  Cds:=TClientDataset.Create(nil);
  try
     Cds.LoadFromFile(String(FileName));
     ok:=Cds.Active;
  except
     ok:=false;
  end;
  if not ok then
     begin
        cds.Free;
        result:=false;
        exit;
     end;
  cds.First;
  while not cds.Eof do
     begin
        TmpObj := TTQQ_FILE.ReadFromCDS(Cds);
        Self.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
  result:=true;
end;

//
// 保存列表对象中所含数据到内存流对象...
function TTQQ_FILEList.SaveToStream(aStream: TMemoryStream; DataFormat: TDataPacketFormat): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
  i: integer;
  ok: boolean;
begin
  Cds:=TTQQ_FILE.CreateCds;
  ok:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQQ01_Updated then
              Cds.FieldValues['TQQ01']:=TmpObj.f_TQQ01;
           if TmpObj.TQQ02_Updated then
              Cds.FieldValues['TQQ02']:=TmpObj.f_TQQ02;
           if TmpObj.TQQ03_Updated then
              Cds.FieldValues['TQQ03']:=TmpObj.f_TQQ03;
           if TmpObj.TQQ04_Updated then
              Cds.FieldValues['TQQ04']:=TmpObj.f_TQQ04;
           if TmpObj.TQQ05_Updated then
              Cds.FieldValues['TQQ05']:=TmpObj.f_TQQ05;
           if TmpObj.TQQ06_Updated then
              Cds.FieldValues['TQQ06']:=TmpObj.f_TQQ06;
           if TmpObj.TQQUD01_Updated then
              Cds.FieldValues['TQQUD01']:=TmpObj.f_TQQUD01;
           if TmpObj.TQQUD02_Updated then
              Cds.FieldValues['TQQUD02']:=TmpObj.f_TQQUD02;
           if TmpObj.TQQUD03_Updated then
              Cds.FieldValues['TQQUD03']:=TmpObj.f_TQQUD03;
           if TmpObj.TQQUD04_Updated then
              Cds.FieldValues['TQQUD04']:=TmpObj.f_TQQUD04;
           if TmpObj.TQQUD05_Updated then
              Cds.FieldValues['TQQUD05']:=TmpObj.f_TQQUD05;
           if TmpObj.TQQUD06_Updated then
              Cds.FieldValues['TQQUD06']:=TmpObj.f_TQQUD06;
           if TmpObj.TQQUD07_Updated then
              Cds.FieldValues['TQQUD07']:=TmpObj.f_TQQUD07;
           if TmpObj.TQQUD08_Updated then
              Cds.FieldValues['TQQUD08']:=TmpObj.f_TQQUD08;
           if TmpObj.TQQUD09_Updated then
              Cds.FieldValues['TQQUD09']:=TmpObj.f_TQQUD09;
           if TmpObj.TQQUD10_Updated then
              Cds.FieldValues['TQQUD10']:=TmpObj.f_TQQUD10;
           if TmpObj.TQQUD11_Updated then
              Cds.FieldValues['TQQUD11']:=TmpObj.f_TQQUD11;
           if TmpObj.TQQUD12_Updated then
              Cds.FieldValues['TQQUD12']:=TmpObj.f_TQQUD12;
           if TmpObj.TQQUD13_Updated then
              Cds.FieldValues['TQQUD13']:=TmpObj.f_TQQUD13;
           if TmpObj.TQQUD14_Updated then
              Cds.FieldValues['TQQUD14']:=TmpObj.f_TQQUD14;
           if TmpObj.TQQUD15_Updated then
              Cds.FieldValues['TQQUD15']:=TmpObj.f_TQQUD15;
           if TmpObj.TQQPLANT_Updated then
              Cds.FieldValues['TQQPLANT']:=TmpObj.f_TQQPLANT;
           if TmpObj.TQQLEGAL_Updated then
              Cds.FieldValues['TQQLEGAL']:=TmpObj.f_TQQLEGAL;
           Cds.Post;
        except
           ok:=false;
        end;
        if not ok then
           break;
     end;
  if ok then
     begin
        try
           aStream.Clear;
           Cds.SaveToStream(aStream,DataFormat);
        except
           ok:=false;
        end;
     end;
  Cds.Free;
  result:=ok;
end;

//
// 从内存流对象装入数据到列表对象中...
function TTQQ_FILEList.LoadFromStream(aStream: TMemoryStream): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
  ok: boolean;
begin
  self.Clear;
  Cds:=TClientDataset.Create(nil);
  try
     aStream.Position:=0;
     Cds.LoadFromStream(aStream);
     ok:=Cds.Active;
  except
     ok:=false;
  end;
  if not ok then
     begin
        cds.Free;
        result:=false;
        exit;
     end;
  cds.First;
  while not cds.Eof do
     begin
        TmpObj := TTQQ_FILE.ReadFromCDS(Cds);
        Self.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
  result:=true;
end;

{$IFDEF MSWINDOWS}
//
// 保存列表对象数据到数据包裹对象中...
function TTQQ_FILEList.SaveToParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
  i: integer;
  ok: boolean;
begin
  Cds:=TTQQ_FILE.CreateCds;
  ok:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQQ01_Updated then
              Cds.FieldValues['TQQ01']:=TmpObj.f_TQQ01;
           if TmpObj.TQQ02_Updated then
              Cds.FieldValues['TQQ02']:=TmpObj.f_TQQ02;
           if TmpObj.TQQ03_Updated then
              Cds.FieldValues['TQQ03']:=TmpObj.f_TQQ03;
           if TmpObj.TQQ04_Updated then
              Cds.FieldValues['TQQ04']:=TmpObj.f_TQQ04;
           if TmpObj.TQQ05_Updated then
              Cds.FieldValues['TQQ05']:=TmpObj.f_TQQ05;
           if TmpObj.TQQ06_Updated then
              Cds.FieldValues['TQQ06']:=TmpObj.f_TQQ06;
           if TmpObj.TQQUD01_Updated then
              Cds.FieldValues['TQQUD01']:=TmpObj.f_TQQUD01;
           if TmpObj.TQQUD02_Updated then
              Cds.FieldValues['TQQUD02']:=TmpObj.f_TQQUD02;
           if TmpObj.TQQUD03_Updated then
              Cds.FieldValues['TQQUD03']:=TmpObj.f_TQQUD03;
           if TmpObj.TQQUD04_Updated then
              Cds.FieldValues['TQQUD04']:=TmpObj.f_TQQUD04;
           if TmpObj.TQQUD05_Updated then
              Cds.FieldValues['TQQUD05']:=TmpObj.f_TQQUD05;
           if TmpObj.TQQUD06_Updated then
              Cds.FieldValues['TQQUD06']:=TmpObj.f_TQQUD06;
           if TmpObj.TQQUD07_Updated then
              Cds.FieldValues['TQQUD07']:=TmpObj.f_TQQUD07;
           if TmpObj.TQQUD08_Updated then
              Cds.FieldValues['TQQUD08']:=TmpObj.f_TQQUD08;
           if TmpObj.TQQUD09_Updated then
              Cds.FieldValues['TQQUD09']:=TmpObj.f_TQQUD09;
           if TmpObj.TQQUD10_Updated then
              Cds.FieldValues['TQQUD10']:=TmpObj.f_TQQUD10;
           if TmpObj.TQQUD11_Updated then
              Cds.FieldValues['TQQUD11']:=TmpObj.f_TQQUD11;
           if TmpObj.TQQUD12_Updated then
              Cds.FieldValues['TQQUD12']:=TmpObj.f_TQQUD12;
           if TmpObj.TQQUD13_Updated then
              Cds.FieldValues['TQQUD13']:=TmpObj.f_TQQUD13;
           if TmpObj.TQQUD14_Updated then
              Cds.FieldValues['TQQUD14']:=TmpObj.f_TQQUD14;
           if TmpObj.TQQUD15_Updated then
              Cds.FieldValues['TQQUD15']:=TmpObj.f_TQQUD15;
           if TmpObj.TQQPLANT_Updated then
              Cds.FieldValues['TQQPLANT']:=TmpObj.f_TQQPLANT;
           if TmpObj.TQQLEGAL_Updated then
              Cds.FieldValues['TQQLEGAL']:=TmpObj.f_TQQLEGAL;
           Cds.Post;
        except
           ok:=false;
        end;
        if not ok then
           break;
     end;
  if ok then
     begin
        try
           aParcel.PutCDSGoods(GoodsName,Cds);
           ok:=true;
        except
           ok:=false;
        end;
     end;
  Cds.Free;
  result:=ok;
end;

//
// 从数据包裹对象中装入数据到一个列表对象......
function TTQQ_FILEList.LoadFromParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQQ_FILE;
  ok: boolean;
begin
  self.Clear;
  Cds:=TClientDataset.Create(nil);
  try
     aParcel.GetCDSGoods(GoodsName,Cds);
     ok:=Cds.Active;
  except
     ok:=false;
  end;
  if not ok then
     begin
        cds.Free;
        result:=false;
        exit;
     end;
  cds.First;
  while not cds.Eof do
     begin
        TmpObj := TTQQ_FILE.ReadFromCDS(Cds);
        Self.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
  result:=true;
end;
{$ENDIF}

//
// 列表对象克隆...
function TTQQ_FILEList.CloneTo(NewList: TTQQ_FILEList): boolean;
var
   i: integer;
   NewObj: TTQQ_FILE;
begin
   for i:=0 to Self.Count-1 do
      begin
         NewObj:=TTQQ_FILE.Create;
         TTQQ_FILE(Self.Items[i]).CloneTo(NewObj);
         NewList.Add(NewObj);
      end;
   Result:=true;
end;

end.
