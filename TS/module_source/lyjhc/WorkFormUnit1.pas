﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters,cxStyles,cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridCustomView, cxGrid, cxClasses, math,cxGridExportLink;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    ToXlsMX: TButton;
    xmF: TPanel;
    Panel5: TPanel;
    Bevel3: TBevel;
    xmsa: TButton;
    xmcl: TButton;
    xmok: TButton;
    xmca: TButton;
    xmcb: TAdvStringGrid;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    MGrid: TcxGrid;
    MTV: TcxGridBandedTableView;
    MTVColumn1: TcxGridBandedColumn;
    MTVColumn2: TcxGridBandedColumn;
    MTVColumn3: TcxGridBandedColumn;
    MTVColumn4: TcxGridBandedColumn;
    MTVColumn5: TcxGridBandedColumn;
    MTVColumn6: TcxGridBandedColumn;
    MTVColumn7: TcxGridBandedColumn;
    MTVColumn8: TcxGridBandedColumn;
    MTVColumn9: TcxGridBandedColumn;
    MTVColumn10: TcxGridBandedColumn;
    MTVColumn11: TcxGridBandedColumn;
    MTVColumn12: TcxGridBandedColumn;
    MTVColumn13: TcxGridBandedColumn;
    MTVColumn14: TcxGridBandedColumn;
    MTVColumn15: TcxGridBandedColumn;
    MTVColumn16: TcxGridBandedColumn;
    MTVColumn17: TcxGridBandedColumn;
    MTVColumn18: TcxGridBandedColumn;
    MTVColumn19: TcxGridBandedColumn;
    MTVColumn20: TcxGridBandedColumn;
    MTVColumn21: TcxGridBandedColumn;
    MTVColumn22: TcxGridBandedColumn;
    MTVColumn25: TcxGridBandedColumn;
    MTVColumn26: TcxGridBandedColumn;
    MTVColumn27: TcxGridBandedColumn;
    MTVColumn28: TcxGridBandedColumn;
    MTVColumn23: TcxGridBandedColumn;
    MTVColumn24: TcxGridBandedColumn;
    MGridLevel1: TcxGridLevel;
    MTVColumn29: TcxGridBandedColumn;
    MTVColumn31: TcxGridBandedColumn;
    MTVColumn32: TcxGridBandedColumn;
    MTVColumn33: TcxGridBandedColumn;
    MTVColumn34: TcxGridBandedColumn;
    MTVColumn30: TcxGridBandedColumn;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    MTVColumn35: TcxGridBandedColumn;
    MTVColumn36: TcxGridBandedColumn;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure ztxzClick(Sender: TObject);
    procedure qxmClick(Sender: TObject);
    procedure xmclClick(Sender: TObject);
    procedure xmokClick(Sender: TObject);
    procedure xmsaClick(Sender: TObject);
    procedure xmcaClick(Sender: TObject);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure MTVCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function XmCount(const zh: string): Integer;
    procedure XmcbClose;
    procedure XmcbOpen;
    procedure SetTimeRange;
    function XmcbSelectCount: Integer;
    function RunCDS(SQL: string): Boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet; tp: Integer = 1);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure WaitMessage;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'LYJHC_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  // 获取插件formCaption
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage;   //同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now;

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  TimeP.Visible := false;

  // 获取是否可修改项目编号权限,设置xeLE的只读性
  GetPrivilege('ASSERT','1',zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    xmLE.ReadOnly := true
    else
    xmLE.ReadOnly := PrivilegeCDS.FieldByName('R1').AsString = '0';
end;

// -----------------------------------------------------------------------------
// 业务代码
Const

  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';

  // 显示的料件不仅要包括在指定时间段内有变化的，还包括在期初时间时点库存数不为0指定时间内无变化的部分   有库存指现场有库存,不包括在线
  STEP1SQL =
    'SELECT DISTINCT tlf01 FROM %stlf_file WHERE SUBSTR(tlf904,INSTR(tlf904,''-'',1)+1,5) = ''%s'' AND LENGTH(tlf902) = 7 AND %s '
    + #13#10 + 'UNION                  ' + #13#10 + 'SELECT na FROM         ' +
    #13#10 + '(                      ' + #13#10 +
    'SELECT tlf01 na,SUM(tlf907*tlf10) s FROM tlf_file WHERE regexp_like(tlf904,''%1:s$'') AND LENGTH(tlf902) = 7 AND %3:s AND tlf907 IN (-1,1) GROUP BY tlf01   '
    + #13#10 + 'UNION ALL              ' + #13#10 +
    'SELECT imk01 na,sum(imk09) s FROM imk_file WHERE regexp_like(imk04,''%1:s$'') AND LENGTH(imk02) = 7 AND imk05 = %4:d  AND imk06 = %5:d GROUP BY imk01  '
    + #13#10 + ') GROUP BY na HAVING SUM(s) >0 OR SUM(s) < 0   ';

  STEP2SQL = 'SELECT imn03,  ' + #13#10 +
    'SUM(DECODE(imm14,''%s'',imn32,NULL)) ljs,    ' + #13#10 +
    'SUM(DECODE(imm14,''%0:s'',imn35,NULL)) ljz,  ' + #13#10 +
    'SUM(DECODE(imm14,''%0:s'',NULL,imn32)) lcs,  ' + #13#10 +
    'SUM(DECODE(imm14,''%0:s'',NULL,imn35)) lcz   ' + #13#10 +
    'FROM %1:simn_file,%1:simm_file            ' + #13#10 +
    'WHERE imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''^CL01'') AND imn06 <> SUBSTR(imn17,1,LENGTH(imn06)) AND (imm14 = ''%0:s'' OR regexp_like(imn06,''%0:s$'')) AND  %2:s '
    + #13#10 + 'GROUP BY imn03';

  STEP3SQL = 'SELECT imn03,SUM(imn42) shs,SUM(imn45) shz  ' + #13#10 +
    'FROM %simn_file,%0:simm_file                ' + #13#10 +
    'WHERE imm01 = imn01 AND imm03 = ''Y'' AND regexp_like(imm01,''^SH'') AND imm14 = ''%1:s'' AND %2:s '
    + #13#10 + 'GROUP BY imn03  ';
  STEP4SQL = 'SELECT imn03,  ' + #13#10 +
    //'SUM(DECODE(ta_imm07,''%s'',imn32,NULL)) bjs,    ' + #13#10 +
    'SUM(CASE WHEN (ta_imm07 = ''%s'' OR SUBSTR(imn17,INSTR(imn17,''-'',-1)+1) = ''%0:s'') THEN imn32 ELSE NULL END) bjs,' + #13#10 +
    //'SUM(DECODE(ta_imm07,''%0:s'',imn35,NULL)) bjz,  ' + #13#10 +
    'SUM(CASE WHEN (ta_imm07 = ''%0:s'' OR SUBSTR(imn17,INSTR(imn17,''-'',-1)+1) = ''%0:s'') THEN imn35 ELSE NULL END) bjz,' + #13#10 +
    //'SUM(DECODE(ta_imm05,''%0:s'',imn42,NULL)) bcs,  ' + #13#10 +
    'SUM(CASE WHEN (ta_imm05 = ''%0:s'' OR SUBSTR(imn06,INSTR(imn06,''-'',-1)+1) = ''%0:s'') THEN imn42 ELSE NULL END) bcs,' + #13#10 +
    //'SUM(DECODE(ta_imm05,''%0:s'',imn45,NULL)) bcz   ' + #13#10 +
    'SUM(CASE WHEN (ta_imm05 = ''%0:s'' OR SUBSTR(imn06,INSTR(imn06,''-'',-1)+1) = ''%0:s'') THEN imn45 ELSE NULL END) bcz ' + #13#10 +
    'FROM %1:simn_file,%1:simm_file                      ' + #13#10 +
    'WHERE imm01 = imn01 AND imm03 = ''Y'' AND regexp_like(imm01,''^XD'') AND (ta_imm05 = ''%0:s'' OR ta_imm07 = ''%0:s'' OR imm14 = ''%0:s'') AND %2:s '
    + #13#10 + 'GROUP BY imn03  ';

  STEP5SQL =
    'SELECT imn03,A_T2 xqs,A_T5 xqz,B_T2 wxs,B_T5 wxz,C_T2 fts,C_T5 ftz FROM ' +
    #13#10 + '(  ' + #13#10 +
    'SELECT imn03,immud03,imn32,imn35 FROM %simn_file,%0:simm_file WHERE imm01 = imn01 AND  regexp_like(imm01,''^CL01'') AND imm14 = ''%s'' AND imm03 = ''Y'' AND %s '
    + #13#10 + ') ' + #13#10 + 'PIVOT  ' + #13#10 +
    '(SUM(imn32) T2,SUM(imn35) T5 FOR immud03 IN (''1'' AS A,''2'' AS B,''3'' AS C)) ';

  STEP6SQL =
    'SELECT ima01,ima02,ima021,imaud02,imaud03,imaud05,ima25,ima907 FROM %sima_file where ima01 IN (%s)';

  // 无订单出货扣库存
  STEP7SQL =
    'SELECT ogb04,sum(ogb912) sl,sum(ogb915) zl FROM %soga_file,%0:sogb_file WHERE oga01 = ogb01 AND regexp_like(ogb092,''-%1:s$'') '
    + #13#10 +
    ' AND ogapost = ''Y'' AND ogaconf = ''Y'' AND %2:s GROUP BY ogb04';
  // 杂收发引起的库存异动
  STEP8SQL = 'SELECT inb04,a_csl,a_czl,b_csl,b_czl FROM ( ' + #13#10 +
    ' SELECT ina00,inb04,SUM(inb904) sl,SUM(inb924) zl FROM %sinb_file,%0:sina_file WHERE ina01 = inb01 AND ina00 IN (''1'',''3'') AND '
    + #13#10 +
    ' inapost = ''Y'' AND inaconf = ''Y'' AND regexp_like(inb07,''-%1:s$'') AND %2:s '
    + #13#10 + ' GROUP BY ina00,inb04)' + #13#10 +
    ' PIVOT (SUM(sl) csl,SUM(zl) czl FOR ina00 IN (''1'' AS A,''3'' AS B))';

  STEP10SQL = 'SELECT tlf01,Nvl(s_csl,0) s_csl,NVL(b_csl,0) b_csl FROM ' +
    #13#10 + '(SELECT  tlf01,SUM(tlf10*tlf907) sl,1 AS tp  FROM %stlf_file WHERE tlf01 IN (%s) AND regexp_like(tlf904,''-%s$'') AND %s '
    + #13#10 + 'GROUP BY tlf01   ' + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT  tlf01,SUM(tlf10*tlf907) sl,2 AS tp  FROM %0:stlf_file WHERE tlf01 IN (%1:s) AND regexp_like(tlf904,''-%2:s$'') AND %4:s '
    + #13#10 + 'GROUP BY tlf01   ' + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imk01,sum(imk09) sl, 1 AS tp FROM %0:simk_file WHERE imk01 IN (%1:s) AND regexp_like(imk04,''%2:s$'') AND imk05 = %5:d AND imk06 = %6:d GROUP BY imk01 '
    + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imk01,sum(imk09) sl, 2 AS tp FROM %0:simk_file WHERE imk01 IN (%1:s) AND regexp_like(imk04,''%2:s$'') AND imk05 = %7:d AND imk06 = %8:d GROUP BY imk01 '
    + #13#10 + ')   ' + #13#10 + 'PIVOT  ' + #13#10 +
    '(SUM(sl) csl FOR tp IN (1 AS S,2 AS B))';

  STEP20SQL = 'SELECT tlff01,Nvl(s_csl,0) s_csl,NVL(b_csl,0) b_csl FROM ' +
    #13#10 + '(SELECT  tlff01,SUM(tlff10*tlff907) sl,1 AS tp  FROM %stlff_file WHERE tlff01 IN (%s) AND regexp_like(tlff904,''-%s$'') AND %s '
    + #13#10 + 'GROUP BY tlff01   ' + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT  tlff01,SUM(tlff10*tlff907) sl,2 AS tp  FROM %0:stlff_file WHERE tlff01 IN (%1:s) AND regexp_like(tlff904,''-%2:s$'') AND %4:s '
    + #13#10 + 'GROUP BY tlff01   ' + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imkk01,sum(imkk09) sl, 1 AS tp FROM %0:simkk_file WHERE imkk01 IN (%1:s) AND regexp_like(imkk04,''%2:s$'') AND imkk05 = %5:d AND imkk06 = %6:d GROUP BY imkk01 '
    + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imkk01,sum(imkk09) sl, 2 AS tp FROM %0:simkk_file WHERE imkk01 IN (%1:s) AND regexp_like(imkk04,''%2:s$'') AND imkk05 = %7:d AND imkk06 = %8:d GROUP BY imkk01 '
    + #13#10 + ')   ' + #13#10 + 'PIVOT  ' + #13#10 +
    '(SUM(sl) csl FOR tp IN (1 AS S,2 AS B))';

  STEP22SQL =    //现场退货量
  'SELECT imn03,SUM(imn32) imn32,SUM(imn35) imn35 FROM %simn_file,%0:simm_file WHERE imm01 = imn01 AND  regexp_like(imm01,''^DB99'') '+
  'AND regexp_like(imn06,''-%1:s$'') AND imm03 = ''Y'' AND  %s ' + sLineBreak +
  'GROUP BY imn03';

var
  xmNF, zqTimeF, qTimeF, bTimeF, chTimeF, ZxTimeF: string; // 项目,时间的过滤条件
  qyear, qmonth, byear, bmonth: Integer;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.XmCount(const zh: string): Integer;
// 返回账号所负责的项目数目
var
  i: Integer;
  SQL: string;
begin
  if zh = '' then
    exit(0);
  // 查询返回项目数
  if CurrentZT = '' then
    CurrentZT := DefaultZT;

  SQL := Format(QueryXmSQL, [CurrentZT + '.', zh]);
  if Not RunCDS(SQL) then
    exit(0);

  if Cds.RecordCount = 0 then
    exit(0);

  dba.ReadDataset(SQL, Cds);
  if Cds.RecordCount = 0 then
    exit(0);

  xmcb.FixedCols := 0;
  xmcb.ColWidths[0] := 20;
  xmcb.Options := xmcb.Options + [goRowSelect, goEditing];
  xmcb.ShowSelection := false;

  // 在GRID中显示项目
  xmcb.RowCount := Cds.RecordCount + 1;
  Cds.First;
  i := 1;
  while not Cds.Eof do
  begin
    xmcb.AddCheckBox(0, i, false, false);
    xmcb.Cells[1, i] := Cds.Fields.Fields[0].AsString;
    xmcb.Cells[2, i] := Cds.Fields.Fields[1].AsString;
    inc(i);
    Cds.Next;
  end;
  Result := Cds.RecordCount;
end;

procedure TWorkForm.xmokClick(Sender: TObject);
// 确定返回的选中项目号写入项目编号中。若全不选中，则弹出出错框。若全选，则筛选条件不包括项目编号条件
var
  i: Integer;
  State: Boolean;
  Value: TstringList;
begin
  Value := TstringList.Create;

  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, i, State);
    if State then
      Value.Append(xmcb.Cells[1, i]);
  end;
  if Value.Count = 0 then
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
    begin
      Value.Free;
      exit;
    end;

  xmLE.Text := Value.DelimitedText; // 返回项目统计选择项
  Value.Free;
  XmcbClose;
end;

procedure TWorkForm.xmsaClick(Sender: TObject);
var
  i: Integer;
begin
  // 设置所有项目编号全选中
  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, i, True);
    xmcb.RowColor[i] := xmcb.SelectionColor;
  end;
end;

procedure TWorkForm.ztxzClick(Sender: TObject);
begin
  // 切换当前选择项，获取当前账套
  case ztxz.ItemIndex of
    1:
      CurrentZT := 'XCXD' // 现场测试账套
  else
    CurrentZT := DefaultZT;
  end;
  // 获取新的项目编号
  // 获取新的项目编号
  xmF.Visible := false;
  TimeP.Visible := false;
  xmcb.ClearAll;
  MTV.DataController.RecordCount := 0;

  // 界面初始化
  xmLE.Text := '';

  if XmCount(zhanghu) = 0 then
    application.MessageBox(Pchar('当前账套下没有账户：' + zhanghu + ' 所负责的项目!'), '提示');

  // 界面初始化
  xmLE.Text := '';
end;

procedure TWorkForm.XmcbOpen;
// 打开项目编号选择
begin
  MasterP.Enabled := false;
  xmF.Visible := True;
  ResizeForm;
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
var
  y, m, d: word;
  SQL: string;
begin
  // 期别的影响
  if Not TimeP.Visible then
    exit;

  SQL := 'SELECT  NVL(ta_gem33,1) dd FROM ' + CurrentZT +
    '.gem_file WHERE gem01 = ''' + trim(xmLE.Text) + '''';
  if Not RunCDS(SQL) then
    exit;
  Cds.First;
  if Cds.RecordCount > 1 then
    exit; // 查询的结果大于1条,不可能,直接退出
  y := strtoint(qbYear.Text);
  m := qbmon.ItemIndex + 1;
  d := Cds.FieldByName('dd').AsInteger;
  if d = 1 then
  begin
    beginT.DateTime := EncodeDate(y, m, d);
    endT.DateTime := IncMonth(EnCodeDate(y, m, d),1) - 1;
  end
  else
  begin
    beginT.DateTime := incMonth(EncodeDate(y, m, d), -1) + 1;
    endT.DateTime := EncodeDate(y, m, d);
  end;
end;

procedure TWorkForm.XmcbClose;
// 关闭项目编号选择
begin
  xmF.Visible := false;
  MasterP.Enabled := True;
  // 当选中项为1时显示期间选择
  TimeP.Visible := XmcbSelectCount = 1;
  if TimeP.Visible then
    SetTimeRange;
  ResizeForm;
end;

function TWorkForm.XmcbSelectCount: Integer;
// 返回项目编号选择数目
var
  i: Integer;
  State: Boolean;
begin
  Result := 0;
  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, i, State);
    if State then
      inc(Result);
  end;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin
  // 返回项目编号选择,选择前后MasterP大小发生变化不知为何，重新定义其大小
  XmcbOpen;
end;

procedure TWorkForm.xmcaClick(Sender: TObject);
begin
  // 直接退出，设置xmf不可见。保持原筛选条件
  if XmcbSelectCount = 0 then
  begin
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
      exit;
  end;
  XmcbClose;
end;

procedure TWorkForm.xmclClick(Sender: TObject);
var
  i: Integer;
begin
  // 设置所有项目编号全不选中
  for i := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, i, false);
    xmcb.RowColor[i] := xmcb.Color;
  end;
end;

function TWorkForm.GetFilterS: string;
// 返回动态筛选条件,分为四个部分  项目编号(多于1个取第一个)/周期时间/上月结存时间/本月结存时间
// 若项目编号选择为空，不再查询
var
  bt, et: Tdate;
begin
  Result := '';

  if xmLE.Text = '' then
  begin
    exit('项目编号不能为空');
  end
  else if pos(DChar, xmLE.Text) < 1 then
    xmNF := xmLE.Text
  else
    xmNF := copy(xmLE.Text, 1, pos(DChar, xmLE.Text) - 1);;

  // 获取月结使用的年份与期别
  bt := incMonth(beginT.Date, -1);
  qyear := YearOf(bt);
  qmonth := MonthOf(bt);
  bt := incMonth(endT.Date, -1);
  byear := YearOf(bt);
  bmonth := MonthOf(bt);

  bt := beginT.Date;
  et := endT.Date;

  if et < bt then
  begin
    exit('Error : 结束日期不可小于开始时间！');
  end;

  zqTimeF := ' imm17 BETWEEN to_date(''' + FormatDateTime('yyyymmdd', bt) +
    ''',''yyyymmdd'') AND ' + ' to_date(''' + FormatDateTime('yyyymmdd', et) +
    ''',''yyyymmdd'')';
  chTimeF := stringReplace(zqTimeF, 'imm17', 'oga02', [rfReplaceAll]);
  ZxTimeF := stringReplace(zqTimeF, 'imm17', 'ina02', [rfReplaceAll]);

  qTimeF := ' tlf06 BETWEEN to_date(''' + FormatDateTime('yyyymmdd',
    StartOfTheMonth(bt)) + ''',''yyyymmdd'') AND ' + ' to_date(''' +
    FormatDateTime('yyyymmdd', incDay(bt, -1)) + ''',''yyyymmdd'')';

  bTimeF := ' tlf06 BETWEEN to_date(''' + FormatDateTime('yyyymmdd',
    StartOfTheMonth(et)) + ''',''yyyymmdd'') AND ' + ' to_date(''' +
    FormatDateTime('yyyymmdd', et) + ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  LJBH: string;
  SQL: string;
  E: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;
  // 主查询
  screen.Cursor := crHourGlass;

  E := GetFilterS;
  if E <> '' then
  begin
    screen.Cursor := crDefault;
    application.MessageBox(Pchar(E), '提示');
    exit;
  end;
  // 进耗存要多次从数据库取数,所以,采用分段填充GRID的方式
  MTV.DataController.RecordCount := 0;
  MTV.BeginUpdate();
  try
    // step 1 :料件查询
    SQL := Format(STEP1SQL, [CurrentZT + '.', xmNF, stringReplace(zqTimeF,
      'imm17', 'tlf06', [rfReplaceAll]), qTimeF, qyear, qmonth]);
    RunCDS(SQL);
    if Cds.RecordCount = 0 then
    begin
      application.MessageBox('没有查询到相关记录，请修改查询条件后继续！', '提示');
      screen.Cursor := crDefault;
      exit;
    end
    else
      FillmGrid(Cds);
    // 获取料件集
    LJBH := '';
    Cds.First;
    while Not Cds.Eof do
    begin
      LJBH := LJBH + ',''' + Cds.FieldByName('tlf01').AsString + '''';
      Cds.Next;
    end;
    LJBH := copy(LJBH, 2, length(LJBH));

    // step 3 :收货信息
    SQL := Format(STEP3SQL, [CurrentZT + '.', xmNF, stringReplace(zqTimeF,
      'imm17', 'imm12', [rfReplaceAll])]);
    // memo1.Lines.Append(CDS.SQL.Text);
    RunCDS(SQL);
    FillmGrid(Cds, 3);

    // step 4 :调拨信息
    SQL := Format(STEP4SQL, [xmNF, CurrentZT + '.', zqTimeF]);
    // memo1.Lines.Append(CDS.SQL.Text);
//                  memo1.Lines.Text := sql;
//    exit;
    RunCDS(SQL);
    FillmGrid(Cds, 4);

    // step 5 :领料信息
    SQL := Format(STEP5SQL, [CurrentZT + '.', xmNF, zqTimeF]);
//     memo1.Lines.Text := sql;Exit;
    RunCDS(SQL);
    FillmGrid(Cds, 5);

    // step 2 :跨项目领料信息
    SQL := Format(STEP2SQL, [xmNF, CurrentZT + '.', zqTimeF]);
    // memo1.Lines.Append(CDS.SQL.Text);
    RunCDS(SQL);
    FillmGrid(Cds, 2);

    // step 6 :料件信息导入
    SQL := Format(STEP6SQL, [CurrentZT + '.', LJBH]);
    // memo1.Lines.Append(CDS.SQL.Text);
    RunCDS(SQL);
    FillmGrid(Cds, 6);
    // step 7 :650出货单导入
    SQL := Format(STEP7SQL, [CurrentZT + '.', xmNF, chTimeF]);
    RunCDS(SQL);
    FillmGrid(Cds, 7);

    // step 8 :杂收发导入
    SQL := Format(STEP8SQL, [CurrentZT + '.', xmNF, ZxTimeF]);
    RunCDS(SQL);
    FillmGrid(Cds, 8);

    // step 10 :结存数量导入
    SQL := Format(STEP10SQL, [CurrentZT + '.', LJBH, xmNF, qTimeF, bTimeF,
      qyear, qmonth, byear, bmonth]);
    // memo1.lines.append(CDS.sql.text);
    RunCDS(SQL);
    FillmGrid(Cds, 10);

    // step 20 :结存重量导入
    SQL := Format(STEP20SQL, [CurrentZT + '.', LJBH, xmNF, stringReplace(qTimeF,
      'tlf06', 'tlff06', [rfReplaceAll]), stringReplace(bTimeF, 'tlf06',
      'tlff06', [rfReplaceAll]), qyear, qmonth, byear, bmonth]);
    // memo1.lines.append(CDS.sql.text);
    RunCDS(SQL);
    FillmGrid(Cds, 20);

//step22 :现场退货数量导入
    // step 5 :领料信息
    SQL := Format(STEP22SQL, [CurrentZT + '.', xmNF, zqTimeF]);
//     memo1.Lines.Text := sql;
//     Exit;
    RunCDS(SQL);
    FillmGrid(Cds, 22);

  finally
    MTV.EndUpdate;
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
// 将领料汇总值报表导入excel
const
  XMNO = 14; // 项目编号所在的列数
var
  str: string;
  i, j, m, y: Integer;
  excelapp, sheet: Variant;
  colName, Bcaption, XMRQ: string; // 项目编号 / 列标题 / 尾部标识行内容 /筛选时间
  FSQL, SQL: string;
  procedure pexit;
  // 本过程退出，恢复鼠标类型
  begin
    screen.Cursor := crDefault;
    exit;
  end;

begin
  screen.Cursor := crAppStart;

  // 1.取期别为beginT的期别,获取项目包结存信息
  m := MonthOf(beginT.Date);
  y := YearOf(beginT.Date);

  // 若项目编号选择为空，则退出
  if xmLE.Text = '' then
    pexit;

  FSQL := Format(' tc_pjz10 = ''%s'' AND tc_pjz02 = %d AND tc_pjz03 = %d',
    [xmNF, y, m]);

  SQL := 'SELECT DISTINCT tc_pjz10,tc_pjz02,tc_pjz03,nvl(tc_pjz19,0) tc_pjz19,'
    + 'nvl(tc_pjz20,0) tc_pjz20,nvl(tc_pjz21,0) tc_pjz21,nvl(tc_pjz22,0) tc_pjz22 '
    + ' FROM ' + CurrentZT + '.tc_pjz_file WHERE ' + FSQL;
  RunCDS(SQL);

  str := '';

  // 时间范围
  XMRQ := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date)
    + char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date);

  colName := '';
  Bcaption := '';
  for i := 0 to MTV.ColumnCount - 1 do
  // 显示列标题
  begin
    colName := colName + MTV.Columns[i].Caption + char(9);
    Bcaption := Bcaption + '================' + char(9);
  end;

  // 显示内容
  str := '项目编号 ： ' + char(9) + xmNF + #13 + XMRQ;

  if Not Cds.Locate('tc_pjz10', xmNF, []) then
  begin
    application.MessageBox('没有找到当前项目的包号月度信息,无法导出', '提示');
    pexit;
  end;
  str := str + #13 + '上月结存包个数: ' + char(9) + Cds.FieldByName('tc_pjz19')
    .AsString + char(9) + '本月做包个数: ' + char(9) + Cds.FieldByName('tc_pjz20')
    .AsString + char(9) + '本月下线包个数: ' + char(9) + Cds.FieldByName('tc_pjz21')
    .AsString + char(9) + '本月结存包个数: ' + char(9) + Cds.FieldByName('tc_pjz22')
    .AsString + char(9) + #13 + #13 + colName + #13;
  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    for j := 0 to MTV.ColumnCount - 1 do
    begin
      if MTV.DataController.Values[i, j] <> NULL then
        str := str + MTV.DataController.Values[i, j] + char(9)
      else
        str := str + '' + char(9);
    end;
    str := str + #13;
    application.ProcessMessages;
  end;

  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := True;
    except
      application.MessageBox('Excel未安装', '提示');
    end;
  finally
    clipboard.Clear;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
      ExportGridToExcel(SaveDialog.FileName, MGrid, True, True, True, 'xls');
      showmessage('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet; tp: Integer = 1);
var
  i: Integer;
  s: string;
begin
  if Q.RecordCount = 0 then
    exit;
  case tp of
    1: // 添加料件编号
      begin
        Q.First;
        while not Q.Eof do
        begin
          i := MTV.DataController.AppendRecord;
          MTV.DataController.Values[i, 0] := Q.Fields.Fields[0].AsString;
          Q.Next;
        end;
      end;
    2: // 跨项目领用
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('imn03', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 22] := Q.FieldByName('ljs').AsString;
          MTV.DataController.Values[i, 23] := Q.FieldByName('ljz').AsString;
          MTV.DataController.Values[i, 24] := Q.FieldByName('lcs').AsString;
          MTV.DataController.Values[i, 25] := Q.FieldByName('lcz').AsString;
        end;
      end;
    10: // 计算上月与本月结存数量
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Q.Locate('tlf01', s, []) then // 定位Q
          begin
            MTV.DataController.Values[i, 8] := Q.FieldByName('s_csl').AsString;
            MTV.DataController.Values[i, 26] := Q.FieldByName('b_csl').AsString;
          end
          else
          begin
            MTV.DataController.Values[i, 8] := '0';
            MTV.DataController.Values[i, 26] := '0';
          end;
        end;
      end;
    20: // 计算上月与本月结存重
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Q.Locate('tlff01', s, []) then // 定位Q
          begin
            MTV.DataController.Values[i, 9] := Q.FieldByName('s_csl').AsString;
            MTV.DataController.Values[i, 27] := Q.FieldByName('b_csl').AsString;
          end
          else
          begin
            MTV.DataController.Values[i, 9] := '0';
            MTV.DataController.Values[i, 27] := '0';
          end;
        end;
      end;
    3: // 本月收货
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('imn03', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 10] := Q.FieldByName('shs').AsString;
          MTV.DataController.Values[i, 11] := Q.FieldByName('shz').AsString;
        end;
      end;
    4: // 本月调拨出/入
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('imn03', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 12] := Q.FieldByName('bjs').AsString;
          MTV.DataController.Values[i, 13] := Q.FieldByName('bjz').AsString;
          MTV.DataController.Values[i, 20] := Q.FieldByName('bcs').AsString;
          MTV.DataController.Values[i, 21] := Q.FieldByName('bcz').AsString;
        end;
      end;
    5: // 本月领料
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('imn03', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 14] := Q.FieldByName('xqs').AsString;
          MTV.DataController.Values[i, 15] := Q.FieldByName('xqz').AsString;
          MTV.DataController.Values[i, 16] := Q.FieldByName('wxs').AsString;
          MTV.DataController.Values[i, 17] := Q.FieldByName('wxz').AsString;
          MTV.DataController.Values[i, 18] := Q.FieldByName('fts').AsString;
          MTV.DataController.Values[i, 19] := Q.FieldByName('ftz').AsString;
        end;
      end;
    6: // 料件基本信息
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('ima01', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 1] := Q.FieldByName('ima02').AsString;
          MTV.DataController.Values[i, 2] := Q.FieldByName('ima021').AsString;
          MTV.DataController.Values[i, 3] := Q.FieldByName('imaud02').AsString;
          MTV.DataController.Values[i, 4] := Q.FieldByName('imaud03').AsString;
          MTV.DataController.Values[i, 5] := Q.FieldByName('imaud05').AsString;
          MTV.DataController.Values[i, 6] := Q.FieldByName('ima25').AsString;
          MTV.DataController.Values[i, 7] := Q.FieldByName('ima907').AsString;
        end;
      end;
    7: // 单结出货扣库存
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('ogb04', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 28] := Q.FieldByName('sl').AsString;
          MTV.DataController.Values[i, 29] := Q.FieldByName('zl').AsString;
        end;
      end;
    8: // 杂项变更库存
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('inb04', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 30] := Q.FieldByName('A_CSL').AsString;
          MTV.DataController.Values[i, 31] := Q.FieldByName('A_CZL').AsString;
          MTV.DataController.Values[i, 32] := Q.FieldByName('B_CSL').AsString;
          MTV.DataController.Values[i, 33] := Q.FieldByName('B_CZL').AsString;
        end;
      end;
    22: // 退货信息
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('imn03', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 34] := Q.FieldByName('imn32').AsString;
          MTV.DataController.Values[i, 35] := Q.FieldByName('imn35').AsString;
        end;
      end;
  end;
end;

procedure TWorkForm.MTVCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
// 当期初数量加上差异量不等于期末数量时，行颜色变为clMenu
const
  VCont = 14;
  tt: array [1 .. VCont] of Integer = (8, 10, 12, 14, 16, 18, 20, 22, 24, 26,
    28, 30, 32,34);
var
  key: array [1 .. VCont] of double;
  l: double;
  t: Integer;
  v: Variant;
begin
  for t := 1 to VCont do
    key[t] := 0;
  for t := 1 to VCont do
  begin
    v := AViewInfo.RecordViewInfo.GridRecord.Values[tt[t]];
    if (v = NULL) or (v = '') then
      l := 0
    else
      l := roundTo(StrToFloat(v), -3);
    key[t] := l;
  end;
  l := roundTo(key[1] + key[2] + key[3] - key[4] - key[5] - key[6] - key[7] +
    key[8] - key[9] - key[10] - key[11] - key[12] + key[13] - key[14], -3);
  if l <> 0 then
    ACanvas.Brush.Color := clred;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息  同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      zhanghu := InParcel.GetStringGoods('LoginUser');
      XmCount(zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
