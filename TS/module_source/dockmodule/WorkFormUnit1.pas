﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
//                     Version 2.00
//                     Author: Jopher(W.G.Z)
//                     QQ: 779545524
//                     Email: Jopher@189.cn
//                     Homepage: http://www.quickburro.com/
//                     Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel;

type
  TWorkForm = class(TForm)
    DBGrid1: TDBGrid;
    Cds: TClientDataSet;
    DataSource1: TDataSource;
    dba: TDBAccessor;
    Bevel1: TBevel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

//
// 窗口显示时读取数据集...
procedure TWorkForm.FormShow(Sender: TObject);
var
   SQLCommand: string;
begin
//
// 初始化TDBAccessor对象...
   dba.UserConnection:=UserConn;
   dba.TargetNodeId:=UserConn.UserNodeId;
   dba.TargetDatabaseId:=InputParcel.GetAnsiStringGoods('DatabaseId');
//
// 读数据集...
   SQLCommand:=InputParcel.GetStringGoods('SqlCommand');
   if SQLCommand = '' then
     SQLCommand := 'select top 10 * from customer';
   if not dba.ReadDataset(SQLCommand,cds) then
      application.MessageBox('读数据集失败！','Infomation');
end;

end.
