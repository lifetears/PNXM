object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 332
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 686
    Height = 332
    Align = alClient
    Shape = bsFrame
    ExplicitWidth = 670
    ExplicitHeight = 333
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 686
    Height = 332
    Align = alClient
    DataSource = DataSource1
    ImeName = #20013#25991' ('#31616#20307') - '#35895#27468#25340#38899#36755#20837#27861
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #23435#20307
    TitleFont.Style = []
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 312
    Top = 136
  end
  object DataSource1: TDataSource
    DataSet = Cds
    Left = 248
    Top = 136
  end
  object dba: TDBAccessor
    TaskTimeout = 10
    EnableBCD = False
    IsolationLevel = ilCursorStability
    EnableLoadBalance = False
    Left = 408
    Top = 144
  end
end
