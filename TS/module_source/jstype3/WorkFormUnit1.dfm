﻿object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1029
      Height = 35
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 272
        Top = 11
        Width = 60
        Height = 12
        Caption = #26597#35810#26102#38388#65306
      end
      object xmbh: TLabeledEdit
        Left = 104
        Top = 8
        Width = 121
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #39033#30446#32534#21495#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        TabOrder = 0
        Text = '30004'
      end
      object DTP: TDateTimePicker
        Left = 352
        Top = 7
        Width = 121
        Height = 21
        Date = 43074.489328449080000000
        Time = 43074.489328449080000000
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 1
        TabStop = False
      end
      object Button2: TButton
        Left = 832
        Top = 4
        Width = 175
        Height = 25
        Caption = #26597#35810#28809#27425#20449#24687
        TabOrder = 2
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 502
        Top = 4
        Width = 96
        Height = 25
        Caption = #27169#25311#35745#31639
        TabOrder = 3
        Visible = False
      end
      object btn1: TButton
        Left = 632
        Top = 4
        Width = 145
        Height = 25
        Caption = #26597#35810#39033#30446#21253#21495#39046' '#26009#27719#24635
        Enabled = False
        TabOrder = 4
        OnClick = btn1Click
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 71
      Width = 1029
      Height = 35
      Align = alTop
      TabOrder = 1
      object qmwyb: TLabeledEdit
        Left = 104
        Top = 7
        Width = 121
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #26399#26411#26410#29992#21253#25968#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 0
        Text = '0'
      end
      object qmyyb: TLabeledEdit
        Left = 352
        Top = 7
        Width = 121
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #26399#26411#24050#29992#21253#25968#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 1
        Text = '0'
      end
      object ymyylc: TLabeledEdit
        Left = 615
        Top = 6
        Width = 121
        Height = 20
        EditLabel.Width = 120
        EditLabel.Height = 12
        EditLabel.Caption = #26399#26411#24050#29992#21253#32047#35745#28809#27425#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 2
        Text = '0'
      end
      object lllc: TLabeledEdit
        Left = 832
        Top = 6
        Width = 73
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #26399#26411#29702#35770#28809#40836#65306
        LabelPosition = lpLeft
        TabOrder = 3
        Text = '1'
      end
      object Button4: TButton
        Left = 911
        Top = 4
        Width = 96
        Height = 25
        Caption = #26597#35810#39046#26009#26126#32454
        Enabled = False
        TabOrder = 4
        OnClick = Button4Click
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 36
      Width = 1029
      Height = 35
      Align = alTop
      TabOrder = 2
      object qcwys: TLabeledEdit
        Left = 104
        Top = 7
        Width = 121
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #26399#21021#26410#29992#21253#25968#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 0
        Text = '0'
      end
      object qcyys: TLabeledEdit
        Left = 352
        Top = 6
        Width = 121
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #26399#21021#24050#29992#21253#25968#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 1
        Text = '0'
      end
      object qcljlc: TLabeledEdit
        Left = 615
        Top = 7
        Width = 121
        Height = 20
        EditLabel.Width = 120
        EditLabel.Height = 12
        EditLabel.Caption = #26399#21021#24050#29992#21253#32047#35745#28809#27425#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 2
        Text = '0'
      end
      object qclc: TLabeledEdit
        Left = 834
        Top = 8
        Width = 71
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #26399#21021#29702#35770#28809#27425#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ReadOnly = True
        TabOrder = 3
        Text = '0'
      end
      object Button1: TButton
        Left = 911
        Top = 4
        Width = 96
        Height = 25
        Caption = #26597#35810#39046#26009
        Enabled = False
        TabOrder = 4
        OnClick = Button1Click
      end
    end
    object xsxx: TPageControl
      Left = 1
      Top = 106
      Width = 1029
      Height = 482
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = #39046#26009#20449#24687
        object llxx: TCRDBGrid
          Left = 0
          Top = 0
          Width = 1021
          Height = 454
          OptionsEx = [dgeEnableSort, dgeLocalFilter, dgeLocalSorting, dgeRecordCount, dgeSummary]
          Align = alClient
          Color = clInfoBk
          DataSource = UniDataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          PopupMenu = PopupMenu1
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
      end
      object ts1: TTabSheet
        Caption = #39033#30446#21253#21495#39046#26009#27719#24635
        ImageIndex = 2
        object pnl1: TPanel
          Left = 0
          Top = 0
          Width = 1021
          Height = 41
          Align = alTop
          BevelInner = bvSpace
          BevelOuter = bvNone
          TabOrder = 0
          object rg1: TRadioGroup
            Left = 1
            Top = 1
            Width = 1019
            Height = 39
            Align = alClient
            Caption = #21253#21495
            Columns = 5
            ItemIndex = 0
            Items.Strings = (
              #25152#26377#20449#24687
              #26399#21021#26410#29992#21253#20449#24687
              #26399#21021#24050#29992#21253#20449#24687
              #26399#26411#26410#29992#21253#20449#24687
              #26399#26411#24050#29992#21253#20449#24687)
            TabOrder = 0
            OnClick = rg1Click
          end
        end
        object bhll: TCRDBGrid
          Left = 0
          Top = 41
          Width = 1021
          Height = 413
          Align = alClient
          Color = clInfoBk
          DataSource = UniDataSource2
          PopupMenu = pm1
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #23435#20307
          TitleFont.Style = []
        end
      end
      object 项目包号信息: TTabSheet
        Caption = #39033#30446#21253#21495#20449#24687
        ImageIndex = 1
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1021
          Height = 41
          Align = alTop
          BevelInner = bvSpace
          BevelOuter = bvNone
          TabOrder = 0
          object RadioGroup1: TRadioGroup
            Left = 1
            Top = 1
            Width = 1019
            Height = 39
            Align = alClient
            Caption = #21253#21495
            Columns = 5
            ItemIndex = 0
            Items.Strings = (
              #25152#26377#20449#24687
              #26399#21021#26410#29992#21253#20449#24687
              #26399#21021#24050#29992#21253#20449#24687
              #26399#26411#26410#29992#21253#20449#24687
              #26399#26411#24050#29992#21253#20449#24687)
            TabOrder = 0
            OnClick = RadioGroup1Click
          end
        end
        object bhxx: TCRDBGrid
          Left = 0
          Top = 41
          Width = 1021
          Height = 413
          Align = alClient
          Color = clInfoBk
          DataSource = UniDataSource3
          PopupMenu = PopupMenu2
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #23435#20307
          TitleFont.Style = []
        end
      end
    end
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 272
    Top = 424
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 176
    Top = 398
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 312
    Top = 424
  end
  object PopupMenu2: TPopupMenu
    Left = 388
    Top = 337
    object EXCEL2: TMenuItem
      Caption = #23548#20986'EXCEL'
      OnClick = EXCEL2Click
    end
  end
  object pm1: TPopupMenu
    Left = 388
    Top = 233
    object EXCEL4: TMenuItem
      Caption = #23548#20986'EXCEL'
      OnClick = EXCEL4Click
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 388
    Top = 289
    object EXCEL1: TMenuItem
      Caption = #23548#20986'EXCEL'
      OnClick = EXCEL1Click
    end
  end
  object bhllq: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    Left = 256
    Top = 288
  end
  object llq: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    Left = 264
    Top = 232
  end
  object bhq: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    Left = 256
    Top = 328
  end
  object UniDataSource1: TUniDataSource
    DataSet = llq
    Left = 317
    Top = 234
  end
  object UniDataSource2: TUniDataSource
    DataSet = bhllq
    Left = 317
    Top = 282
  end
  object UniDataSource3: TUniDataSource
    DataSet = bhq
    Left = 317
    Top = 330
  end
end
