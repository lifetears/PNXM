﻿object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object flwpnl1: TFlowPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 76
    Align = alTop
    TabOrder = 0
    object pnl1: TPanel
      Left = 1
      Top = 1
      Width = 248
      Height = 41
      TabOrder = 0
      object rg1: TRadioGroup
        Left = 1
        Top = 1
        Width = 246
        Height = 39
        Align = alClient
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          #22810#26009#21495#25104#26412#21015#34920
          #21333#26009#21495#23637#33267#23614#38454)
        TabOrder = 0
      end
    end
    object pnl2: TPanel
      Left = 249
      Top = 1
      Width = 159
      Height = 41
      Caption = 'pnl2'
      TabOrder = 1
      object cbb3: TComboBox
        Left = 47
        Top = 11
        Width = 106
        Height = 20
        ItemIndex = 0
        TabOrder = 0
        Text = 'GWGS'
        Items.Strings = (
          'GWGS'
          'GNGS'
          'YKGS'
          'SHBG'
          'MASYS')
      end
      object txt1: TStaticText
        Left = 6
        Top = 13
        Width = 40
        Height = 16
        Caption = #36134#22871#65306
        TabOrder = 1
      end
    end
    object pnl6: TPanel
      Left = 408
      Top = 1
      Width = 185
      Height = 41
      Caption = 'pnl6'
      TabOrder = 2
      object Q_year: TLabeledEdit
        Left = 64
        Top = 11
        Width = 81
        Height = 20
        EditLabel.Width = 36
        EditLabel.Height = 12
        EditLabel.Caption = #24180#20221#65306
        LabelPosition = lpLeft
        TabOrder = 0
      end
    end
    object pnl8: TPanel
      Left = 593
      Top = 1
      Width = 185
      Height = 41
      Caption = 'pnl8'
      TabOrder = 3
      object Q_dept: TLabeledEdit
        Left = 68
        Top = 11
        Width = 97
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #37096#38376#32534#21495#65306
        LabelPosition = lpLeft
        TabOrder = 0
      end
    end
    object pnl4: TPanel
      Left = 1
      Top = 42
      Width = 253
      Height = 41
      TabOrder = 4
      object Q_ima01: TLabeledEdit
        Left = 112
        Top = 8
        Width = 121
        Height = 20
        EditLabel.Width = 102
        EditLabel.Height = 12
        EditLabel.Caption = #26009#21495#65306'           '
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object cbb1: TComboBox
        Left = 41
        Top = 8
        Width = 65
        Height = 20
        ItemIndex = 0
        TabOrder = 1
        Text = #31561#20110
        Items.Strings = (
          #31561#20110
          #21253#21547
          #24320#22836#20026
          #32467#23614#20026)
      end
    end
    object pnl7: TPanel
      Left = 254
      Top = 42
      Width = 259
      Height = 41
      TabOrder = 5
      object Q_ima02: TLabeledEdit
        Left = 119
        Top = 8
        Width = 130
        Height = 20
        EditLabel.Width = 96
        EditLabel.Height = 12
        EditLabel.Caption = #21697#21517':           '
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object cbb2: TComboBox
        Left = 57
        Top = 8
        Width = 64
        Height = 20
        ItemIndex = 0
        TabOrder = 1
        Text = #31561#20110
        Items.Strings = (
          #31561#20110
          #21253#21547
          #24320#22836#20026
          #32467#23614#20026)
      end
    end
  end
  object pnl5: TPanel
    Left = 0
    Top = 76
    Width = 1031
    Height = 41
    Align = alTop
    Alignment = taLeftJustify
    Caption = #27880#24847#65306#24314#35758#37319#29992#26009#21495#31561#20110'/'#24320#22836#20026#26597#35810#65292#21487#25552#39640#25928#29575
    TabOrder = 1
    object btn1: TButton
      Left = 648
      Top = 9
      Width = 75
      Height = 25
      Caption = #26597#35810
      TabOrder = 0
      OnClick = btn1Click
    end
    object btn2: TButton
      Left = 753
      Top = 9
      Width = 75
      Height = 25
      Caption = #27719#20986'EXCEL'
      TabOrder = 1
      OnClick = btn2Click
    end
    object Button1: TButton
      Left = 848
      Top = 9
      Width = 75
      Height = 25
      Caption = #20840#37096#23637#24320
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 929
      Top = 9
      Width = 75
      Height = 25
      Caption = #20840#37096#25910#32553
      TabOrder = 3
      OnClick = Button2Click
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 117
    Width = 1031
    Height = 472
    ActivePage = ts1
    Align = alClient
    TabOrder = 2
    object ts1: TTabSheet
      Caption = 'ts1'
      ExplicitWidth = 995
      ExplicitHeight = 318
      object cxG: TcxGrid
        Left = 0
        Top = 0
        Width = 1023
        Height = 444
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 154
        ExplicitTop = 56
        ExplicitWidth = 670
        ExplicitHeight = 321
        object cGB: TcxGridTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Column = cGBC_A
            end
            item
              Kind = skSum
              Column = cGBC_B
            end
            item
              Kind = skSum
              Column = cGBC_C
            end
            item
              Kind = skSum
              Column = cGBC_D
            end
            item
              Kind = skSum
              Column = cGBC_F
            end
            item
              Kind = skSum
              Column = cGBC_G
            end
            item
              Kind = skSum
              Column = cGBC_H
            end
            item
              Kind = skSum
              Column = cGBC_I
            end
            item
              Kind = skSum
              Column = cGBC_J
            end
            item
              Kind = skSum
              Column = cGBC_K
            end
            item
              Kind = skSum
              Column = cGBC_TOT
            end>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          OptionsView.GroupSummaryLayout = gslAlignWithColumns
          Styles.ContentOdd = cxStyle2
          OnCustomDrawGroupSummaryCell = cGBCustomDrawGroupSummaryCell
          object cGBC_cua00: TcxGridColumn
            Caption = #24180#24230
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 45
          end
          object cGBC_cua01: TcxGridColumn
            Caption = 'ver'
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 30
          end
          object cGBC_cua02: TcxGridColumn
            Caption = #21697#21495
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC__ima02: TcxGridColumn
            Caption = #21697#21517
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 140
          end
          object cGBC_ima021: TcxGridColumn
            Caption = #35268#26684
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 140
          end
          object cGBC_cua16: TcxGridColumn
            Caption = #29305#24615#20195#30721
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
          end
          object cGBC_gem02: TcxGridColumn
            Caption = #36710#38388
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 70
          end
          object cGBC_cua14: TcxGridColumn
            Caption = #24037#33402#31449
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 40
          end
          object cGBC_eca02: TcxGridColumn
            Caption = #24037#33402#31449#21517#31216
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 90
          end
          object cGBC_A: TcxGridColumn
            Caption = #21407#26448#26009
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.Alignment.Vert = taVCenter
            FooterAlignmentHorz = taRightJustify
            GroupSummaryAlignment = taRightJustify
            HeaderAlignmentHorz = taCenter
            HeaderGlyphAlignmentHorz = taRightJustify
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_B: TcxGridColumn
            Caption = #36741#26009
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_C: TcxGridColumn
            Caption = #27169#20855
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_D: TcxGridColumn
            Caption = #38468#20214
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_E: TcxGridColumn
            Caption = #21253#35013
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_F: TcxGridColumn
            Caption = #30452#25509#20154#24037
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_G: TcxGridColumn
            Caption = #38388#25509#20154#24037
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_H: TcxGridColumn
            Caption = #30005#36153
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_I: TcxGridColumn
            Caption = #22825#28982#27668#36153
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_J: TcxGridColumn
            Caption = #20854#20182#36153#29992
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_K: TcxGridColumn
            Caption = #25240#26087
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
            Width = 80
          end
          object cGBC_TOT: TcxGridColumn
            Caption = #21512#35745
            DataBinding.ValueType = 'Float'
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
          end
          object cGBC_UID: TcxGridColumn
            Caption = 'UID'
            Visible = False
            GroupIndex = 0
            HeaderAlignmentHorz = taCenter
            Options.Editing = False
            Options.Filtering = False
            Options.Sorting = False
          end
        end
        object cxGLevel1: TcxGridLevel
          GridView = cGB
        end
      end
      object Memo1: TMemo
        Left = 632
        Top = 104
        Width = 230
        Height = 105
        Lines.Strings = (
          'Memo1')
        TabOrder = 1
        Visible = False
      end
    end
    object ts2: TTabSheet
      Caption = 'ts2'
      ImageIndex = 1
    end
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 80
    Top = 430
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 752
    Top = 344
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = 15784893
    end
    object DELS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsStrikeOut]
    end
    object AddS: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clRed
    end
    object ModS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
    end
    object DefaultS: TcxStyle
    end
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 160
    Top = 432
  end
  object TQ: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 480
  end
  object CDS: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    OnCalcFields = CdsCalcFields
    Left = 124
    Top = 429
    object strngfldds2TC_CUA00: TStringField
      DisplayWidth = 8
      FieldName = 'TC_CUA00'
      Required = True
      Size = 8
    end
    object strngfldds2TC_CUA01: TStringField
      DisplayWidth = 4
      FieldName = 'TC_CUA01'
      Required = True
      Size = 10
    end
    object strngfldds2TC_CUA02: TStringField
      DisplayWidth = 16
      FieldName = 'TC_CUA02'
      Required = True
      Size = 160
    end
    object strngfldds2IMA02: TStringField
      DisplayWidth = 40
      FieldName = 'IMA02'
      Size = 480
    end
    object strngfldds2IMA021: TStringField
      DisplayWidth = 40
      FieldName = 'IMA021'
      Size = 480
    end
    object strngfldds2TC_CUA16: TStringField
      DisplayWidth = 12
      FieldName = 'TC_CUA16'
      Required = True
      Size = 40
    end
    object strngfldds2GEM02: TStringField
      DisplayWidth = 16
      FieldName = 'GEM02'
      Size = 24
    end
    object strngfldds2TC_CUA14: TStringField
      DisplayWidth = 8
      FieldName = 'TC_CUA14'
      Size = 40
    end
    object strngfldds2ECA02: TStringField
      DisplayWidth = 16
      FieldName = 'ECA02'
      Size = 320
    end
    object CDS原材料: TFloatField
      DisplayWidth = 12
      FieldName = #21407#26448#26009
    end
    object CDS辅料: TFloatField
      DisplayWidth = 12
      FieldName = #36741#26009
    end
    object CDS模具: TFloatField
      DisplayWidth = 12
      FieldName = #27169#20855
    end
    object CDS附件: TFloatField
      DisplayWidth = 12
      FieldName = #38468#20214
    end
    object CDS包装: TFloatField
      DisplayWidth = 12
      FieldName = #21253#35013
    end
    object CDS直接人工: TFloatField
      DisplayWidth = 12
      FieldName = #30452#25509#20154#24037
    end
    object CDS间接人工: TFloatField
      DisplayWidth = 12
      FieldName = #38388#25509#20154#24037
    end
    object CDS电费: TFloatField
      DisplayWidth = 12
      FieldName = #30005#36153
    end
    object CDS天然气费: TFloatField
      DisplayWidth = 12
      FieldName = #22825#28982#27668#36153
    end
    object CDS其他费用: TFloatField
      DisplayWidth = 12
      FieldName = #20854#20182#36153#29992
    end
    object CDS折旧: TFloatField
      DisplayWidth = 12
      FieldName = #25240#26087
    end
    object CDS合同: TFloatField
      DisplayWidth = 12
      FieldKind = fkCalculated
      FieldName = #21512#35745
      Calculated = True
    end
    object strngfldds2UID: TStringField
      FieldKind = fkCalculated
      FieldName = 'UID'
      Size = 100
      Calculated = True
    end
  end
end
