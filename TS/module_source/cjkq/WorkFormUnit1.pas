﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,cxGridExportLink,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridCustomView, cxGrid, cxClasses, math;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    MGrid: TcxGrid;
    MTV: TcxGridBandedTableView;
    MGridLevel1: TcxGridLevel;
    Panel1: TPanel;
    Panel2: TPanel;
    ztRG: TRadioGroup;
    Label1: TLabel;
    btime: TDateTimePicker;
    ckbhcb: TComboBox;
    ckmccb: TComboBox;
    ljHead: TLabeledEdit;
    RunQuery: TButton;
    ToXLS: TButton;
    Panel3: TPanel;
    stHigh: TcxStyle;
    stLow: TcxStyle;
    Button1: TButton;
    MTVColumn1: TcxGridBandedColumn;
    MTVColumn2: TcxGridBandedColumn;
    MTVColumn3: TcxGridBandedColumn;
    MTVColumn4: TcxGridBandedColumn;
    MTVColumn5: TcxGridBandedColumn;
    MTVColumn6: TcxGridBandedColumn;
    MTVColumn7: TcxGridBandedColumn;
    MTVColumn8: TcxGridBandedColumn;
    MTVColumn9: TcxGridBandedColumn;
    MTVColumn10: TcxGridBandedColumn;
    MTVColumn11: TcxGridBandedColumn;
    MTVColumn12: TcxGridBandedColumn;
    MTVColumn13: TcxGridBandedColumn;
    MTVColumn14: TcxGridBandedColumn;
    MTVColumn15: TcxGridBandedColumn;
    MTVColumn16: TcxGridBandedColumn;
    MTVColumn17: TcxGridBandedColumn;
    MTVColumn18: TcxGridBandedColumn;
    MTVColumn19: TcxGridBandedColumn;
    MTVColumn20: TcxGridBandedColumn;
    MTVColumn21: TcxGridBandedColumn;
    MTVColumn22: TcxGridBandedColumn;
    MTVColumn23: TcxGridBandedColumn;
    MTVColumn24: TcxGridBandedColumn;
    MTVColumn25: TcxGridBandedColumn;
    MTVColumn26: TcxGridBandedColumn;
    MTVColumn27: TcxGridBandedColumn;
    MTVColumn28: TcxGridBandedColumn;
    MTVColumn29: TcxGridBandedColumn;
    MTVColumn30: TcxGridBandedColumn;
    MTVColumn31: TcxGridBandedColumn;
    MTVColumn32: TcxGridBandedColumn;
    MTVColumn33: TcxGridBandedColumn;
    MTVColumn34: TcxGridBandedColumn;
    MTVColumn35: TcxGridBandedColumn;
    MTVColumn36: TcxGridBandedColumn;
    MTVColumn37: TcxGridBandedColumn;
    Memo1: TMemo;
    stXiaoshu: TcxStyle;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure ckbhcbChange(Sender: TObject);
    procedure ztRGClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btimeChange(Sender: TObject);
    procedure MTVColumn7StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    procedure FillmGrid(Q: TClientDataSet);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure IniBMBH(zt: string);
    procedure IniZtRG(zt: string);
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure IniGrid(D: Tdate);
    procedure WaitMessage;
    procedure GetBM(userID: string);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CJKQ';
  WK: array [1 .. 7] of string = ('周日', '周一', '周二', '周三', '周四', '周五', '周六');
  BMSQL = 'SELECT GEN03 FROM %s.GEN_FILE WHERE GEN01 = ''%s''';
  HeadTitle = '%s %d年%d月出勤表';

var
  zhanghu: string;
  zt: string; // 当前账套
  softWareAuth: Boolean;
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  RunView: Boolean; // 记录是否可以跨部门查询考勤
  T, Z: array [1 .. 31] of Integer;
  CMonth: Integer; // 记录日期中的月份，为提高效率设定
  CmStartDay, CmLastDay: Tdate; // 记录日期的月份第一天与最后一天

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.IniBMBH(zt: string);
// 查询选定账套的部门信息
begin
  if zt = '' then
    exit;
    if Not RunCDS
      (format('SELECT wm_concat(GEM01) gem01,wm_concat(GEM02) gem02 FROM %s.gem_file WHERE GEM07 > ''G'' ',
      // AND LENGTH(GEM01) = 6',
      [zt])) then
      exit;

    ckbhcb.Items.DelimitedText := Cds.FieldByName('gem01').AsString;
    ckmccb.Items.DelimitedText := Cds.FieldByName('gem02').AsString;
    ckbhcb.ItemIndex := 0;
    ckmccb.ItemIndex := 0;
    GetBM(zhanghu);
end;

procedure TWorkForm.IniZtRG(zt: string);
// 根据权限设置可以选择的账套，并动态设置itemindex, columns值
begin
  ztRG.Items.DelimitedText := zt;
  ztRG.Columns := ztRG.Items.Count;
end;

procedure TWorkForm.GetBM(userID: string);
// 获取部门
var
  SQL, v: string;
  i : integer;
begin
  i := -1;
  if ztRG.ItemIndex = -1 then
    ztRG.ItemIndex := 0;
  SQL := format(BMSQL, [zt, userID]);
  // LogMsg(SQL);
  if dba.ReadSimpleResult(SQL, v) then
    i := ckbhcb.Items.IndexOf(v);
  if i > -1 then
  begin
    ckbhcb.ItemIndex := i;
    ckmccb.ItemIndex := i;
  end;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  zt := '';
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // caption记录插件的ID

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage;
  IniZtRG(zt);

  // 获取更改部门的权限
  GetPrivilege('ASSERT', '5', zhanghu);
  WaitMessage;
  if Cds.RecordCount = 0 then
    RunView := false
  else
    RunView := Not(Cds.FieldByName('R1').AsString = '0');

  ztRG.Enabled := RunView;
  ckbhcb.Enabled := RunView;

  ztRG.ItemIndex := 0;
  // 界面初始化
  // 默认为当天
  btime.Date := now;
  IniGrid(btime.Date);
end;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  ljF1, SQL: string;
begin
  if Not softWareAuth then
  begin
    application.MessageBox
      ('The application raise a Error ,the address is : 0X9107311FF', 'Error');
    exit;
  end;
  // 主查询
  screen.Cursor := crSQLWait;
  try
    ljF1 := 'SELECT tc_hre01,DAY(tc_hre01) d,tc_hre02,gen02,' +
      'tc_hre03,ecg02,nvl(tc_hre04,0) tc_hre04 ' + sLineBreak +
      ' FROM %0:s.TC_HRE_FILE ' + sLineBreak +
      'LEFT JOIN %0:s.gen_file ON tc_hre02 = gen01   ' + sLineBreak +
      'LEFT JOIN %0:s.ecg_file ON tc_hre03 = ecg01 AND ta_ecg01 = tc_hre06 ' +
      ' WHERE TC_HRE01 BETWEEN  to_date(''' + FormatDateTime('yyyymmdd',
      CmStartDay) + ''',''yyyymmdd'')  AND to_date(''' +
      FormatDateTime('yyyymmdd', CmLastDay) + ''',''yyyymmdd'') ' + sLineBreak +
      ' AND TC_HRE06 = ''%1:s'' AND TC_HRECONF = ''Y'' ' + sLineBreak +
      ' ORDER BY TC_HRE02,D,TC_HRE03';

    SQL := format(ljF1, [ztRG.Items[ztRG.ItemIndex], ckbhcb.Text]);
    // memo1.Lines.Text := sql;
    // exit;
    RunCDS(SQL);
    FillmGrid(Cds);
  finally
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  strFileName: string;
begin
  OpenDlg := TOpenDialog.Create(nil);
  try
    OpenDlg.Filter := 'EXCLE(*.xlsx)|*.xlsx';
    OpenDlg.DefaultExt := '*.xlsx';
    if OpenDlg.Execute then
    begin
      strFileName := Trim(OpenDlg.FileName);
      if strFileName <> '' then
      begin
            ExportGridToXLSX(strFileName, MGrid, True, True);
        application.MessageBox(pchar('转换完成'), '提示');
      end;
    end;
  finally
    FreeAndNil(OpenDlg);
  end;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
begin
  //
end;

procedure TWorkForm.IniGrid(D: Tdate);
// 初始化grid
var
   endD, w: Integer;
  i: Integer;
begin
  if CMonth = Monthof(D) then
    exit;
  endD := DaysInMonth(D);
  w := DayOfWeek(D);
  for i := 1 to 31 do
  begin
    if i > endD then
      T[i] := 0
    else
      T[i] := 1;
    if w > 7 then
      w := 1;
    Z[i] := w;
    inc(w);
  end;
  MTV.BeginUpdate();
  for i := 3 to 33 do
  begin
    MTV.Bands[i].Visible := T[i - 2] = 1;
    MTV.Columns[i + 3].Visible := MTV.Bands[i].Visible;
    MTV.Columns[i + 3].Caption := WK[Z[i - 2]];
  end;
  MTV.EndUpdate;
  CMonth := Monthof(D);
  CmStartDay := encodeDate(Yearof(D), Monthof(D), 1); // 本月第一天
  CmLastDay := encodeDate(Yearof(D), Monthof(D), endD); // 本月的最后一天
  MTV.Bands[0].Caption := Format(HeadTitle,[ckmccb.Text,Yearof(D),Monthof(D)]);
  ResizeForm;
end;

procedure TWorkForm.btimeChange(Sender: TObject);
begin
  IniGrid(btime.Date);
  ResizeForm;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
begin
  application.MessageBox(Pchar(self.Caption), 'test');
end;

procedure TWorkForm.ckbhcbChange(Sender: TObject);
begin
  ckmccb.ItemIndex := ckbhcb.ItemIndex;
  MTV.Bands[0].Caption := Format(HeadTitle,[ckmccb.Text,Yearof(bTime.Date),Monthof(bTime.Date)]);
end;

procedure TWorkForm.ztRGClick(Sender: TObject);
begin
  if ztRG.ItemIndex = -1 then
    exit;

  zt := ztRG.Items[ztRG.ItemIndex];
  IniBMBH(zt);
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  i: Integer;
  nameNo: string;
begin
  MTV.DataController.RecordCount := 0;
  if Q.RecordCount = 0 then
    exit;
  Q.First;
  nameNo := '';
  MTV.BeginUpdate();
  while not Q.Eof do
  begin
    if nameNo <> Q.FieldByName('tc_hre02').Text then
    begin
      i := MTV.DataController.AppendRecord;
      MTV.DataController.Values[i, 1] := Q.FieldByName('tc_hre03').Text;
      MTV.DataController.Values[i, 2] := Q.FieldByName('ecg02').Text;
      MTV.DataController.Values[i, 3] := Q.FieldByName('tc_hre02').Text;
      MTV.DataController.Values[i, 4] := Q.FieldByName('gen02').Text;
    end;
    if MTV.DataController.Values[i, 5] = null then
     begin
     MTV.DataController.Values[i, 5] := FormatFloat('##.##', 0 + StrToFloatDef(Q.FieldByName('tc_hre04').AsString,0))
     end else
    MTV.DataController.Values[i, 5] := FormatFloat('##.##',StrTofloatDef(MTV.DataController.Values[i, 5],0) + StrToFloatDef(Q.FieldByName('tc_hre04').AsString,0));
    MTV.DataController.Values[i, Q.FieldByName('D').AsInteger + 5] :=
      Q.FieldByName('tc_hre04').AsString;
    nameNo := Q.FieldByName('tc_hre02').Text;
    Q.Next;
  end;
  MTV.EndUpdate;
end;

procedure TWorkForm.MTVColumn7StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  v: Variant;
begin
   v := ARecord.Values[aItem.Index];
   if (v = NULL) or (v = '0') then
   begin
   AStyle := stLow;
   exit;
   end;
   if v > 1 then
   begin
   AStyle := stHigh;
   exit;
   end;
   if (v > 0) and (v < 1) then
   begin
     Astyle := stXiaoshu;
     exit;
   end;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  LogMsg(DLL_KEY + ':发送权限' + TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      // 获取插件权限信息
      GetPrivilege('MENU', self.Caption, zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', Cds);
      if zt = '' then // 若ZT为空，表示formshow时运行，此时赋ZT值，后期不再赋值
        zt := Cds.FieldByName('ZT').AsString;
    end;
    exit;
  end;
end;
{$ENDREGION}

end.
