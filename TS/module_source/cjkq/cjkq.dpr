﻿//
// ClientModuleSample.dpr -- 客户端模块（含窗体）模板工程文件（本单元无需应用程序员改动）
//                           Version 2.00
//                           Author: Jopher(W.G.Z)
//                           QQ: 779545524
//                           Email: Jopher@189.cn
//                           Homepage: http://www.quickburro.com/
//                           Copyright(C) Jopher Software Studio
//
library cjkq;

uses
   {$IF CompilerVersion>=23.0}Winapi.Windows{$ELSE}Windows{$IFEND},
   {$IF CompilerVersion>=23.0}System.SysUtils{$ELSE}SysUtils{$IFEND},
   {$IF CompilerVersion>=23.0}System.Classes{$ELSE}Classes{$IFEND},
   {$IFDEF UNICODE}
   {$IF CompilerVersion>=23.0}System.AnsiStrings{$ELSE}AnsiStrings{$IFEND},
   {$ENDIF}
   {$IF CompilerVersion>=23.0}Vcl.Forms{$ELSE}Forms{$IFEND},
   {$IF CompilerVersion>=23.0}Vcl.ExtCtrls{$ELSE}ExtCtrls{$IFEND},
   UserConnection,
   QBParcel,
   DllSpread,
   TaskCommon,
   WorkFormUnit1 in 'WorkFormUnit1.pas' {WorkForm};

//
// 窗体实例数组元素记录...
type
   TWorkFormRecord=Record
      FormHandle: integer;
      Form: TWorkForm;
   end;

//
// 全局变量，以管理多实例...
var
   OldApp: TApplication;
//   OldScreen: TScreen;
   UserConn: TUserConnection;
   //
   HandleCount: integer;
   WorkFormCs: TRTLCriticalSection;
   WorkFormCount: integer;
   WorkForms: array of TWorkFormRecord;

{$R *.res}

//
// 初始化DLL模块...
function DllFormInit(AppPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF};
   ScreenPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF};
   ConnPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF}): boolean; stdcall;
begin
   Application:=TApplication(Pointer(AppPtr));
//   Screen:=TScreen(Pointer(ScreenPtr));
   UserConn:=TUserConnection(pointer(ConnPtr));
   result:=true;
end;

//
// 创建工作窗体...
function DllFormCreate(): Integer; stdcall;
var
   j: integer;
begin
   EnterCriticalSection(WorkFormCs);
   try
      InitSocketPool;
      j:=WorkFormCount;
      inc(WorkFormCount);
      setlength(WorkForms,WorkFormCount);
      WorkForms[j].FormHandle:=HandleCount;
      WorkForms[j].form:=nil;
      inc(HandleCount);
      try
        WorkForms[j].Form:=TWorkForm.Create(nil);
        WorkForms[j].Form.Visible:=false;
        WorkForms[j].Form.UserConn:=UserConn;
        WorkForms[j].Form.InputParcel:=nil;
        WorkForms[j].Form.OutputParcel:=nil;
        result:=WorkForms[j].FormHandle;
      except
        result:=-1;
        if WorkForms[j].form<>nil then
           FreeAndNil(WorkForms[j].form);
        dec(WorkFormCount);
        setlength(WorkForms,WorkFormCount);
      end;
   finally
      LeaveCriticalSection(WorkFormCs);
   end;
end;

//
// 模态窗形式显示工作窗...
function DllFormShowModal(FormHandle: integer;
   InputParcelPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF};
   var OutputParcelPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF}): boolean; stdcall;
var
   ParentForm: TForm;
   FormCaption: ansistring;
   i,j: integer;
begin
   EnterCriticalSection(WorkFormCs);
   try
      //
      // 先找到窗体...
      j:=-1;
      for i:=0 to WorkFormCount-1 do
         if WorkForms[i].FormHandle=FormHandle then
            begin
               j:=i;
               break;
            end;
      result:=(j<>-1);
      //
      // 找到，显示窗口...
      if result then
         begin
            //
            // 设置窗口入口参数包...
            WorkForms[j].Form.InputParcel:=TQBParcel(pointer(InputParcelPtr));
            //
            // 准备出口参数包...
            if assigned(WorkForms[j].Form.OutputParcel) then
               WorkForms[j].Form.OutputParcel.Clear
            else
               WorkForms[j].Form.OutputParcel:=TQBParcel.Create;
            //
            // 设置窗口的父窗口...
            {$IFDEF WIN64}
            ParentForm:=TForm(Pointer(WorkForms[j].Form.InputParcel.GetInt64Goods('ParentFormPtr')));
            {$ELSE}
            ParentForm:=TForm(Pointer(WorkForms[j].Form.InputParcel.GetIntegerGoods('ParentFormPtr')));
            {$ENDIF}
            WorkForms[j].Form.ParentForm:=ParentForm;
            //
            // 设置窗口标题...
            FormCaption:=WorkForms[j].Form.InputParcel.GetAnsiStringGoods('FormCaption');
            WorkForms[j].Form.Caption:=string(FormCaption);
            //
            // 假如在ActiveForm里使用，需要特别设置...
            if WorkForms[j].Form.InputParcel.GetBooleanGoods('IsActiveForm') then
               begin
                  WorkForms[j].Form.ParentWindow:=ParentForm.Handle;
                  WorkForms[j].Form.Parent:=ParentForm;
                  WorkForms[j].Form.BringToFront;
               end;
            //
            // 设置窗体一般属性...
            WorkForms[j].Form.Position:=poDesktopCenter;
            WorkForms[j].Form.WindowState:=wsNormal;
            //
            // 显示模态窗...
            WorkForms[j].Form.ShowModal;
            WorkForms[j].Form.ParentForm:=nil;
            //
            // 返回出口参数包...
            OutputParcelPtr:=Parcel2Mem(WorkForms[j].Form.OutputParcel);
         end;
   finally
      LeaveCriticalSection(WorkFormCs);
   end;
end;

//
// 非模态窗形式显示工作窗体...
function DllFormShow(FormHandle: integer;
   InputParcelPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF}): boolean; stdcall;
var
   i,j: integer;
   FormCaption: AnsiString;
begin
   EnterCriticalSection(WorkFormCs);
   try
      //
      // 先找到窗体...
      j:=-1;
      for i:=0 to WorkFormCount-1 do
         if WorkForms[i].FormHandle=FormHandle then
            begin
               j:=i;
               break;
            end;
      result:=(j<>-1);
      //
      // 找到，显示窗口...
      if result then
         begin
            //
            // 设置窗口入口参数包...
            WorkForms[j].Form.InputParcel:=TQBParcel(pointer(InputParcelPtr));
            //
            // 准备出口参数包...
            if assigned(WorkForms[j].Form.OutputParcel) then
               FreeAndNil(WorkForms[j].Form.OutputParcel);
            //
            // 窗口标题...
            FormCaption:=WorkForms[j].Form.InputParcel.GetAnsiStringGoods('FormCaption');
            WorkForms[j].Form.Caption:=string(FormCaption);
            //
            WorkForms[j].Form.Position:=poDesktopCenter;
            WorkForms[j].Form.WindowState:=wsNormal;
            WorkForms[j].Form.Show;
         end;
   finally
      LeaveCriticalSection(WorkFormCs);
   end;
end;

//
// 停靠工作窗体...
function DllFormDock(FormHandle: integer;
   InputParcelPtr: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF}): boolean; stdcall;
var
   i,j: integer;
   DockPanel: TPanel;
begin
   EnterCriticalSection(WorkFormCs);
   try
      //
      // 先找到窗体...
      j:=-1;
      for i:=0 to WorkFormCount-1 do
         if WorkForms[i].FormHandle=FormHandle then
            begin
               j:=i;
               break;
            end;
      result:=(j<>-1) and (not WorkForms[i].Form.Visible);
      //
      // 找到，显示窗口...
      if result then
         begin
            //
            // 设置窗口入口参数包...
            WorkForms[j].Form.InputParcel:=TQBParcel(pointer(InputParcelPtr));
            //
            // 准备出口参数包...
            if assigned(WorkForms[j].Form.OutputParcel) then
               FreeAndNil(WorkForms[j].Form.OutputParcel);
            //
            // 设置窗口挂靠...
            {$IFDEF WIN64}
            DockPanel:=TPanel(Pointer(WorkForms[j].Form.InputParcel.GetInt64Goods('DockPanelPtr')));
            {$ELSE}
            DockPanel:=TPanel(Pointer(WorkForms[j].Form.InputParcel.GetIntegerGoods('DockPanelPtr')));
            {$ENDIF}
            WorkForms[j].Form.Position:=poDefault;
            WorkForms[j].Form.WindowState:=wsMaximized;
            WorkForms[j].Form.Left:=0;
            WorkForms[j].Form.Top:=0;
            WorkForms[j].Form.ParentWindow:=DockPanel.Handle;
            WorkForms[j].Form.BoundsRect:=DockPanel.ClientRect;
            WorkForms[j].Form.Show;
         end;
   finally
      LeaveCriticalSection(WorkFormCs);
   end;
end;

//
// 隐藏窗体...
function DllFormHide(FormHandle: integer): boolean; stdcall;
var
   i,j: integer;
begin
   EnterCriticalSection(WorkFormCs);
   try
      //
      // 先找到窗体...
      j:=-1;
      for i:=0 to WorkFormCount-1 do
         if WorkForms[i].FormHandle=FormHandle then
            begin
               j:=i;
               break;
            end;
      result:=(j<>-1);
      //
      // 找到，显示窗口...
      if result then
         WorkForms[j].Form.Hide;
   finally
      LeaveCriticalSection(WorkFormCs);
   end;
end;

//
// 释放窗体...
function DllFormFree(FormHandle: integer): boolean; stdcall;
var
   i,j: integer;
begin
   EnterCriticalSection(WorkFormCs);
   try
      //
      // 先找到窗体...
      j:=-1;
      for i:=0 to WorkFormCount-1 do
         if WorkForms[i].FormHandle=FormHandle then
            begin
               j:=i;
               break;
            end;
      result:=(j<>-1);
      //
      // 找到，释放窗体...
      if result then
         begin
            WorkForms[j].Form.Hide;
            if assigned(WorkForms[j].Form.OutputParcel) then
               FreeAndNil(WorkForms[j].Form.OutputParcel);
            FreeAndNil(WorkForms[j].Form);
            for i:=j to WorkFormCount-2 do
               WorkForms[i]:=WorkForms[i+1];
            dec(WorkFormCount);
            setlength(WorkForms,WorkFormCount);
            FreeSocketPool;
         end;
   finally
      LeaveCriticalSection(WorkFormCs);
   end;
end;

//
// 初始与善后...
procedure DLLHandler(Reason: integer);
var
   i: integer;
begin
  case Reason of
    //
    // 加载DLL时...
    DLL_Process_Attach:
      begin
         HandleCount:=0;
         WorkFormCount:=0;
         SetLength(WorkForms,WorkFormCount);
         InitializeCriticalSection(WorkFormCs);
         //
         OldApp:=Application;
//         OldScreen:=Screen;
      end;
    //
    // 卸载DLL时...
    DLL_PROCESS_DETACH:
      begin
         for i:=0 to WorkFormCount-1 do
            begin
              if assigned(WorkForms[i].Form) then
                 begin
                    if assigned(WorkForms[i].Form.OutputParcel) then
                       FreeAndNil(WorkForms[i].Form.OutputParcel);
                    freeandnil(WorkForms[i].Form);
                 end;
            end;
         WorkFormCount:=0;
         SetLength(WorkForms,WorkFormCount);
         DeleteCriticalSection(WorkFormCs);
         Application:=OldApp;
//         Screen:=OldScreen;
      end;
  end;
end;

//
// 导出...
exports
   DllFormInit,DllFormCreate,DllFormShowModal,DllFormShow,DllFormDock,DllFormHide,DllFormFree;

//
// 初始化...
begin
   DLLProc:=@DLLHandler;
   DLLHandler(DLL_Process_Attach);
end.
