﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd, QBMisc,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTextEdit, cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, cxGridExportLink, DBAccess, Uni, cxGridBandedTableView, cxLabel,
  cxContainer, cxMemo, Vcl.Menus, cxTLExportLink, cxCalc;

type
  TWorkForm = class(TForm)
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    DELS: TcxStyle;
    AddS: TcxStyle;
    ModS: TcxStyle;
    DefaultS: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    flwpnl1: TFlowPanel;
    pnl1: TPanel;
    rg1: TRadioGroup;
    pnl2: TPanel;
    cbb3: TComboBox;
    txt1: TStaticText;
    pnl6: TPanel;
    Q_year: TLabeledEdit;
    pnl8: TPanel;
    Q_dept: TLabeledEdit;
    pnl4: TPanel;
    Q_ima01: TLabeledEdit;
    cbb1: TComboBox;
    pnl7: TPanel;
    Q_ima02: TLabeledEdit;
    cbb2: TComboBox;
    pnl5: TPanel;
    btn1: TButton;
    btn2: TButton;
    TQ: TClientDataSet;
    CDS: TClientDataSet;
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    cxGLevel1: TcxGridLevel;
    cxG: TcxGrid;
    cGB: TcxGridTableView;
    cGBC_cua00: TcxGridColumn;
    cGBC_cua01: TcxGridColumn;
    cGBC_cua02: TcxGridColumn;
    cGBC__ima02: TcxGridColumn;
    cGBC_A: TcxGridColumn;
    cGBC_B: TcxGridColumn;
    cGBC_C: TcxGridColumn;
    cGBC_D: TcxGridColumn;
    cGBC_E: TcxGridColumn;
    cGBC_F: TcxGridColumn;
    cGBC_G: TcxGridColumn;
    cGBC_H: TcxGridColumn;
    cGBC_I: TcxGridColumn;
    cGBC_J: TcxGridColumn;
    cGBC_K: TcxGridColumn;
    cGBC_TOT: TcxGridColumn;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Cost_Run: TButton;
    Cost_tree: TButton;
    Cost_BRun: TButton;
    lstMtreel: TcxTreeList;
    lstMtreelColumn1: TcxTreeListColumn;
    lstMtreelColumn2: TcxTreeListColumn;
    lstMtreelColumn3: TcxTreeListColumn;
    lstMtreelColumn4: TcxTreeListColumn;
    lstMtreelColumn5: TcxTreeListColumn;
    lstMtreelColumn6: TcxTreeListColumn;
    lstMtreelColumn7: TcxTreeListColumn;
    lstMtreelColumn8: TcxTreeListColumn;
    lstMtreelColumn9: TcxTreeListColumn;
    lstMtreelColumn10: TcxTreeListColumn;
    lstMtreelColumn11: TcxTreeListColumn;
    lstMtreelColumn12: TcxTreeListColumn;
    lstMtreelColumn13: TcxTreeListColumn;
    lstMtreelColumn14: TcxTreeListColumn;
    lstMtreelColumn15: TcxTreeListColumn;
    lstMtreelColumn16: TcxTreeListColumn;
    lstMtreelColumn17: TcxTreeListColumn;
    lstMtreelColumn18: TcxTreeListColumn;
    lstMtreelColumn19: TcxTreeListColumn;
    lstMtreelColumn20: TcxTreeListColumn;
    lstMtreelColumn21: TcxTreeListColumn;
    lstMtreelColumn22: TcxTreeListColumn;
    lstMtreelColumn23: TcxTreeListColumn;
    lstMtreelColumn24: TcxTreeListColumn;
    lstMtreelColumn25: TcxTreeListColumn;
    lstMtreelColumn26: TcxTreeListColumn;
    lstMtreelColumn27: TcxTreeListColumn;
    lstMtreelColumn28: TcxTreeListColumn;
    lstMtreelColumn29: TcxTreeListColumn;
    lstMtreelColumn30: TcxTreeListColumn;
    lstMtreelColumn31: TcxTreeListColumn;
    lstMtreelColumn32: TcxTreeListColumn;
    lstMtreelColumn33: TcxTreeListColumn;
    lstMtreelColumn34: TcxTreeListColumn;
    lstMtreelColumn35: TcxTreeListColumn;
    lstMtreelColumn36: TcxTreeListColumn;
    lstMtreelColumn37: TcxTreeListColumn;
    TCDS: TClientDataSet;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Panel1: TPanel;
    leBB: TLabeledEdit;
    cGBColumn1: TcxGridColumn;
    cGBColumn2: TcxGridColumn;
    cGBColumn3: TcxGridColumn;
    Cost_treePl: TButton;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure cGBCustomDrawGroupSummaryCell(Sender: TObject; ACanvas: TcxCanvas;
      ARow: TcxGridGroupRow; AColumn: TcxGridColumn;
      ASummaryItem: TcxDataSummaryItem;
      AViewInfo: TcxCustomGridViewCellViewInfo; var ADone: Boolean);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure Cost_treeClick(Sender: TObject);
    procedure Cost_RunClick(Sender: TObject);
    procedure Cost_BRunClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure Cost_treePlClick(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string; DS: TClientDataSet): Boolean;
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    function F_BM: string;
    function F_ND: string;
    function F_PH: string;
    function FilterStr: string;
    function GetBM: string;
    function GetND: string;
    function GetPh: string;
    function GetPM: string;
    function GetZT: string;
    function GetBB: string; // 获取版本值
    procedure GetZThDP(userID: string = '0912101');
    procedure SetLJSM;
    procedure FillCGrid(D: TClientDataSet);
    procedure ExtractedMethod(var filterS: string);
    procedure RunTreeList(lj, dp, Ys, V, pf: string);
    procedure FullRTTree(ARecords: TstringList; x: TcxTreeList);
    function F_BB: string;
    procedure PlRunTreeList(lj, dp, Ys, V: string);
    procedure PlFullRTTree(ARecords: TstringList; x: TcxTreeList);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'SCSTDPFCOST_DLL';
  // 业务代码
  TreeSplit = '@';
  ZT = 'GWGS,GNGS,YKGS,SHBM,PKGS';
  DZT: array [0 .. 4] of string = ('ERP', 'ERP_GN', 'ERP_YK', 'ERP_SHBM',
    'ERP_PKGS');
  // 记录账套对应的UDAC数据库定义
  BMSQL = 'SELECT GEN03 FROM %s.GEN_FILE WHERE GEN01 = ''%s''';

  QSQL = 'WITH dd AS ' + sLineBreak + '(SELECT MIN(mlj||''^''||R) D,bmb03 ' +
    sLineBreak + ' FROM stdcost_cost %s ' + sLineBreak + ' GROUP BY DPT,bmb03) '
    + sLineBreak +
    ' SELECT dpt,a.bmb03,b.ima02,b.ima021,b.ima25,a.c001+a.c001T c001,a.c002+a.c002T c002,a.c003+a.c003T c003,a.c004+a.c004T c004,'
    + sLineBreak +
    'a.c005+a.c005T c005,a.c101+a.c101T c101,a.c102+a.c102T c102,a.c201+a.c201T c201,a.c202+a.c202T c202,a.c204+a.c204T c204,'
    + 'a.c203+a.c203T c203,a.c001+a.c001T+a.c002+a.c002T+a.c003+a.c003T+a.c004+a.c004T+a.c005+a.c005T+a.c101+a.c101T+a.c102+a.c102T+a.c201+a.c201T+a.c202+a.c202T+a.c204+a.c204T+a.c203+a.c203T TT,'
    + sLineBreak + 'mlj,yl ' + sLineBreak +
    '   FROM stdcost_cost a LEFT JOIN ima_file b ON a.bmb03 = b.ima01  ' +
    sLineBreak +
    '   WHERE  %s AND %s AND %s AND (mlj,R,bmb03) IN (SELECT SUBSTR(D,1,INSTR(D,''^'')-1),SUBSTR(D,INSTR(D,''^'')+1,10),bmb03 FROM dd) ';

  TSQL = 'SELECT a.bmb03,b.ima02,b.ima021,b.ima25,a.IsM,a.ecd07,c.eca02,nvl(a.YL,1) YL,nvl(a.bmb08,0) bmb08,nvl(a.JGXH,0) JGXH,nvl(a.BGC07,0) BGC07,nvl(a.TIm,0) Tim,nvl(a.eqv,0) eqv,'
    + 'nvl(a.c001,0) c001,nvl(a.c001T,0) c001T,nvl(a.c002,0) c002,nvl(a.c002T,0) c002T,nvl(a.c003,0) c003,nvl(a.c003T,0) c003T,nvl(a.c004,0) c004,nvl(a.c004T,0) c004T,nvl(a.c005,0) c005,nvl(a.c005T,0) c005T,'
    + 'a.c101,a.c101T,a.c102,a.c102T,a.c201,a.c201T,a.c202,a.c202T,a.c204,a.c204T,a.c203,a.c203T,'
    + 'a.c001+a.c001T+a.c002+a.c002T+a.c003+a.c003T+a.c004+a.c004T+a.c005+a.c005T+a.c101+a.c101T+a.c102+a.c102T+a.c201+a.c201T+a.c202+a.c202T+a.c204+a.c204T+a.c203+a.c203T TT,a.T,a.R,a.MLJ'
    + '  FROM stdcost_cost a ' + 'LEFT JOIN ima_file b ON a.bmb03 = b.ima01  ' +
    'LEFT JOIN eca_file c ON c.eca01 = a.ecd07 %s ORDER BY R';
  MSQL = 'WITH cbpmb AS ' + sLineBreak +
    ' (SELECT ima01 FROM %0:s.IMA_FILE,%0:s.TC_CUA_FILE WHERE %1:s )' +
    sLineBreak +
    'SELECT tc_cua00,tc_cua01,tc_cua02,ima02,ima021,tc_cua16,gem02,tc_cua14,eca02, '
    + '原材料, 辅料,模具,附件,包装,直接人工,间接人工,电费,天然气费,其他费用,折旧 FROM       ' +
    '(                                                                                     '
    + sLineBreak +
    'SELECT tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14,SUM(A) AS 原材料, SUM(B) AS 辅料,SUM(C) AS 模具,  '
    + 'SUM(D) AS 附件,SUM(E) AS 包装,SUM(F) AS 直接人工, SUM(G) AS 间接人工,SUM(H) AS 电费,SUM(I) AS 天然气费,'
    + 'SUM(J) AS 其他费用,SUM(K) AS 折旧 FROM                                                            '
    + '( ' + sLineBreak +
    'SELECT tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14,A,B,C,D,E,F,G,H,I,J,K FROM  '
    + sLineBreak +
    '(SELECT tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14,tc_cua07,tc_cua04 FROM %0:s.tc_cua_file WHERE %2:s) '
    + sLineBreak +
    'PIVOT (SUM(tc_cua07) FOR tc_cua04 IN                                                                    '
    + '(''001'' A,''002'' B,''003'' C,''004'' D,''005'' E,''101'' F,''102'' G,''201'' H,''202'' I,''203'' J,''204'' K))'
    + sLineBreak + ')   ' + sLineBreak +
    'GROUP BY tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14  ' + sLineBreak +
    ') aa   ' + sLineBreak + 'LEFT JOIN %0:s.ima_file ON ima01 = tc_cua02  ' +
    sLineBreak + 'LEFT JOIN %0:s.eca_file ON eca01 = tc_cua14  ' + sLineBreak +
    'LEFT JOIN %0:s.gem_file ON gem01 = tc_cua16  ' + sLineBreak +
    'ORDER BY 1,2,3,6,8 DESC';

var
  zhanghu: string;
  RunInfo: string;
  user_zt, user_dept: string; // 用户所在部门与账套
  ReadAllZtCb: Boolean; // 定义断言是否可读取所有账套的成本  实现：是否可选择账号所在账套以外的账套
  ReadAllCjCb: Boolean; // 定义断言是否可读取所有车间的成本  实现：是否可修改自己所在账套以外的账套
  RunView: Boolean; // 定义断言是否拥有可以进行标准成本计算的权限
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象（不再使用主程序传回来的DatabaseID值）...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 根据用户信息获取所在账套与部门
  GetZThDP(zhanghu);
  // 根据用户信息初始化数据集对象（不再使用主程序传回来的DatabaseID值）...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := DZT[cbb3.ItemIndex];

  // 权限的设置
  // 获取是否可修改项目编号权限,设置账套的只读性
  GetPrivilege('ASSERT', '2', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    ReadAllZtCb := false
  else
    ReadAllZtCb := Not(PrivilegeCDS.FieldByName('R1').AsString = '0');
  // 获取是否可修改项目编号权限,设置部门的只读性
  GetPrivilege('ASSERT', '3', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    ReadAllCjCb := false
  else
    ReadAllCjCb := Not(PrivilegeCDS.FieldByName('R1').AsString = '0');
  // 获取标准成本计算权限
  GetPrivilege('ASSERT', '4', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    RunView := false
  else
    RunView := Not(PrivilegeCDS.FieldByName('R1').AsString = '0');

  // 界面初始化

  Q_dept.Enabled := ReadAllCjCb;
  cbb3.Enabled := ReadAllZtCb;
  Cost_Run.Visible := RunView;
  Cost_BRun.Visible := RunView;
  // 20190308修改 配方成本功能不需要进行计算
  Cost_Run.Visible := false;
  Cost_BRun.Visible := false;

  Q_dept.Text := user_dept;
  cbb3.Text := user_zt;
  Q_year.Text := IntToStr(YearOf(Now));

  ResizeForm;
end;

// 相关函数
procedure TWorkForm.SetLJSM;
var
  Multi: Boolean;
begin
  Multi := (cbb1.ItemIndex > 0) OR (cbb2.ItemIndex > 0);
  if Multi then
    rg1.ItemIndex := 1
  else
    rg1.ItemIndex := 0;
end;

procedure TWorkForm.GetZThDP(userID: string = '0912101');
var
  sl: TstringList;
  i: Integer;
  SQL: string;
begin
  sl := TstringList.Create;
  sl.DelimitedText := ZT;
  for i := 0 to sl.Count - 1 do
  begin
    SQL := Format(BMSQL, [sl[i], userID]);
    RunCDS(SQL, TQ);
    if TQ.RecordCount = 1 then
      Break;
  end;
  user_zt := sl[i];
  user_dept := TQ.FieldByName('GEN03').Text;
  cbb3.ItemIndex := i;
  Q_ima02.Text := dba.TargetDatabaseId;
  Q_dept.Text := user_dept;
  FreeAndNil(sl);
end;

function TWorkForm.GetPh: string;
// 获取品号筛选条件
var
  s: string;
  T: string;
  D: string;
  V: string;
  L: Integer;
begin
  s := Trim(Q_ima01.Text);

  // 根据账套选配方的固定值
  case cbb3.ItemIndex of
    0 .. 4:
      T := '100'
  else
    T := '1';
  end;
  // 根据账套选配方料号的长度
  case cbb3.ItemIndex of
    0 .. 4:
      L := 8
  else
    L := 8;
  end;

  V := ' LENGTH(BMB03) = ' + L.ToString + ' AND ';
  D := V + ' BMB03 LIKE ''' + T + '%''';
  if s = '' then
    Exit(D);

  case cbb1.ItemIndex of
    0:
      result := ' BMB03 = ''' + s + '''';
    1:
      result := ' BMB03 LIKE ''' + T + '%' + s + '%''';
    2:
      result := ' BMB03 LIKE ''' + T + s + '%''';
    3:
      result := ' BMB03 LIKE ''' + T + '%' + s + ''''
  else
    result := ' 1 = 1 ';
  end;
  result := V + result;
end;

function TWorkForm.GetPM: string;
// 获取品名筛选条件
begin
  //
end;

function TWorkForm.GetND: string;
// 获取年度，若录入异常，默认为当前年度
var
  s: string;
  i: Integer;
begin
  s := Trim(Q_year.Text);
  i := StrToIntDef(s, 0);
  if (i < 2000) or (i > 3000) then
    result := IntToStr(YearOf(Now))
  else
    result := s;
end;

function TWorkForm.GetBM: string;
// 获取部门
begin
  if Trim(Q_dept.Text) = '' then
    result := ''
  else
    result := Trim(Q_dept.Text);
end;

function TWorkForm.GetZT: string;
// 获取账套
begin
  if cbb3.Text = '' then
    result := user_zt
  else
    result := cbb3.Text;
end;

function TWorkForm.GetBB: string;
var
  SQL, s: string;
begin
  result := '';
  SQL := 'SELECT  TC_ECE01 FROM TC_ECE_FILE WHERE TC_ECE03 = ''%s'' AND TC_ECE02 = %s AND TC_ECEUD01 = ''Y''';
  if dba.ReadSimpleResult(Format(SQL, [GetBM, GetND]), s) then
  begin
    if s = '' then
      result := '1'
    else
      result := s;
  end;
  leBB.Text := result;
end;

function TWorkForm.F_BB: string;
var
  s: string;
begin
  s := GetBB;
  if s = '' then
    result := ' VER  = ''1'''
  else
    result := ' VER  = ''' + s + '''';
end;

function TWorkForm.F_ND: string;
begin
  result := ' YEARS = ''' + GetND + '''';
end;

function TWorkForm.F_BM: string;
var
  s: string;
begin
  s := GetBM;
  if s = '' then
    s := ' ';
  result := ' DPT = ''' + s + '''';
end;

function TWorkForm.F_PH: string;
// 获取料号的筛选条件
var
  s, ph, fnd, fbm: string;
begin
  ph := GetPh;
  fnd := F_ND + ' AND ' + F_BB;
  fbm := F_BM;
  if ph = '' then
    Exit('1=1');

  // 年度条件
  s := fnd;
  // 料号条件
  if ph <> '' then
    s := s + ' AND ' + ph;
  // 部门条件
  if fbm <> '' then
    s := s + ' AND ' + fbm;
  // // 品名条件
  // if Pm <> '' then
  // s := s + ' AND ' + Pm;
  result := s;
end;

function TWorkForm.FilterStr: string;
// 根据索引顺序组织筛选条件的顺序
// 索引顺序：年度cua00/版本cua01/品号cua02/作业编号cua03/成本项目cua04/成本类型cua05/原料料号cua06/部门编号cua16/所在账套cua17
var
  s, T: string;
  fnd, fph, fbm: string;
begin
  fbm := F_BM;
  // 年度绝不为空,必须加版本过滤，否则不能使用索引。版本条件做为常量定义在banben字段中
  s := F_ND + ' AND ' + F_BB;
  // 品号条件
  T := F_PH;
  if T <> '1=1' then
    s := s + ' AND tc_cua02 IN (SELECT IMA01 FROM CBPMB) ';
  // 部门条件
  T := F_BM;
  if T <> '' then
    s := s + ' AND ' + T;

  result := s;
end;
// -----------------------------------------------------------------------------

procedure TWorkForm.ExtractedMethod(var filterS: string);
var
  lj: string;
  bm: string;
begin
  lj := GetPh;
  bm := GetBM;
  filterS := ' WHERE 1 = 1';
  if lj <> '' then
  begin
    filterS := filterS + ' AND ' + lj;
  end;
  if bm <> '' then
    filterS := filterS + ' AND TC_ECG03 = ''' + bm + ''' ';

  filterS := filterS + ' AND TC_ECG02 = ' + Q_year.Text;
end;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string; DS: TClientDataSet): Boolean;
var
  s, T: string;
begin
  result := false;
  if not dba.ReadDataset(SQL, DS) then
  begin
    application.MessageBox('读数据集失败！', 'Infomation');
    dba.GetLastError(s, T);
    LogMsg(pchar(DLL_KEY + s + ':' + T));
  end
  else
  begin
    DS.Active := True;
    result := True;
  end;
end;

procedure TWorkForm.FillCGrid(D: TClientDataSet);
var
  i, j: Integer;
  yl: Double;
begin
  if D.RecordCount = 0 then
    Exit;
  cGB.DataController.RecordCount := 0;
  CDS.First;
  cGB.BeginUpdate();
  while Not CDS.Eof do
  begin
    // 取当前记录用量
    yl := CDS.Fields.FieldByName('yl').AsFloat;
    if yl = 0 then
      yl := 1;

    i := cGB.DataController.AppendRecord;
    for j := 0 to cGB.ColumnCount - 1 do
    begin
      if cGB.Columns[j].DataBinding.ValueType = 'Float' then
        cGB.DataController.Values[i, j] :=
          strtoFloatDef(CDS.Fields.Fields[j].AsString, 0) / yl
      else
        cGB.DataController.Values[i, j] := CDS.Fields.Fields[j].AsString;

    end;
    CDS.Next;
  end;
  cGB.EndUpdate;
end;

procedure TWorkForm.btn1Click(Sender: TObject);
var
  V, s1, f, SQL: string;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowError('请选择账套后继续', '提示');
    Exit;
  end;

  pgc1.ActivePageIndex := 0;

  ExtractedMethod(f);
  // memo1.Lines.Add(f);
  s1 := StringReplace(f, 'TC_ECG04', 'bmb03', [rfReplaceAll]);
  s1 := StringReplace(s1, 'TC_ECG03', 'DPT', [rfReplaceAll]);
  s1 := StringReplace(s1, 'TC_ECG02', 'Years', [rfReplaceAll]);
  s1 := s1 + ' AND ' + F_BB + ' AND T > 0 ';
  // Memo1.Lines.Add('s1'+s1);
  V := GetBM;
  if V = '' then
    V := ' 1=1'
  else
    V := ' DPT = ''' + V + '''';
  SQL := Format(QSQL, [s1, F_ND, F_BB, V]);

  // Memo1.Lines.Text := SQL;
  // exit;
  if RunCDS(SQL, CDS) then
  begin
    if CDS.RecordCount = 0 then
      application.MessageBox('没有查到相关信息', 'Infomation')
    else
      FillCGrid(CDS);
  end;

  ResizeForm;
end;

procedure TWorkForm.btn2Click(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  strFileName: string;
begin
  OpenDlg := TOpenDialog.Create(nil);
  try
    OpenDlg.Filter := 'EXCLE(*.xlsx)|*.xlsx';
    OpenDlg.DefaultExt := '*.xlsx';
    if OpenDlg.Execute then
    begin
      strFileName := Trim(OpenDlg.FileName);
      if strFileName <> '' then
      begin
        case pgc1.ActivePageIndex of
          0:
            ExportGridToXLSX(strFileName, cxG, True, True);
          1:
            cxExportTLToXLSX(strFileName, lstMtreel, True, True);
        end;

        application.MessageBox(pchar('转换完成'), '提示');
      end;
    end;
  finally
    FreeAndNil(OpenDlg);
  end;
end;

procedure TWorkForm.cGBCustomDrawGroupSummaryCell(Sender: TObject;
  ACanvas: TcxCanvas; ARow: TcxGridGroupRow; AColumn: TcxGridColumn;
  ASummaryItem: TcxDataSummaryItem; AViewInfo: TcxCustomGridViewCellViewInfo;
  var ADone: Boolean);
begin
  // 去除汇总项的'SUM='字样
  if Copy(AViewInfo.Text, 1, 4) = 'SUM=' then
  begin
    AViewInfo.Text := Copy(AViewInfo.Text, 5, Length(AViewInfo.Text) - 4);
    AViewInfo.AlignmentHorz := taRightJustify;
  end;
end;

procedure Runs;
begin
  sleep(700);
end;

procedure TWorkForm.Cost_BRunClick(Sender: TObject);
var
  filterS, T, s, V, SQL: string;
  i: Integer;
  Tp: TQBParcel;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowError('请选择账套后继续', '错误');
    Exit;
  end;
  if Q_dept.Text = '' then
  begin
    ShowError('料件部门不能为空', '错误');
    Exit;
  end;

  ExtractedMethod(filterS);
  V := GetBB;
  SQL := Format('SELECT count(1) RT FROM TC_ECG_FILE %s', [filterS]);
  if not dba.ReadSimpleResult(SQL, s) then
  begin
    ShowError('查询出错，请重试', '错误');
    Exit;
  end
  else
  begin
    if s = '0' then
    begin
      QBMisc.ShowMessage('没有要计算的成本料件!', '信息');
      Exit;
    end;
  end;

  s := '共需计算 ' + s + ' 条记录';

  SQL := Format('SELECT TC_ECG04,TC_ECG03 RT FROM TC_ECG_FILE %s', [filterS]);

  if not RunCDS(SQL, CDS) then
  begin
    ShowError('查询出错，请重试', '错误');
    Exit;
  end;
  i := 1;
  Tp := TQBParcel.Create;

  CDS.First;
  while not CDS.Eof do
  begin
    Tp.Clear;
    Tp.PutStringGoods('@iUser', zhanghu, gdInput);
    Tp.PutStringGoods('@iLiaoJian', CDS.Fields[0].Text, gdInput);
    Tp.PutStringGoods('@idept', CDS.Fields[1].Text, gdInput);
    Tp.PutIntegerGoods('@iYear', strToint(GetND), gdInput);
    Tp.PutStringGoods('@iVer', V, gdInput);
    Tp.PutStringGoods('@iZT', cbb3.Text, gdInput);
    Runs;
    if dba.ExecuteStoredProc('gwgs.StdRun', Tp) then
      RunInfo := '已完成第' + i.ToString + '条记录的计算'
    else
      RunInfo := '第' + i.ToString + '条记录计算失败';
    Inc(i);
    CDS.Next;
  end;
  QBMisc.ShowMessage('计算完成', '信息');
  Tp.Free;
end;

procedure TWorkForm.Cost_RunClick(Sender: TObject);
var
  T, E, C, V: string;
  Tp: TQBParcel;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowError('请选择账套后继续', '系统错误');
    Exit;
  end;
  if (Q_ima01.Text = '') or (cbb1.ItemIndex > 0) then
  begin
    ShowError('料件不能为空且筛选条件只能置为<等于>才能进行计算', '错误');
    Exit;
  end;
  RunInfo := '共处理1条记录，正在处理第1条记录';
  sleep(500);
  T := GetBM;
  V := GetBB;
  Tp := TQBParcel.Create;

  Tp.PutStringGoods('@iUser', zhanghu, gdInput);
  Tp.PutStringGoods('@iLiaoJian', Trim(Q_ima01.Text), gdInput);
  Tp.PutStringGoods('@idept', T, gdInput);
  Tp.PutIntegerGoods('@iYear', strToint(GetND), gdInput);
  Tp.PutStringGoods('@iVer', V, gdInput);
  Tp.PutStringGoods('@iZT', cbb3.Text, gdInput);

  if dba.ExecuteStoredProc('gwgs.StdRun', Tp) then
    application.MessageBox(pchar('运算成功'), pchar('计算'))
  else
  begin
    ShowError('运算失败，请重试', '错误');
    dba.GetLastError(E, C);
    LogMsg('Run Err: ' + E + '--' + C);
  end;

  Tp.Free;
end;

procedure TWorkForm.FullRTTree(ARecords: TstringList; x: TcxTreeList);
var
  i, j: Integer;
  AValues: TstringList;
  cNode: TcxTreeListNode;
  xtreea: array [0 .. 10] of TcxTreeListNode;
  pLev, cLev: Integer;
begin
  AValues := TstringList.Create;
  AValues.StrictDelimiter := True; // bug修复，不让空格自动分割行
  AValues.Delimiter := TreeSplit;
  x.Clear;

  pLev := 0;
  cLev := 0;
  for j := 0 to ARecords.Count - 1 do
  begin
    AValues.DelimitedText := ARecords[j];
    try
      cLev := strToint(AValues[AValues.Count - 1]);
    except
      cLev := 0;
    end;
    if cLev = 0 then
    begin
      cNode := x.add;
      xtreea[0] := cNode;
    end
    else
    begin
      cNode := xtreea[cLev - 1].AddChild;
      xtreea[cLev] := cNode;
    end;
    for i := 0 to AValues.Count - 1 do
      cNode.Texts[i] := AValues[i];
  end;
  AValues.Free;
end;

procedure TWorkForm.PlFullRTTree(ARecords: TstringList; x: TcxTreeList);
// 多个料号树型显示 与FullRTTree比较，取消 x.Clear行
var
  i, j: Integer;
  AValues: TstringList;
  cNode: TcxTreeListNode;
  xtreea: array [0 .. 10] of TcxTreeListNode;
  pLev, cLev: Integer;
begin
  AValues := TstringList.Create;
  AValues.StrictDelimiter := True; // bug修复，不让空格自动分割行
  AValues.Delimiter := TreeSplit;
  // x.Clear;
  pLev := 0;
  cLev := 0;
  for j := 0 to ARecords.Count - 1 do
  begin
    AValues.DelimitedText := ARecords[j];
    try
      cLev := strToint(AValues[AValues.Count - 1]);
    except
      cLev := 0;
    end;
    if cLev = 0 then
    begin
      cNode := x.add;
      xtreea[0] := cNode;
    end
    else
    begin
      cNode := xtreea[cLev - 1].AddChild;
      xtreea[cLev] := cNode;
    end;
    for i := 0 to AValues.Count - 1 do
      cNode.Texts[i] := AValues[i];
  end;

  AValues.Free;
end;

procedure TWorkForm.RunTreeList(lj, dp, Ys, V, pf: string);
var
  s1, s, SQL: string;
  TS: TstringList;
  R, T: Integer; // 记录号与层次
  yl: Double;
begin
  s1 := ' WHERE  MLJ = ''' + lj + ''' AND dpt = ''' + dp + ''' AND YearS = ' +
    Ys + ' AND VER  = ''' + V + '''';
  SQL := Format(TSQL, [s1]);
  // memo1.Lines.Text := sql;

  if not RunCDS(SQL, TCDS) then
    Exit;
    TCDS.Filtered := False;
//  Memo1.Text := TCDS.RecordCount.ToString + '  ' + pf;
  TCDS.First;
  lstMtreel.Clear;
  s := '';
  // 将指定配方的相关记录写入stringlist中
  R := -1;
  T := -1;
  TS := TstringList.Create;
  TS.StrictDelimiter := True;
  TS.Clear;
  TCDS.First;
  LogMsg('开始循环');
  while not TCDS.Eof do
  begin
    if R = -1 then
    begin
      if TCDS.FieldByName('bmb03').AsString = pf then
      begin
        R := TCDS.FieldByName('R').AsInteger;
        T := TCDS.FieldByName('T').AsInteger;
        // 获取当前记录的用量，为零变为1
        yl := TCDS.FieldByName('YL').AsFloat;
        if yl = 0 then
          yl := 1;
      end
      else
      begin
        TCDS.Next;
        Continue;
      end;
    end;
    // 已获取层级后，当层级大于最外层时，循环终止
    if (TCDS.FieldByName('R').AsInteger > R) and
      (TCDS.FieldByName('T').AsInteger <= T) then
      Break;
    s := TCDS.FieldByName('bmb03').AsString + TreeSplit +
      TCDS.FieldByName('ima02').AsString + TreeSplit +
      TCDS.FieldByName('ima021').AsString + TreeSplit +
      TCDS.FieldByName('ima25').AsString + TreeSplit + TCDS.FieldByName('IsM')
      .AsString + TreeSplit + TCDS.FieldByName('ecd07').AsString + TreeSplit +
      TCDS.FieldByName('eca02').AsString + TreeSplit +
      FloatToStr(TCDS.FieldByName('YL').AsFloat / yl) + TreeSplit +
      TCDS.FieldByName('bmb08').AsString + TreeSplit + TCDS.FieldByName('JGXH')
      .AsString + TreeSplit + TCDS.FieldByName('BGC07').AsString + TreeSplit +
      TCDS.FieldByName('Tim').AsString + TreeSplit + TCDS.FieldByName('eqv')
      .AsString + TreeSplit + FloatToStr(TCDS.FieldByName('C001').AsFloat / yl)
      + TreeSplit + FloatToStr(TCDS.FieldByName('C001T').AsFloat / yl) +
      TreeSplit + FloatToStr(TCDS.FieldByName('C002').AsFloat / yl) + TreeSplit
      + FloatToStr(TCDS.FieldByName('C002T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C003').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C003T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C004').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C004T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C005').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C005').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C101').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C101T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C102').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C102T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C201').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C201T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C202').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C202T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C204').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C204T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C203').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('C203T').AsFloat / yl) + TreeSplit +
      FloatToStr(TCDS.FieldByName('TT').AsFloat / yl) + TreeSplit +
      IntToStr(TCDS.FieldByName('T').AsInteger - T);
    // if s = V then
    // begin
    // TCDS.Next;
    // Continue;
    // end;
//    LogMsg(s);
    TS.add(s);
    // V := s;
    TCDS.Next;
  end;
    lstMtreel.BeginUpdate;
  FullRTTree(TS, lstMtreel);
    lstMtreel.EndUpdate;
  TS.Free;
  ResizeForm;
end;

procedure TWorkForm.PlRunTreeList(lj, dp, Ys, V: string);
// 批量运行树显示
var
  s1, s, SQL: string;
  mlj, pfName: string;
  TS: TstringList;
  R, T, i: Integer; // 记录号与层次
  yl: Double;
begin
  s1 := ' WHERE  MLJ in ' + lj + ' AND dpt = ''' + dp + ''' AND YearS = ' + Ys +
    ' AND VER  = ''' + V + '''';
  SQL := Format(TSQL, [s1]);
  // Memo1.Text := sql;
  // exit;
  if not RunCDS(SQL, TCDS) then
    Exit;
//  LogMsg('TCDS befor fier record ' + tcds.RecordCount.ToString);
  TS := TstringList.Create;
  TS.StrictDelimiter := True;

  lstMtreel.BeginUpdate;
  Screen.Cursor := crHourGlass;
  lstMtreel.Clear; // 树形GRID清空
  // 从GRID中读取产成品料号与配方号
  for i := 0 to cGB.DataController.RecordCount - 1 do
  begin
    mlj := cGB.DataController.Values[i, 17];
    pfName := cGB.DataController.Values[i, 1];
    // 将指定配方的相关记录写入stringlist中
    R := -1;
    T := -1;
    TS.Clear;

    TCDS.Filter := ' MLJ = ''' + mlj + '''';
    TCDS.Filtered := True;
//    LogMsg('TCDS after fier record ' + tcds.RecordCount.ToString);
    s := '';
    TCDS.First;

    while not TCDS.Eof do
    begin
      if R = -1 then
      begin
        if TCDS.FieldByName('bmb03').AsString = pfName then
        begin
          R := TCDS.FieldByName('R').AsInteger;
          T := TCDS.FieldByName('T').AsInteger;
          // 获取当前记录的用量，为零变为1
          yl := TCDS.FieldByName('YL').AsFloat;
          if yl = 0 then
            yl := 1;
        end
        else
        begin
          TCDS.Next;
          Continue;
        end;
      end;
      // 已获取层级后，当层级大于最外层时，循环终止
      if (TCDS.FieldByName('R').AsInteger > R) and
        (TCDS.FieldByName('T').AsInteger <= T) then
        Break;
      s := TCDS.FieldByName('bmb03').AsString + TreeSplit +
        TCDS.FieldByName('ima02').AsString + TreeSplit +
        TCDS.FieldByName('ima021').AsString + TreeSplit +
        TCDS.FieldByName('ima25').AsString + TreeSplit + TCDS.FieldByName('IsM')
        .AsString + TreeSplit + TCDS.FieldByName('ecd07').AsString + TreeSplit +
        TCDS.FieldByName('eca02').AsString + TreeSplit +
        FloatToStr(TCDS.FieldByName('YL').AsFloat / yl) + TreeSplit +
        TCDS.FieldByName('bmb08').AsString + TreeSplit +
        TCDS.FieldByName('JGXH').AsString + TreeSplit +
        TCDS.FieldByName('BGC07').AsString + TreeSplit + TCDS.FieldByName('Tim')
        .AsString + TreeSplit + TCDS.FieldByName('eqv').AsString + TreeSplit +
        FloatToStr(TCDS.FieldByName('C001').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C001T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C002').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C002T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C003').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C003T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C004').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C004T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C005').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C005').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C101').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C101T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C102').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C102T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C201').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C201T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C202').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C202T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C204').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C204T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C203').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('C203T').AsFloat / yl) + TreeSplit +
        FloatToStr(TCDS.FieldByName('TT').AsFloat / yl) + TreeSplit +
        IntToStr(TCDS.FieldByName('T').AsInteger - T);
      TS.add(s);
      TCDS.Next;
    end;
    PlFullRTTree(TS, lstMtreel);
  end;
  Screen.Cursor := crDefault;
  lstMtreel.EndUpdate;
  TS.Free;
  ResizeForm;
end;

procedure TWorkForm.Cost_treeClick(Sender: TObject);
var
  aRecord: TstringList;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowMessage('请选择账套后继续', '信息');
    Exit;
  end;
  if (Q_ima01.Text = '') or (cbb1.ItemIndex > 0) then
  begin
    ShowError('料件不能为空且筛选条件只能置为<等于>才能进行计算', '错误');
    Exit;
  end;
  // 如果部门为空，则只查询部门为' '的料件成本信息

  pgc1.ActivePageIndex := 1;

  RunTreeList(Trim(Q_ima01.Text), GetBM, GetND, GetBB, '配方');
end;

procedure TWorkForm.N1Click(Sender: TObject);
var
  ph: string;
  pf: string;
begin
  if cGB.Controller.FocusedRowIndex < 0 then
    Exit;
  ph := cGB.Controller.FocusedRow.Values[17];
  pf := cGB.Controller.FocusedRow.Values[1];
  // Memo1.Lines.Text := pf;
  pgc1.ActivePageIndex := 1;
  RunTreeList(ph, GetBM, GetND, GetBB, pf);
end;

procedure TWorkForm.pgc1Change(Sender: TObject);
begin
  ResizeForm;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
begin
  lstMtreel.FullExpand;
  ResizeForm;
end;

procedure TWorkForm.Button2Click(Sender: TObject);
begin
  lstMtreel.FullCollapse;
  ResizeForm;
end;

procedure TWorkForm.Cost_treePlClick(Sender: TObject);
var
  aRecord: TstringList;
  mlj: string;
  i: Integer;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowMessage('请选择账套后继续', '信息');
    Exit;
  end;
  // 如果部门为空，则只查询部门为' '的料件成本信息

  pgc1.ActivePageIndex := 1;
  mlj := '(';
  for i := 0 to cGB.DataController.RecordCount - 1 do
    mlj := mlj + '''' + cGB.DataController.Values[i, 17] + ''',';
  Delete(mlj, Length(mlj), 1);
  mlj := mlj + ')';
  PlRunTreeList(mlj, GetBM, GetND, GetBB);
end;

procedure TWorkForm.cbb3Change(Sender: TObject);
// 动态设置连接的数据库
begin
  Q_ima02.Text := DZT[cbb3.ItemIndex];
  dba.TargetDatabaseId := DZT[cbb3.ItemIndex];
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(pchar(MsgName), pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        Exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    Exit;
  end;
  // 获取程序校核信息
  if (StrComp(pchar(MsgName), pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        Exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    Exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(pchar(MsgName), pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        Exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    Exit;
  end;
end;

{$ENDREGION}

end.
